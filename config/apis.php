<?php

return [
    'jonview' => [
        'endpoint'          => env(
            'JONVIEW_API_PROVIDER_ENDPOINT',
            'https://h2htest.jonview.com/jonview_host/connectjonview'
        ),
        'user_id'           => env('JONVIEW_API_PROVIDER_USER_ID'),
        'password'          => env('JONVIEW_API_PROVIDER_PASSWORD'),
        'location_sequence' => env('JONVIEW_API_PROVIDER_LOCATION_SEQUENCE'),
    ],
];
