<?php

return [
    'update_profile_info' => 'Update profile info',
    'title'               => 'Company name',
    'shop_domain'         => 'Shop domain',
    'search_tour'         => 'Search tour',
];
