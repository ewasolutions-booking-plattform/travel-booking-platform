@extends('layouts.main')

@section('content')
    <section class="page-title style-two centred" style="background-image: url({{asset('/images/background/page-title-2.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
                <h1>{{ __('main.tours') }}</h1>
            </div>
            <div class="form-inner">
                <form action="index.html" method="post" class="booking-form clearfix">
                    <div class="form-group">
                        <input type="text" name="service" placeholder="{{ __('main.filter.where') }}" required=""
                         value="{{ $filter->country ? __('country.' . $filter->country->name)  : '' }}">
                    </div>
                    <div class="form-group input-date">
                        <i class="far fa-angle-down"></i>
                        <input type="text" name="date" placeholder="{{ __('main.filter.when') }}" id="datepicker">
                    </div>
                    <div class="form-group">
                        <div class="select-box">
                            <select class="wide">
                                <option data-display="Travel Type">{{ __('main.travel_type') }}</option>
                                <option value="1">Adventure Tours</option>
                                <option value="2">City Tours</option>
                                <option value="3">Couple Tours</option>
                                <option value="4">Group Tours</option>
                            </select>
                        </div>
                    </div>
                    <div class="message-btn">
                        <button type="submit" class="theme-btn"><i class="far fa-search"></i>{{ __('main.find') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <section class="tours-page-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 content-side">
                    <div class="item-shorting clearfix">
                        <div class="left-column pull-left">
                            <h3>{{ __('main.showing') }} {{ $tours->firstItem() }} - {{ $tours->lastItem() }} of {{ $tours->total() }}</h3>
                        </div>
                        <div class="right-column pull-right clearfix">
                            <div class="short-box clearfix">
                                <div class="select-box">
                                    <select class="wide">
                                        <option data-display="Sort by">{{ __('main.sort_by') }}</option>
                                        <option value="1">{{ __('main.sort.name_a_to_z') }}</option>
                                        <option value="2">{{ __('main.sort.name_z_to_a') }}</option>
                                        <option value="3">{{ __('main.sort.price_low_to_high') }}</option>
                                        <option value="3">{{ __('main.sort.price_high_low') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper grid">
                        <div class="tour-grid-content">
                            <div class="row clearfix">
                                @foreach($tours as $tour)
                                    <div class="col-lg-4 col-md-6 col-sm-12 tour-block">
                                        <div class="tour-block-one wow fadeInUp animated animated" data-wow-delay="00ms"
                                             data-wow-duration="1500ms">
                                            <div class="inner-box">

                                                <figure class="image-box">
                                                    <img src="{{ $supplierService->getFirstImage($tour->products()->first()->supplier) }}" alt="">
                                                    <a href="{{ route('tour', $tour->slug) }}"><i class="fas fa-link"></i></a>
                                                </figure>

                                                <div class="lower-content">
                                                    <h3><a href="{{ route('tour', $tour->slug) }}">{{ $tour->title }}</a></h3>
                                                    <h4>{{ $tour->price }}<span> / Per person</span></h4>
                                                    <ul class="info clearfix">
                                                        <li><i class="far fa-clock"></i>{{ $tour->duration }} {{ __('main.days') }}</li>
                                                        <li><i class="far fa-map"></i>{{ $tour->country->name }}, {{ $tour->region->code }}</li>
                                                    </ul>
                                                    {!! $tour->description !!}
                                                    <div class="btn-box">
                                                        <a href="{{ route('tour', $tour->slug) }}">{{ __('main.details') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    @if ($tours->hasPages())
                        <div class="pagination-wrapper">
                            <ul class="pagination clearfix">
                                @if (!$tours->onFirstPage())
                                    <li><a href="{{ $tours->previousPageUrl() }}"><i class="icon-Left-Arrow"></i></a></li>
                                @endif

                                @for($i = 1; $i <= $tours->lastPage(); $i++)
                                    @if ($i === $tours->currentPage())
                                        <li><a href="#" class="current">{{ $i }}</a></li>
                                    @else
                                        <li><a href="{{ $tours->url($i) }}">{{ $i }}</a></li>
                                    @endif
                                @endfor

                                @if ($tours->hasMorePages())
                                    <li><a href="{{ $tours->nextPageUrl() }}"><i class="icon-Right-Arrow"></i></a></li>
                                @endif
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
