@extends('layouts.main')

@section('content')

    <section class="page-title centred" style="background-image: url({{ asset('/images/background/page-title.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
                <h1>{{ __('main.destinations') }}</h1>
            </div>
        </div>
    </section>

    <section class="place-section sec-pad-2">
        <div class="anim-icon">
            <div class="icon anim-icon-1" style="background-image: url({{ asset('/images/icons/anim-icon-1.png')}});"></div>
            <div class="icon anim-icon-2" style="background-image: url({{ asset('assets/images/shape/shape-3.png')}});"></div>
            <div class="icon anim-icon-3" style="background-image: url({{ asset('assets/images/shape/shape-3.png')}});"></div>
        </div>
        <div class="auto-container">
            <div class="row clearfix">
                @foreach($countries as $country)
                    <div class="col-lg-4 col-md-6 col-sm-12 place-block">
                        <div class="place-block-one">
                            <div class="inner-box">
                                <figure class="image-box"><img src="{{ asset('/images/resource/place-1.jpg')}}" alt=""></figure>
                                <div class="text">
                                    <h3>
                                        <a href="{{ route('tours', ['country' => $country->code]) }}">
                                            {{ __('country.'.$country->name) }}
                                        </a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection
