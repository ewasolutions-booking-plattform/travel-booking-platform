@extends('layouts.main')

@section('content')

    <section class="page-title centred" style="background-image: url({{asset('/images/background/page-title-5.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
                <h1>{{ __('main.contact') }}</h1>
            </div>
        </div>
    </section>

    <section class="contact-info-section bg-color-1">
        <div class="anim-icon">
            <div class="icon anim-icon-1" style="background-image: url({{asset('/images/shape/shape-3.png')}});"></div>
            <div class="icon anim-icon-2" style="background-image: url({{asset('/images/shape/shape-3.png')}});"></div>
        </div>
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                    <div class="single-info-box wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="icon-box"><i class="icon-Location"></i></div>
                            <h3>{{ __('main.address') }}</h3>
                            <p>EWA Solutions GmbH
                                Unter Altstadt 10
                                6300 Zug</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                    <div class="single-info-box wow fadeInUp animated animated" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="icon-box"><i class="icon-Phone"></i></div>
                            <h3>{{ __('main.phone') }}</h3>
                            <p><a href="tel:0041765220158">0041 76 522 01 58</a></p>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                    <div class="single-info-box wow fadeInUp animated animated" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="icon-box"><i class="icon-Envelope"></i></div>
                            <h3>{{ __('main.email') }}</h3>
                            <p><a href="mailto:info@example.com">info@ewasolutions.ch</a></p>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contact-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-5 col-md-12 col-sm-12 content-column">
                    <div class="content_block_5">
                        <div class="content-box">
                            <div class="sec-title">
                                <h2>Uns finden Sie auch offline</h2>
                            </div>
                            <div class="text">
                                <p>{{ Str::upper(__('main.contact')) }}</p>
                                <p>
                                    <a class="text-secondary" href="mailto:info@ewasolutions.ch">info@ewasolutions.ch</a>
                                    <br>
                                    <a class="text-secondary" href="tel:0765220058">076 522 01 58</a>
                                </p>
                                <p>Unter Altstadt 10, 6300 Zug</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12 col-sm-12 form-column">
                    <div class="form-inner">
                        <form method="post" action="sendemail.php" id="contact-form" class="default-form">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <input type="text" name="username" placeholder="{{ __('main.name') }}" required="">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <input type="email" name="email" placeholder="{{ __('main.email') }}" required="">
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                    <input type="text" name="phone" required="" placeholder="{{ __('main.phone') }}">
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                    <input type="text" name="subject" required="" placeholder="{{ __('main.subject') }}">
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                    <textarea name="message" placeholder="{{ __('main.message') }}"></textarea>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group message-btn">
                                    <button class="theme-btn" type="submit" name="submit-form">{{ __('main.send') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
