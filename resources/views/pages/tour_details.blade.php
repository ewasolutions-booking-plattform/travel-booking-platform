@extends('layouts.main')

@section('content')

    <section class="page-title style-three" style="background-image: url({{asset('/images/background/page-title-3.jpg')}});">
        <div class="auto-container">
            <div class="inner-box">
                <h2>MIKE WIEGELE: VON KÄRNTEN NACH KANADA</h2>
                <h3>CHF 1700.00<span> / Per person</span></h3>
            </div>
        </div>
    </section>

    <section class="tour-details">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                    <div class="tour-details-content">
                        <div class="inner-box">
                            <div class="text">
                                <h2>Overview</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                <ul class="info-list clearfix">
                                    <li><i class="far fa-clock"></i>5 Days</li>
                                    <li><i class="far fa-user"></i>Age 15+</li>
                                    <li><i class="far fa-map"></i>G87P, Birmingham</li>
                                </ul>
                            </div>
                        </div>
                        <div class="overview-inner">
                            <ul class="overview-list clearfix">
                                <li><span>Destination:</span>Kanada</li>
                                <li><span>Departure:</span>Yes Requard</li>
                                <li><span>Departure Time:</span>New York City</li>
                                <li><span>Return Time:</span>English & Spanish</li>
                                <li class="clearfix"><span>Included:</span>
                                    <ul class="included-list clearfix">
                                        <li>Air fares</li>
                                        <li>4 Nights Hotel Accomodation</li>
                                        <li>Entrance Fees</li>
                                        <li>Tour Guide</li>
                                    </ul>
                                </li>
                                <li class="clearfix"><span>Excluded:</span>
                                    <ul class="excluded-list clearfix">
                                        <li>Air fares</li>
                                        <li>Air fares</li>
                                        <li>Air fares</li>
                                        <li>Air fares</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="tour-plan">
                            <div class="text">
                                <h2>Tour Plan</h2>
                                <p>Aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="content-box">
                                <div class="single-box">
                                    <span>01</span>
                                    <h4>8:00 am to 10:00 am</h4>
                                    <h3>Day 1: Arrive South Africa Forest</h3>
                                    <p>Aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <ul class="list clearfix">
                                        <li>Air fares</li>
                                        <li>4 Nights Hotel Accomodation</li>
                                        <li>Entrance Fees</li>
                                    </ul>
                                </div>
                                <div class="single-box">
                                    <span>02</span>
                                    <h4>8:00 am to 10:00 am</h4>
                                    <h3>Day 2: Arrive South Africa Forest</h3>
                                    <p>Aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <ul class="list clearfix">
                                        <li>Air fares</li>
                                        <li>4 Nights Hotel Accomodation</li>
                                        <li>Entrance Fees</li>
                                    </ul>
                                </div>
                                <div class="single-box">
                                    <span>03</span>
                                    <h4>8:00 am to 10:00 am</h4>
                                    <h3>Day 3: Arrive South Africa Forest</h3>
                                    <p>Aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <ul class="list clearfix">
                                        <li>Air fares</li>
                                        <li>4 Nights Hotel Accomodation</li>
                                        <li>Entrance Fees</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="location-map">
                            <div class="text">
                                <h2>View On Map</h2>
                                <p>Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat duis aute irure dolor.</p>
                            </div>
                            <div class="map-inner">
                                <div
                                    class="google-map"
                                    id="contact-google-map"
                                    data-map-lat="40.712776"
                                    data-map-lng="-74.005974"
                                    data-icon-path="{{asset('/images/icons/map-marker.png')}}"
                                    data-map-title="Brooklyn, New York, United Kingdom"
                                    data-map-zoom="12"
                                    data-markers='{
                                            "marker-1": [40.712776, -74.005974, "<h4>Branch Office</h4><p>77/99 New York</p>","{{asset('/images/icons/map-marker.png')}}"]
                                        }'>

                                </div>
                            </div>
                        </div>
                        <div class="photo-gallery">
                            <div class="text">
                                <h2>Photo Gallery</h2>
                                <p>Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            </div>
                            <div class="image-box clearfix">
                                <figure class="image">
                                    <img src="{{asset('/images/resource/destination-44.jpg')}}" alt="">
                                    <a href="{{asset('/images/resource/destination-4.jpg')}}" class="view-btn lightbox-image" data-fancybox="gallery"><i class="icon-Plus"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="{{asset('/images/resource/destination-44.jpg')}}" alt="">
                                    <a href="{{asset('/images/resource/destination-4.jpg')}}" class="view-btn lightbox-image" data-fancybox="gallery"><i class="icon-Plus"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="{{asset('/images/resource/destination-44.jpg')}}" alt="">
                                    <a href="{{asset('/images/resource/destination-4.jpg')}}" class="view-btn lightbox-image" data-fancybox="gallery"><i class="icon-Plus"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="{{asset('/images/resource/destination-55.jpg')}}" alt="">
                                    <a href="{{asset('/images/resource/destination-555.jpg')}}" class="view-btn lightbox-image" data-fancybox="gallery"><i class="icon-Plus"></i></a>
                                </figure>
                                <figure class="image">
                                    <img src="{{asset('/images/resource/destination-44.jpg')}}" alt="">
                                    <a href="{{asset('/images/resource/destination-4.jpg')}}" class="view-btn lightbox-image" data-fancybox="gallery"><i class="icon-Plus"></i></a>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                    <div class="default-sidebar tour-sidebar ml-20">
                        <div class="advice-widget">
                            <div class="inner-box" style="background-image: url({{asset('/images/resource/advice-1.jpg')}});">
                                <div class="text">
                                    <h2>Get <br />25% Off <br />On New York Tours</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
