<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @php $isHome = 'home' === Route::currentRouteName(); @endphp

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <title>Travel platform</title>

    <link rel="icon" href="{{ asset('/images/favicon-150x150.ico')}}" sizes="32x32"/>
    <link rel="icon" href="{{ asset('/images/favicon.ico')}}" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/images/favicon.ico')}}"/>
    <meta name="msapplication-TileImage" content="{{ asset('/images/favicon.ico')}}"/>

    <link
        href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <link href="{{ asset('/css/font-awesome-all.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/flaticon.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/owl.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/ion.rangeSlider.min.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/jquery.fancybox.min.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/nice-select.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/jquery-ui.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/color.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="{{ asset('/css/booking-multistep-form.css') }}" rel="stylesheet" id="bootstrap">
    <link href="{{ asset('/css/responsive.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    @livewireStyles
</head>

<body>

<div class="boxed_wrapper">

    <div class="loader-wrap">
        <div class="preloader">
            <div class="preloader-close">Preloader Close</div>
            <div id="handle-preloader" class="handle-preloader">
                <div class="animation-preloader">
                    <div class="spinner"></div>
                    <div class="txt-loading">
                        <span data-text-preloader="E" class="letters-loading">E</span>
                        <span data-text-preloader="W" class="letters-loading">W</span>
                        <span data-text-preloader="A" class="letters-loading">A</span>
                        <span data-text-preloader="S" class="letters-loading">S</span>
                        <span data-text-preloader="O" class="letters-loading">O</span>
                        <span data-text-preloader="L" class="letters-loading">L</span>
                        <span data-text-preloader="U" class="letters-loading">U</span>
                        <span data-text-preloader="T" class="letters-loading">T</span>
                        <span data-text-preloader="I" class="letters-loading">I</span>
                        <span data-text-preloader="O" class="letters-loading">O</span>
                        <span data-text-preloader="N" class="letters-loading">N</span>
                        <span data-text-preloader="S" class="letters-loading">S</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <header @class(['main-header', 'style-one' => $isHome, 'style-three' => !$isHome])>

        <div class="header-lower">
                <div class="auto-container">
                    <div class="outer-box">
                        <div class="logo-box">
                            <figure class="logo"><a href="/"><img src="{{ asset('/images/logo.svg')}}" alt=""></a></figure>
                        </div>

                        <div class="menu-area clearfix">
                            <div class="mobile-nav-toggler">
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                            </div>

                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li><a href="/">{{ __('main.home') }}</a></li>
                                        <li><a href="/destinations">{{ __('main.destinations') }}</a></li>
                                        <li><a href="/tours">{{ __('main.tours') }}</a></li>
                                        <li><a href="/contact">{{ __('main.contact') }}</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>

                        <ul class="menu-right-content clearfix">
                            <li class="search-box-outer">
                                <div class="dropdown">
                                    <button class="search-box-btn" type="button" id="dropdownMenu3" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false"><i class="far fa-search"></i>
                                    </button>
                                    <div class="dropdown-menu search-panel" aria-labelledby="dropdownMenu3">
                                        <div class="form-container">
                                            <form method="post" action="blog.html">
                                                <div class="form-group">
                                                    <input type="search" name="search-field" value=""
                                                           placeholder="Search...." required="">
                                                    <button type="submit" class="search-btn"><span
                                                            class="fas fa-search"></span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="user-link">
                                <a href=""><i class="icon-Profile"></i></a>
                            </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="sticky-header">
            <div class="auto-container">
                <div class="outer-box">
                    <div class="logo-box">
                        <figure class="logo"><a href="/"><img src="{{ asset('/images/logo.svg')}}" alt=""></a></figure>
                    </div>
                    <div class="menu-area">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                    <ul class="menu-right-content clearfix">
                        <li class="search-box-outer">
                            <div class="dropdown">
                                <button class="search-box-btn" type="button" id="dropdownMenu4" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false"><i class="far fa-search"></i>
                                </button>
                                <div class="dropdown-menu search-panel" aria-labelledby="dropdownMenu4">
                                    <div class="form-container">
                                        <form method="post" action="blog.html">
                                            <div class="form-group">
                                                <input type="search" name="search-field" value=""
                                                       placeholder="Search...." required="">
                                                <button type="submit" class="search-btn"><span
                                                        class="fas fa-search"></span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="user-link">
                            <a href=""><i class="icon-Profile"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>

        <nav class="menu-box">
            <div class="nav-logo"><a href="/"><img src="{{ asset('/images/logo.svg')}}" alt="" title=""></a>
            </div>
            <div class="menu-outer">
                <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <div class="contact-info">
                <h4>Contact Info</h4>
                <ul>
                    <li>Unter Altstadt 10, 6300 Zug</li>
                    <li><a href="tel:+8801682648101">+88 01682648101</a></li>
                    <li><a href="mailto:info@example.com">info@example.com</a></li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="clearfix">
                    <li><a href="/"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="/"><span class="fab fa-facebook-square"></span></a></li>
                    <li><a href="/"><span class="fab fa-pinterest-p"></span></a></li>
                    <li><a href="/"><span class="fab fa-instagram"></span></a></li>
                    <li><a href="/"><span class="fab fa-youtube"></span></a></li>
                </ul>
            </div>
        </nav>
    </div>

    @yield('content')

    <footer class="main-footer bg-color-2">
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="bottom-inner clearfix">
                    <div class="copyright pull-left">
                        <p><a href="/">EWA Solutions</a> &copy; 2021 All Right Reserved</p>
                    </div>
                    <ul class="footer-nav pull-right">
                        <li><a href="/">Terms of Service</a></li>
                        <li><a href="/">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <button class="scroll-top scroll-to-target" data-target="html">
        <span class="fal fa-angle-up"></span>
    </button>
</div>

<script src="{{ asset('/js/jquery.js')}}"></script>
<script src="{{ asset('/js/popper.min.js')}}"></script>
<script src="{{ asset('/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('/js/bootstrap-select.min.js')}}"></script>
<script src="{{ asset('/js/owl.js')}}"></script>
<script src="{{ asset('/js/wow.js')}}"></script>
<script src="{{ asset('/js/validation.js')}}"></script>
<script src="{{ asset('/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('/js/appear.js')}}"></script>
<script src="{{ asset('/js/scrollbar.js')}}"></script>
<script src="{{ asset('/js/jquery.nice-select.min.js')}}"></script>
<script src="{{ asset('/js/jquery-ui.js')}}"></script>

<!-- main-js -->
<script src="{{ asset('/js/script.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{ asset('/js/booking-multistep-form.js')}}"></script>
<script src="{{ asset('/js/imask.js')}}"></script>
@livewireScripts
<script src="{{ asset('/js/ion.rangeSlider.min.js')}}"></script>
@stack('budget-range')
</body>
</html>
