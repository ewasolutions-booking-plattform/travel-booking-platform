<div>
    <fildset>
                <div class="form-group mb-0" wire:ignore>
                    <select
                        wire:model="sort"
                        data-width="170px"
                        id="tours-sort"
                        class="selectpicker ignore">
                        <option value="">{{ __('main.sort_by') }}</option>
                        @foreach($sortTypes as $value)
                            <option value="{{ $value }}" @if($value === $sort) 'selected' @endif>
                            {{ __('main.sort.'.$value) }}
                            </option>
                        @endforeach
                    </select>
                </div>
    </fildset>

</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.selectpicker').selectpicker();
        });
    </script>
@endpush
