<div>

    <div class="row">
        <div class="col">
        @foreach($typeGroups as $id => $group)
            <div class="form-check form-check-inline">
                <input id="type-{{ $id }}"
                       class="form-check-input"
                       type="radio"
                       value="{{ $id }}"
                       wire:model="typeGroup"
                       wire:key="{{ $id }}"
                >
                <label for="type-{{ $id }}"
                    class="radio-inline form-check-label font-weight-normal"
                >
                    {{ $group }}
                </label>
            </div>
        @endforeach
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <select
                wire:model="productType"
                id="product_type"
                class="form-control select2-hidden-accessible">
                <option value="0">{{ __('booking.select_product_type') }}</option>
                @foreach($productTypes as $code => $name)
                    <option wire:key="{{ $code }}" value="{{$code}}"> {{ $name }}</option>
                @endforeach
            </select>
        </div>
    </div>

</div>
