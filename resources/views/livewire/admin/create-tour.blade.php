<div>
    <div wire:ignore.self
         id="createTourModal"
         class="modal fade"
         tabindex="-1"
         role="dialog"
         aria-labelledby="createTourModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="createTourModalLabel">Create tour</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <input wire:model="tourTitle"
                           type="text"
                           class="form-control my-2"
                           placeholder="tour title" />

                    <select
                        wire:model="tripTheme"
                        id="trip_theme"
                        class="form-control select2-hidden-accessible my-2">
                        <option value="0">{{ __('booking.select_trip_theme') }}</option>
                        @foreach($tripThemes as $id => $title)
                            <option value="{{$id}}"> {{ $title }}</option>
                        @endforeach
                    </select>

                    <select
                        wire:model="tripThemeType"
                        id="trip_theme_type"
                        class="form-control select2-hidden-accessible my-2">
                        <option value="0">{{ __('booking.select_trip_theme_type') }}</option>
                        @foreach($tripThemeTypes as $id => $title)
                            <option value="{{$id}}"> {{ $title }}</option>
                        @endforeach
                    </select>

                    <input wire:model="tourUserPrice"
                           type="text"
                           class="form-control my-2"
                           placeholder="tour price per person for user" />

                    <div class="form-check">
                        <input wire:model="isPublish" type="checkbox" class="form-check-input" id="is-publish">
                        <label class="form-check-label" for="is-publish">Publish on the frontend</label>
                    </div>

                    @error('tourTitle')
                        <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                    @error('tripTheme')
                        <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                    @error('tripThemeType')
                        <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                    @error('tourUserPrice')
                        <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror

                </div>

                <div class="modal-footer">
                    <button type="button" wire:click.prevent="cancel" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                    <button type="button" wire:click.prevent="store" class="btn btn-primary close-modal">Create</button>
                </div>

            </div>
        </div>
    </div>

</div>
