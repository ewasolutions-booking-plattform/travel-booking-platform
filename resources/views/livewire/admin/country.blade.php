<div>
    <label for="country">{{ __('booking.country') }}</label>
    <select
        wire:model="country"
        id="country"
        class="form-control select2-hidden-accessible">
        <option value="0">{{ __('booking.select_country') }}</option>
        @foreach($this->sortedCountries as $id => $name)
            <option wire:key="{{ $id }}" value="{{$id}}"> {{ $name }}</option>
        @endforeach
    </select>
</div>
