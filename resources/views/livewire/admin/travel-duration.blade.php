<div class="row">
    <div class="col-md-6">
        <label for="start_date">{{ __('booking.tour_start_date') }}</label>
        <input
            wire:model="travelStartDate"
            id="start_date"
            class="form-control"
            type="date"
            name="start_date"
            min="{{ $minStartDate->format('Y-m-d') }}"
        >
        @error('travelStartDate') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="col-md-6">
        <label for="end_date">{{ __('booking.tour_end_date') }}</label>
        <input
            wire:model="travelEndDate"
            id="end_date"
            class="form-control"
            type="date"
            name="end_date"
            min="{{ $minStartDate->format('Y-m-d') }}"
        >
        @error('travelEndDate') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

</div>
