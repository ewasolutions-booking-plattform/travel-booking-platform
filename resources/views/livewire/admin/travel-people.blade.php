<div class="row">
    @foreach($ageCategories as $ageCategory)
        @php
            $ageModel = 'travelPeople.'.$ageCategory
        @endphp
        <div class="col-md-4">

            <label for="{{ $ageModel }}">{{ __('booking.'.$ageCategory) }}</label>
            <input wire:model="{{$ageModel}}"
                   id="{{ $ageModel }}"
                   class="form-control"
                   type="number"
                   min="{{ 'adult' === $ageCategory ? 1 : 0 }}"
                   max="10">
            @error('travelPeople.'.$ageCategory) <span class="text-danger">{{ $message }}</span>@enderror
        </div>
    @endforeach
</div>
