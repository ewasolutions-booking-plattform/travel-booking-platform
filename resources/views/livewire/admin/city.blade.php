<div>
    <label for="city">{{ __('booking.city') }}</label>
    <select
        wire:model="city"
        id="city"
        class="form-control select2-hidden-accessible">
        <option value="0">{{ __('booking.select_city') }}</option>
        @foreach($this->sortedCities as $city)
            <option wire:key="{{ $city['id'] }}" value="{{ $city['id'] }}"> {{ Str::title($city['name']) }}</option>
        @endforeach
    </select>
</div>
