<div>
    <label for="region">{{ __('booking.region') }}</label>
    <select
        wire:model="region"
        id="region"
        class="form-control select2-hidden-accessible">
        <option value="0">{{ __('booking.select_region') }}</option>
        @foreach($this->sortedRegions as $id => $name)
            <option wire:key="{{ $id }}" value="{{ $id }}"> {{ $name }}</option>
        @endforeach
    </select>
</div>
