<div>
    <div wire:ignore.self
         id="selectBookingModal"
         class="modal fade"
         tabindex="-1"
         role="dialog"
         aria-labelledby="bookingModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="bookingModalLabel">Select booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <input wire:model="tourTitle"
                           type="text"
                           class="form-control my-1">

                    <select
                        wire:model="tripTheme"
                        id="trip_theme"
                        class="form-control select2-hidden-accessible my-1">
                        <option value="0">{{ __('booking.select_trip_theme') }}</option>
                        @foreach($tripThemes as $id => $title)
                            <option value="{{$id}}"> {{ $title }}</option>
                        @endforeach
                    </select>

                    <select
                        wire:model="tripThemeType"
                        id="trip_theme_type"
                        class="form-control select2-hidden-accessible my-1">
                        <option value="0">{{ __('booking.select_trip_theme_type') }}</option>
                        @foreach($tripThemeTypes as $id => $title)
                            <option value="{{$id}}"> {{ $title }}</option>
                        @endforeach
                    </select>

                    <input wire:model="tourUserPrice"
                           type="text"
                           class="form-control my-2"
                           placeholder="tour price per person for user" />

                    <div class="form-check">
                        <input wire:model="isPublish" type="checkbox" class="form-check-input" id="is-publish">
                        <label class="form-check-label" for="is-publish">Publish on the frontend</label>
                    </div>

                    @error('tourTitle')
                    <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                    @error('tripTheme')
                    <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                    @error('tripThemeType')
                    <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                    @error('tourUserPrice')
                    <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror

                    <div class="list-group">
                        @foreach($bookings as $item)
                            <label class="list-group-item pl-4">
                                <input
                                    wire:key="{{ $item['id'] }}"
                                    wire:model="booking"
                                    name="booking"
                                    class="form-check-inline"
                                    type="radio"
                                    value="{{ $item['id'] }}">
                                {{ $item['client_email'] }}

                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{ Str::ucfirst($item['client_first_name']) }} {{ Str::ucfirst($item['client_last_name']) }} </h5>
                                    <small> {{ $item['duration_count'] }} {{ $item['duration_type'] }}</small>
                                </div>
                                <p class="mb-1"><strong>Booking:</strong>
                                    {{ $item['trip_theme']['title'] ?? '' }} {{ $item['trip_theme_type']['title'] ?? '' }}
                                </p>
                                <small>{{ $item['city']['name'] ?? '' }}</small>
                            </label>
                        @endforeach
                    </div>

                    @error('booking')
                        <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div class="modal-footer">
                    <button type="button" wire:click.prevent="cancel" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                    <button type="button" wire:click.prevent="store" class="btn btn-primary close-modal">Append</button>
                </div>

            </div>
        </div>
    </div>

</div>
