<div>
    <div wire:ignore.self
         id="modal"
         class="modal fade"
         tabindex="-1"
         role="dialog"
         aria-labelledby="modalLabel"
         aria-hidden="true">

        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Create tour</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <input wire:model="tourTitle"
                           type="text"
                           class="form-control my-2"
                           placeholder="tour title" />

                    <input wire:model="tourUserPrice"
                           type="text"
                           class="form-control my-2"
                           placeholder="0.0" />

                    <div class="form-check">
                        <input wire:model="isPublish" type="checkbox" class="form-check-input" id="is-publish">
                        <label class="form-check-label" for="is-publish">Publish on the frontend</label>
                    </div>

                    @error('tourTitle')
                    <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror
                    @error('tourUserPrice')
                    <p class="alert alert-danger mt-2">{{ $message }}</p>
                    @enderror

                </div>

                <div class="modal-footer">
                    <button type="button" wire:click.prevent="cancel" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                    <button type="button" wire:click.prevent="store" class="btn btn-primary close-modal">Create</button>
                </div>

            </div>
        </div>
    </div>

</div>
