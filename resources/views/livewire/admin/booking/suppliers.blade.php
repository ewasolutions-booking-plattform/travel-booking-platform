<div>
    @error('productID')
        <p class="alert alert-danger" role="alert">{{ $message }}</p>
    @enderror
    @error('bookingID')
        <p class="alert alert-danger" role="alert">{{ $message }}</p>
    @enderror
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Sup. Code</th>
            <th>Supplier</th>
            <th>Rating</th>
            <th>Address</th>
            <th>Phone / Fax</th>
            <th>Amenities</th>
            <th>Activities</th>
        </tr>
        </thead>
        <tbody>
        @forelse($suppliers as $supplier)
            <tr>
                <td>{{ $supplier['code'] }}</td>
                <td>{{ $supplier['name'] }}</td>
                <td>{{ $supplier['rating'] }}</td>
                <td>{{ $supplier['address1'] }} {{ $supplier['address2'] }} {{ $supplier['address3'] }}</td>
                <td>{{ $supplier['phone'] }} / {{ $supplier['fax'] }}</td>
                <td>
                    @foreach($supplier['amenities'] as $amenity)
                        {{ $amenity['name'] }} &nbsp;
                    @endforeach
                </td>
                <td>
                    @foreach($supplier['activities'] as $activity)
                        {{ $activity['name'] }} &nbsp;
                    @endforeach
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <p>{{ $supplier['description'] }}</p>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <div class="card-group">
                        @foreach($supplier['products'] as $product)
                            <div class="card mr-3" style="max-width: 20rem;">
                                <div class="card-header text-white bg-primary mb-3">
                                    {{ $product['name'] }} ({{ $product['code'] }})
                                </div>

                                <div class="card-body">
                                    @error('product_id_'.$product['id'])
                                    <p class="alert alert-danger" role="alert">{{ $message }}</p>
                                    @enderror
                                    @if (session()->has('success_'.$product['id']))
                                        <p class="alert alert-success" role="alert">
                                            {{ session('success_'.$product['id']) }}
                                        </p>
                                    @endif
                                    <p class="card-text">
                                        @foreach($product['characteristics'] as $characteristic)
                                            {{ __('booking.'.$characteristic['name']) }}: {{ $characteristic['pivot']['value'] }} <br>
                                        @endforeach
                                    </p>
                                </div>

                                <ul class="list-group list-group-flush">
                                    @foreach($product['prices'] as $price)
                                        <li class="list-group-item">
                                            <i class='icon la la-money-bill-alt'></i> Price of
                                            <ul>
                                                @if($price['single'])
                                                    <li class="list-group-item">single: {{ $price['currency']['code'] }} {{ $price['single'] }}</li>
                                                @endif
                                                @if($price['twin'])
                                                    <li class="list-group-item">twin: {{ $price['currency']['code'] }} {{ $price['twin'] }}</li>
                                                @endif
                                                @if($price['triple'])
                                                    <li class="list-group-item">triple: {{ $price['currency']['code'] }} {{ $price['triple'] }}</li>
                                                @endif
                                                @if($price['quad'])
                                                    <li class="list-group-item">quad: {{ $price['currency']['code'] }} {{ $price['quad'] }}</li>
                                                @endif
                                                @if($price['child1'] > 0)
                                                    <li class="list-group-item">child1: {{ $price['currency']['code'] }} {{ $price['child1'] }}</li>
                                                @endif
                                            </ul>
                                        </li>
                                    @endforeach

                                    @foreach($product['availables'] as $available)
                                        <li class="list-group-item">
                                            <i class='icon la la-calendar-alt'></i> {{ \Carbon\Carbon::parse($available['from_date'])->format('d.m.Y') }} - {{ \Carbon\Carbon::parse($available['to_date'])->format('d.m.Y') }} <br>
                                            @foreach(str_split($available['available']) as $ch)
                                                @if($ch === '0')
                                                    <span class="badge rounded-pill bg-warning">{{ $ch }}</span>
                                                @endif
                                                @if($ch >= 1 && $ch <= 5)
                                                    <span class="badge rounded-pill bg-success">{{ $ch }}</span>
                                                @endif
                                                @if($ch == 'B')
                                                    <span class="badge rounded-pill bg-danger">{{ $ch }}</span>
                                                @endif
                                            @endforeach
                                        </li>
                                    @endforeach

                                </ul>
                                <div class="card-footer bg-light">
                                    <button type="button"
                                            class="btn btn-success"
                                            wire:click.prevent="showTourModalClick({{ $product['id'] }})">
                                        Create tour
                                    </button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="7">Products not found</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
