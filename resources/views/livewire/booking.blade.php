<div>
    @if(!empty($successMsg))
        <div class="alert alert-success">
            {{ $successMsg }}
        </div>
    @endif

    <div class="stepwizard">

        <div class="sec-title text-center">
            <h2>{{ __('booking.trip_place') }} {{ $country > 0 ? __('country.' . $dataCountries[$country]) : ''}}</h2>
        </div>
    </div>

    <fieldset class="row setup-content {{ $currentStep != 1 ? 'display-none' : '' }}" id="step-1">
        <div class="col-md-12">
            <h3>{{ __('booking.title_step_1') }}</h3>
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="country">{{ __('booking.country') }}</label>
                        <div wire:ignore id="for-country">
                            <select wire:model="country"
                                    class="selectpicker ignore"
                                    data-live-search="true"
                                    data-container="#for-country"
                                    id="country">
                                <option value="0">{{ __('booking.select_country') }}</option>
                                @foreach($dataCountries as $id => $name)
                                    <option wire:key="{{$id}}" value="{{ $id }}">{{ __('country.'.$name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('country') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="region">{{ __('booking.region') }}</label>
                        <div class="" wire:ignore id="for-region">
                            <select wire:model="region"
                                    class="selectpicker ignore"
                                    data-live-search="true"
                                    data-container="#for-region"
                                    id="region">
                                <option value="0">{{ __('booking.select_region') }}</option>
                                @foreach($dataRegions as $id => $name)
                                    <option wire:key="{{$id}}" value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('region') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="city">{{ __('booking.city') }}</label>
                        <div class="" wire:ignore id="for-region">
                            <select wire:model="city"
                                    class="selectpicker ignore"
                                    data-live-search="true"
                                    data-container="#for-city"
                                    id="city">
                                <option value="0">{{ __('booking.select_city') }}</option>
                                @foreach($dataCities as $id => $name)
                                    <option wire:key="{{ $id }}" value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('city') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>

            </div>
            <button class="theme-btn nextBtn pull-right" wire:click="oneStepSubmit"
                    type="button">{{ __('booking.next') }}</button>
        </div>
    </fieldset>

    <fieldset class="row setup-content {{ $currentStep != 2 ? 'display-none' : '' }}" id="step-2">
        <div class="col-md-12">
            <h3>{{ __('booking.title_step_2') }}</h3>
            <div class="row">

                @foreach($dataAgeCategories as $ageCategory)
                    @php
                        $ageModel = 'selectedAgeCategories.'.$ageCategory['id']
                    @endphp
                    <div class="col-sm-4 col-md-3">
                        <div class="form-group">
                            <label for="{{ $ageModel }}">{{ __('booking.'.$ageCategory['title']) }}</label>
                            <div class="input-group">
                                <div class="number-input">
                                    <button wire:click="stepDownAge({{ $ageCategory['id'] }})"></button>
                                    <input class="quantity" id="{{ $ageModel }}" wire:model="{{$ageModel}}"
                                           type="number" min="1" max="10">
                                    <button wire:click="stepUpAge({{ $ageCategory['id'] }})" class="plus"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="col-sm-4 col-md-3">
                    <div class="form-group">
                        <div class="form-date-range">
                            <label for="date-range">{{ __('booking.trip_duration') }}</label>
                            <div class="input-group" wire:ignore>
                                <input id="date-range"
                                       class="form-control"
                                       type="text"
                                       name="daterange"
                                       wire:model="tripRangeDate"
                                       value="{{ $startDate .' - ' . $endDate }}"
                                       onchange="this.dispatchEvent(new InputEvent('input'))"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        @if($errors->has('selectedAgeCategories.1'))
                            <span class="error">{{ $errors->first('selectedAgeCategories.1') }}</span>
                        @endif
                        @error('tripRangeDate') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>
            <button class="theme-btn nextBtn pull-right" type="button"
                    wire:click="twoStepSubmit">{{ __('booking.next') }} </button>
            <button class="theme-btn prevBtn pull-left" type="button"
                    wire:click="back(1)">{{ __('booking.back') }}</button>
        </div>
    </fieldset>

    <fieldset class="row setup-content {{ $currentStep != 3 ? 'display-none' : '' }}" id="step-3">
        <div class="col-md-12">
            <h3></h3>
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="trip_theme"></label>
                        <div class="input-group">
                            <div class="" wire:ignore id="for-trip_theme">
                                <select id="trip_theme"
                                        wire:model="tripTheme"
                                        class="selectpicker ignore"
                                        data-live-search="true"
                                        data-container="#for-trip_theme">
                                    <option value="0">{{ __('booking.select_trip_theme') }}</option>
                                    @foreach($dataTripTheme as $theme)
                                        <option value="{{ $theme['id'] }}"> {{ __('booking.'.$theme['title']) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @error('tripTheme') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="trip_theme_type"></label>
                        <div class="input-group">
                            <div class=""
                                 wire:ignore
                                 id="for-trip_theme_type"
                            >
                                <select
                                    id="trip_theme_type"
                                    wire:model="tripThemeType"
                                    class="selectpicker ignore"
                                    data-live-search="true"
                                    data-container="#for-trip_theme_type">
                                    <option value="0">{{ __('booking.select_trip_theme_type') }}</option>
                                    @foreach($dataTripThemeType as $id => $title)
                                        <option value="{{ $id }}"> {{ __('booking.'.$title) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @error('tripThemeType') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>

            <button class="theme-btn nextBtn pull-right" type="button" wire:click="fourStepSubmit">
                {{ __('booking.next') }}
            </button>
            <button class="theme-btn prevBtn pull-left" type="button" wire:click="back(2)">
                {{ __('booking.back') }}
            </button>
        </div>
    </fieldset>

    <fieldset class="row setup-content {{ $currentStep != 4 ? 'display-none' : '' }}" id="step-4">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <h3>{{ __('booking.title_step_5') }}</h3>
                    <div class="" wire:ignore id="for-budget">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="budget">{{ __('booking.label_step_5_1') }}</label>

                                    <input type="text"
                                           class="budget-range-slider"
                                           data-container="#for-budget"
                                           wire:model="budget"
                                    />
                                </div>

                                @error('budget') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col-sm-4">
                                <div class="your-budget">
                                    <div class="your-budget-val">{{$budget}}</div>
                                    {{ __('booking.label_step_5_2') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div wire:loading wire:target="fiveStepSubmit" class="loader">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row justify-content-md-center  text-center">
                            <div class="col col-lg-4">
                                <div style="margin:20px 0">{{ __('booking.label_step_5_3') }}</div>
                                <div class="text-center" style="display:inline-block;">
                                    <div style="color: #ff7c5b" class="la-ball-spin la-3x">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button class="theme-btn nextBtn pull-right" type="button" wire:click="fiveStepSubmit">
                        {{ __('booking.next') }}
                    </button>
                    <button class="theme-btn prevBtn pull-left" type="button" wire:click="back(3)">
                        {{ __('booking.back') }}
                    </button>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="row setup-content {{ $currentStep != 5 ? 'display-none' : '' }}" id="step-5">
        <div class="col-md-12">
            <h3>{{ __('booking.title_step_6') }}</h3>
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="firstName"></label>
                        <div class="input-group">
                            <input id="firstName"
                                   type="text"
                                   wire:model="firstName"
                                   class="form-control"
                                   placeholder="{{ __('booking.label_first_name') }}"/>
                        </div>
                        @error('firstName') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="lastName"></label>
                        <div class="input-group">
                            <input id="lastName"
                                   type="text"
                                   wire:model="lastName"
                                   class="form-control"
                                   placeholder="{{ __('booking.label_last_name') }}"/>
                        </div>
                        @error('lastName') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="email"></label>
                        <div class="input-group">
                            <input id="email"
                                   wire:model="email"
                                   class="form-control"
                                   placeholder="{{ __('booking.label_email') }}"/>
                        </div>
                        @error('email') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="form-group">
                        <label for="phone"></label>
                        <div class="input-group">
                            <input id="phone"
                                   wire:model="phone"
                                   class="form-control"
                                   placeholder="{{__('booking.label_phone')}}"/>
                        </div>
                        @error('phone') <span class="error">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>

            <button class="theme-btn nextBtn pull-right" type="button"
                    wire:click="sendProposal">{{ __('booking.send') }}</button>
        </div>
    </fieldset>

</div>

@push('budget-range')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var $budget = $(".budget-range-slider");

            $budget.ionRangeSlider({
                min: 500,
                max: 10000,
                from: 5000,
                step: 100,
                prefix: "CHF ",
                onStart: function (data) {
                    var budget_val = data.from;

                },
            });

            $budget.on("change", function () {
                var $inp = $(this);
                var from = $inp.prop("value"); // reading input value
                @this.budget = from;
                $('.your-budget-val').text(from);
            });

            moment.locale('de');
            $('#date-range').daterangepicker({
                format: "DD.MM.YYYY",
                minDate: "{{ $minDate }}",
                startDate: "{{ $startDate }}",
                endDate: "{{ $endDate }}",
                locale: {
                    cancelLabel: "{{ __('booking.daterange.cancel') }}",
                    applyLabel: "{{ __('booking.daterange.apply') }}",
                }
            });

            var phoneMask = IMask(document.getElementById('phone'), {mask: '0000 00 000 00 00'});
        });

        Livewire.on('updatedData', () => {
            $('.booking-travel-form select#trip_theme_type option').remove();
            $('.booking-travel-form select#trip_theme_type')
                .append('<option value="0">Reiseart wählen</option>');
        @this.dataTripThemeType.forEach(function (type, i) {
            $('.booking-travel-form select#trip_theme_type')
                .append('<option value="' + type.id + '">' + type.title + '</option>');
        });
            $('.booking-travel-form select#trip_theme_type').selectpicker('refresh');
        })

        Livewire.on('updatedDataRegions', () => {
            $('.booking-travel-form select#region option').remove();
            $('.booking-travel-form select#region')
                .append('<option value="0">Region wählen</option>');
            Object.keys(@this.dataRegions).forEach(id => {
                $('.booking-travel-form select#region')
                    .append('<option value="' + id + '">' + @this.dataRegions[id] + '</option>');
            });
            $('.booking-travel-form select#region').selectpicker('refresh');
            $('.booking-travel-form select#city option').remove();
            $('.booking-travel-form select#city')
                .append('<option value="0">tadt wählen</option>');
            $('.booking-travel-form select#city').selectpicker('refresh');
        })

        Livewire.on('updatedDataCities', () => {
            $('.booking-travel-form select#city option').remove();
            $('.booking-travel-form select#city')
                .append('<option value="0">tadt wählen</option>');
            Object.keys(@this.dataCities).forEach(id => {
                $('.booking-travel-form select#city')
                    .append('<option value="' + id + '">' + @this.dataCities[id] + '</option>');
            });
            $('.booking-travel-form select#city').selectpicker('refresh');
        })

        Livewire.on('updatedDataTripThemeType', () => {
            $('.booking-travel-form select#trip_theme_type option').remove();
            $('.booking-travel-form select#trip_theme_type')
                .append('<option value="0">Reiseart wählen</option>');
            Object.keys(@this.dataTripThemeType).forEach(id => {
                $('.booking-travel-form select#trip_theme_type')
                    .append('<option value="' + id + '">' + @this.dataTripThemeType[id] + '</option>');
            });
            $('.booking-travel-form select#trip_theme_type').selectpicker('refresh');
        })


    </script>
@endpush
