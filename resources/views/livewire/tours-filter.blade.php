<div>
    <fieldset @class(['row align-items-center', 'pb-0' => $countryCode || $tripTheme])>
        <div class="col-sm-4 col-md-4">
            <div class="form-group mb-0" wire:ignore>
                <select
                    wire:model="countryCode"
                    id="filter_country_code"
                    class="selectpicker ignore">
                        <option>{{ __('booking.select_country') }}</option>
                    @foreach($countries as $code => $country)
                        <option
                            value="{{ $code }}"
                            @if($code === $countryCode) 'selected' @endif>
                            {{ __('country.'.$country) }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-sm-4 col-md-4">
            <div class="form-group mb-0" wire:ignore>
                <select
                    wire:model="tripTheme"
                    id="filter_trip_theme"
                    class="selectpicker ignore">
                        <option>{{ __('booking.select_trip_theme') }}</option>
                    @foreach($tripThemes as $id => $title)
                        <option
                            value="{{ $id }}"
                            @if($id === $tripTheme) 'selected' @endif
                        >
                            {{ __('booking.'.$title) }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-sm-4 col-md-4">
             <button type="button"
                     class="theme-btn pull-right"
                     wire:click="filter">
                 <i class="far fa-search"></i>{{ __('main.find') }}
             </button>
        </div>

    </fieldset>
    @if($countryCode || $tripTheme)
        <div class="row">
            <div class="col text-left ml-3">
                <button type="button"
                        class="btn-light bg-transparent small"
                        wire:click="clear"
                >
                <i class="far fa-times"></i> {{ __('main.clear_filter') }}
                </button>
            </div>
        </div>
    @endif
</div>

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.selectpicker').selectpicker();
        });
    </script>
@endpush
