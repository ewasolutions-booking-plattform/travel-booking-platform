@extends('layouts.main')

@section('content')
    <!-- banner-section -->
    <section class="banner-section" style="background-image: url({{ asset('/images/banner/banner-1.jpg')}});">
        <div class="pattern-layer" style="background-image: url({{ asset('/images/shape/shape-1.png')}});"></div>
        <div class="auto-container">
            <div class="content-box">
                <div id="booking-filter" class="container booking-travel-form">
                    <livewire:booking/>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->

    <!-- tour-section -->
    <section class="tour-section bg-color-1 sec-pad">
        <div class="pattern-layer" style="background-image: url({{ asset('/images/shape/shape-4.png')}});"></div>
        <div class="auto-container">
            <div class="sec-title text-center">
                <p>Modern & Beautiful</p>
                <h2>Our Most Popular Adventures</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 tour-block">
                    <div class="tour-block-one wow fadeInUp animated animated" data-wow-delay="00ms"
                         data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="{{ asset('/images/tour/tour-1.jpg')}}" alt="">
                                <a href="/tour-details/"><i class="fas fa-link"></i></a>
                            </figure>
                            <div class="lower-content">
                                <h3><a href="/tour-details/">MIKE WIEGELE: VON KÄRNTEN NACH KANADA</a></h3>
                                <h4>CHF 1700.00<span> / Per person</span></h4>
                                <ul class="info clearfix">
                                    <li><i class="far fa-clock"></i>5 Days</li>
                                    <li><i class="far fa-map"></i>G87P, Birmingham</li>
                                </ul>
                                <p>Lorem ipsum dolor amet consectetur adipiscing sed.</p>
                                <div class="btn-box">
                                    <a href="/tour-details/">See Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 tour-block">
                    <div class="tour-block-one wow fadeInUp animated animated" data-wow-delay="300ms"
                         data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="{{ asset('/images/tour/tour-2.jpg')}}" alt="">
                                <a href="/tour-details/"><i class="fas fa-link"></i></a>
                            </figure>
                            <div class="lower-content">
                                <h3><a href="/tour-details/">Blue River Resort @ Mike Wiegele Helicopter Skiing</a></h3>
                                <h4>CHF 1900.00<span> / Per person</span></h4>
                                <ul class="info clearfix">
                                    <li><i class="far fa-clock"></i>5 Days</li>
                                    <li><i class="far fa-map"></i>G87P, Birmingham</li>
                                </ul>
                                <p>Lorem ipsum dolor amet consectetur adipiscing sed.</p>
                                <div class="btn-box">
                                    <a href="/tour-details/">See Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 tour-block">
                    <div class="tour-block-one wow fadeInUp animated animated" data-wow-delay="600ms"
                         data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="{{ asset('/images/tour/tour-3.jpg')}}" alt="">
                                <a href="/tour-details/"><i class="fas fa-link"></i></a>
                            </figure>
                            <div class="lower-content">
                                <h3><a href="/tour-details/">
                                        Albreda Lodge @ Mike Wiegele Helicopter Skiing</a></h3>
                                <h4>CHF 2100.00<span> / Per person</span></h4>
                                <ul class="info clearfix">
                                    <li><i class="far fa-clock"></i>5 Days</li>
                                    <li><i class="far fa-map"></i>G87P, Birmingham</li>
                                </ul>
                                <p>Lorem ipsum dolor amet consectetur adipiscing sed.</p>
                                <div class="btn-box">
                                    <a href="/tour-details/">See Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- tour-section end -->


    <!-- deals-section -->
    <section class="deals-section" style="background-image: url({{ asset('/images/background/deals-1.jpg')}});">
        <div class="auto-container">
            <div class="content_block_2">
                <div class="content-box">
                    <h3>Group Travel <br/>to Island</h3>
                    <div class="price"><h4>CHF 2500</h4>
                        <del>CHF 3500</del>
                    </div>
                    <p>Lorem ipsum dolor amet consectetur adipiscing sed do eiusmod tempor ux incidunt labore dolore
                        magna aliqua Quis ipsum suspen.</p>
                    <a href="/tour-details/" class="theme-btn">See Details</a>
                </div>
            </div>
        </div>
    </section>
    <!-- deals-section end -->


    <!-- place-section -->
    <section class="place-section sec-pad">
        <div class="anim-icon">
            <div class="icon anim-icon-1"
                 style="background-image: url({{ asset('/images/icons/anim-icon-1.png')}});"></div>
            <div class="icon anim-icon-2" style="background-image: url({{ asset('/images/shape/shape-3.png')}});"></div>
            <div class="icon anim-icon-3" style="background-image: url({{ asset('/images/shape/shape-3.png')}});"></div>
        </div>
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 title-column">
                    <div class="sec-title">
                        <p>Choose Your place</p>
                        <h2>Popular Destinations</h2>
                    </div>
                    <div class="text">
                        <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiu smod tempor incididunt.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 place-block">
                    <div class="place-block-one">
                        <div class="inner-box">
                            <figure class="image-box"><img src="{{ asset('/images/resource/place-1.jpg')}}" alt="">
                            </figure>
                            <div class="text">
                                <h3><a href="/destinations/">Kanada</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 place-block">
                    <div class="place-block-one">
                        <div class="inner-box">
                            <figure class="image-box"><img src="{{ asset('/images/resource/place-1.jpg')}}" alt="">
                            </figure>
                            <div class="text">
                                <h3><a href="/destinations/">Island</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 place-block">
                    <div class="place-block-one">
                        <div class="inner-box">
                            <figure class="image-box"><img src="{{ asset('/images/resource/place-1.jpg')}}" alt="">
                            </figure>
                            <div class="text">
                                <h3><a href="/destinations/">Alaska</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 place-block">
                    <div class="place-block-one">
                        <div class="inner-box">
                            <figure class="image-box"><img src="{{ asset('/images/resource/place-1.jpg')}}" alt="">
                            </figure>
                            <div class="text">
                                <h3><a href="/destinations/">Turkei</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 link-column">
                    <div class="link-box centred">
                        <h3>Find All <br/>Destination</h3>
                        <a href="/destination/" class="theme-btn">Find Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- place-section end -->

    <!-- funfact-section -->
    <section class="funfact-section sec-pad centred">
        <div class="anim-icon">
            <div class="icon anim-icon-1" style="background-image: url({{ asset('/images/shape/shape-3.png')}});"></div>
            <div class="icon anim-icon-2" style="background-image: url({{ asset('/images/shape/shape-3.png')}});"></div>
        </div>
        <div class="auto-container">
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6 col-sm-12 counter-block">
                        <div class="counter-block-one">
                            <div class="inner-box">
                                <div class="pattern"
                                     style="background-image: url({{ asset('/images/shape/shape-5.png')}});"></div>
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="1500" data-stop="2000">0</span><span>+</span>
                                    <p>Awesome Hikers</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 counter-block">
                        <div class="counter-block-one">
                            <div class="inner-box">
                                <div class="pattern"
                                     style="background-image: url({{ asset('/images/shape/shape-6.png')}});"></div>
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="1500" data-stop="70">0</span><span>+</span>
                                    <p>Stunning Places</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 counter-block">
                        <div class="counter-block-one">
                            <div class="inner-box">
                                <div class="pattern"
                                     style="background-image: url({{ asset('/images/shape/shape-7.png')}});"></div>
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="1500" data-stop="1200">0</span><span>+</span>
                                    <p>Miles to Hike</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 counter-block">
                        <div class="counter-block-one">
                            <div class="inner-box">
                                <div class="pattern"
                                     style="background-image: url({{ asset('/images/shape/shape-8.png')}});"></div>
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="1500" data-stop="15">0</span>
                                    <p>Years in Service</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- funfact-section end -->


    <!-- testimonial-section -->
    <section class="testimonial-section sec-pad centred">
        <div class="auto-container">
            <div class="sec-title centred">
                <p>Review & Testimonials</p>
                <h2>Top Reviews for EWA Solutions</h2>
            </div>
            <div class="three-item-carousel owl-carousel owl-theme owl-nav-none">
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});"></div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Mike Hardson</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Amy Johnson</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Luaka Smith</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Mike Hardson</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Amy Johnson</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Luaka Smith</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Mike Hardson</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}});">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Amy Johnson</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <ul class="rating-box clearfix">
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                            <li><i class="icon-Star"></i></li>
                        </ul>
                        <div class="text">
                            <div class="icon"
                                 style="background-image: url({{ asset('/images/icons/quote-1.png')}};);">
                            </div>
                            <p>Lorem ipsum dolor amet consectet adipiscing sed do eiusmod tempor incididunt labore et
                                dolore magna aliqua ipsum suspen disse ultrices gravida Risus</p>
                        </div>
                        <div class="author-box">
                            <h4>Luaka Smith</h4>
                            <span class="designation">Traveler</span>
                            <figure class="thumb-box"><img src="{{ asset('/images/resource/testimonial-1.jpg')}}"
                                                           alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
