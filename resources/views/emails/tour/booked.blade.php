
<h1>Dear {{ $booking->fullName }}</h1>

<table>
    <tr>
        <td>{{ __('booking.country') }}:</td><td>{{ __('country.'.$booking->country->name) }}</td>
    </tr>
    <tr>
        <td>{{ __('booking.region') }}:</td><td>{{ $booking->region->name }}</td>
    </tr>
    <tr>
        <td>{{ __('booking.trip_theme') }}:</td><td>{{ __('booking.' . $booking->tripTheme->title) }}</td>
    </tr>
    <tr>
        <td>{{ __('booking.trip_theme_type') }}:</td><td>{{ __('booking.'.$booking->tripThemeType->title) }}</td>
    </tr>
    <tr>
        <td>{{ __('booking.duration') }}:</td><td>{{ $booking->duration_count }} {{ __($booking->duration_type) }}</td>
    </tr>
    <tr>
        <td>{{ __('booking.budget') }}:</td><td>{{ $booking->showBudget }}</td>
    </tr>
</table>

@if(!$booking->tour)
    <h2>{{ __('booking.not_found_tour') }}</h2>
@else
    @php
        $tour = $booking->tour;
    @endphp

    <h2>{{ __('booking.found_tour') }}</h2>

    <table>
        <tr>
            <td>{{ __('booking.tour_title') }}:</td><td>{{ $tour->title }}</td>
        </tr>
        <tr>
            <td>{{ __('booking.trip_theme') }}:</td><td>{{ $tour->tripTheme->title }}</td>
        </tr>
        <tr>
            <td>{{ __('booking.trip_theme_type') }}:</td><td>{{ $tour->tripThemeType->title }}</td>
        </tr>
        <tr>
            <td>{{ __('booking.tour_start_date') }}:</td><td>{{ $tour->start_trip_date->format('Y-m-d') }}</td>
        </tr>
        <tr>
            <td>{{ __('booking.tour_end_date') }}:</td><td>{{ $tour->end_trip_date->format('Y-m-d') }}</td>
        </tr>
        <tr>
            <td>{{ __('booking.duration') }}:</td><td>{{ $tour->duration }} day(s)</td>
        </tr>
        <tr>
            <td>{{ __('booking.country') }}:</td><td>{{ __('country.'.$tour->country->name) }}</td>
        </tr>
        <tr>
            <td>{{ __('booking.region') }}:</td><td>{{ $tour->region->name }}</td>
        </tr>
        <tr>
            <td>{{ __('booking.price') }}:</td><td>{{ $tour->price }}</td>
        </tr>

        @foreach($tour->products as $product)
            <tr>
                <td>{{ __('booking.supplier') }}:</td><td>{{ $product->supplier->name }}</td>
            </tr>
            <tr>
                <td>{{ __('booking.activities') }}:</td>
                <td>
                    @foreach($product->supplier->activities as $activity)
                        {{ $activity->name }},
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>{{ __('booking.amenities') }}:</td>
                <td>
                    @foreach($product->supplier->amenities as $amenities)
                        {{ $amenities->name }},
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>{{ __('booking.product') }}:</td><td>{{ $product->name }}</td>
            </tr>
            <tr>
                <td>{{ __('booking.product_characteristics') }}:</td>
                <td>
                    @foreach($product->characteristics as $characteristic)
                        {{ __('booking.'.$characteristic) }},
                    @endforeach
                </td>
        @endforeach
    </table>

@endif
