@extends(backpack_view('crud.show'))

@section('content')
    @parent

    <h2>Available Hotel</h2>

    <div class="row">
        <div class="{{ $crud->getShowContentClass() }}">
            <livewire:admin.booking.suppliers :booking="$booking" :suppliers="$suppliers"/>
        </div>
    </div>
@endsection


@section('after_scripts')
    @parent
    @livewireScripts
    <script>
        window.addEventListener('show-modal', () => $('#modal').modal('show'));
        window.addEventListener('hide-modal', () => $('#modal').modal('hide'));
    </script>
@endsection

@livewire('admin.booking.create-tour')
