<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('booking') }}'><i class='nav-icon la la-question'></i> Booking offers</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tour') }}'><i class='nav-icon la la-question'></i> Tours</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('page') }}'><i class='nav-icon la la-file-o'></i> <span>Pages</span></a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('create-tour') }}'><i class='nav-icon la la-question'></i> Create Tours</a></li>

@role('administrator')
    <li class="nav-title">Dictionaries</li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-question"></i> Trip infos</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('age-category') }}'><i class='nav-icon la la-users-cog'></i> Age categories</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('trip-theme') }}'><i class='nav-icon la la-question'></i> Trip themes</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('trip-theme-type') }}'><i class='nav-icon la la-question'></i> Trip theme types</a></li>
        </ul>
    </li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class='nav-icon la la-globe-europe'></i> Locations</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('continent') }}'><i class='nav-icon la la-globe'></i> Continents</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('country') }}'><i class='nav-icon la la-flag'></i> Countries</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('region') }}'><i class='nav-icon la la-building'></i> Regions</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('city') }}'><i class='nav-icon la la-city'></i> Cities</a></li>
        </ul>
    </li>

    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class='nav-icon la la-building-o'></i> JONVIEW</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('supplier') }}'><i class='nav-icon la la-address-card'></i> Suppliers</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product-type') }}'><i class='nav-icon la la-arrows-alt'></i> Product types</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product-type-group') }}'><i class='nav-icon la la-layer-group'></i> Product type groups</a></li>
        </ul>
    </li>

    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
        </ul>
    </li>
@endrole
