@extends(backpack_view('blank'))

@section('after_styles')
    @livewireStyles
    <style media="screen">
        .backpack-profile-form .required::after {
            content: ' *';
            color: red;
        }
    </style>
@endsection

@php
    $breadcrumbs = [
        trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
        trans('backpack::base.search_tour') => false,
    ];
@endphp

@section('header')
    <section class="content-header">
        <div class="container-fluid mb-3">
            <h1>{{ trans('backpack::base.search_tour') }} {{ $user->hasRole('operator') && $user->profile ? '(for '.$user->profile->title.')' : '' }}</h1>
        </div>
    </section>
@endsection

@section('content')

    @if (session('success'))
        <div class="col-lg-8">
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        </div>
    @endif

    @if ($errors->count())
        <div class="col-lg-8">
            <div class="alert alert-danger">
                <ul class="mb-1">
                    @foreach ($errors->all() as $e)
                        <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <div class="card">

        @livewire('admin.tour')

    </div>
@endsection

@section('after_content_widgets')
    @livewireScripts
    <script>
        window.addEventListener('show-booking-modal', event => {
            $('#selectBookingModal').modal('show');
        });
        window.addEventListener('show-tour-modal', event => {
            $('#createTourModal').modal('show');
        });
        window.addEventListener('hide-append-modal', event => {
            $('#selectBookingModal').modal('hide');
        });
        window.addEventListener('hide-tour-modal', event => {
            $('#createTourModal').modal('hide');
        });
    </script>
@endsection

@livewire('admin.create-tour')
@livewire('admin.append-to-booking')
