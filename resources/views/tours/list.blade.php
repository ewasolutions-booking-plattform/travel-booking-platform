@extends('layouts.main')

@section('content')
    <section class="page-title style-two centred" style="background-image: url({{asset('/images/background/page-title-2.jpg')}});">
        <div class="auto-container">

            <div class="content-box">
                <h1>{{ __('main.tours') }}</h1>
            </div>

            <div class="form-inner p-0">
                <div class="container booking-travel-form">
                    <livewire:tours-filter
                        :country="$selectedCountry"
                        :theme="$selectedTheme"
                        :sort="$selectedSort" />
                </div>
            </div>

        </div>
    </section>

    <section class="tours-page-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 content-side">
                    <div class="item-shorting clearfix">
                        <div class="left-column pull-left">
                            <h3>{{ __('main.showing') }} {{ $tours->total() }}</h3>
                        </div>
                        <div class="right-column pull-right clearfix">
                            <div class="container booking-travel-form pr-0">
                                    <livewire:tours-sort
                                        :country="$selectedCountry"
                                        :theme="$selectedTheme"
                                        :sort="$selectedSort" />
                            </div>
                        </div>
                    </div>
                    <div class="wrapper grid">
                        <div class="tour-grid-content">
                            <div class="row clearfix">
                                @foreach($tours as $tour)
                                    <div class="col-lg-4 col-md-6 col-sm-12 tour-block">
                                        <div class="tour-block-one wow fadeInUp animated animated" data-wow-delay="00ms"
                                             data-wow-duration="1500ms">
                                            <div class="inner-box">

                                                <figure class="image-box">
                                                    <img src="{{ $supplierService->getFirstImage($tour->products()->first()->supplier) }}" alt="">
                                                    <a href="{{ route('tour', $tour->slug) }}"><i class="fas fa-link"></i></a>
                                                </figure>

                                                <div class="lower-content">
                                                    <h3><a href="{{ route('tour', $tour->slug) }}">{{ $tour->title }}</a></h3>
                                                    <h4>{{ $tour->price }}<span> / Per person</span></h4>
                                                    <ul class="info clearfix">
                                                        @php $duration = $tour->end_trip_date->diffInDays($tour->start_trip_date) @endphp
                                                        <li><i class="far fa-clock"></i>{{ $duration > 0 ? $duration : 1 }} {{ __('main.days') }}</li>
                                                        <li><i class="far fa-map"></i>{{ __('country.' . $tour->country->name) }}, {{ $tour->region->code }}</li>
                                                    </ul>
                                                    {!! $tour->description !!}
                                                    <div class="btn-box">
                                                        <a href="{{ route('tour', $tour->slug) }}">{{ __('main.details') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    @if ($tours->hasPages())
                        <div class="pagination-wrapper">
                            <ul class="pagination clearfix">
                                @if (!$tours->onFirstPage())
                                    <li><a href="{{ $tours->previousPageUrl() }}"><i class="icon-Left-Arrow"></i></a></li>
                                @endif

                                @for($i = 1; $i <= $tours->lastPage(); $i++)
                                    @if ($i === $tours->currentPage())
                                        <li><a href="#" class="current">{{ $i }}</a></li>
                                    @else
                                        <li><a href="{{ $tours->url($i) }}">{{ $i }}</a></li>
                                    @endif
                                @endfor

                                @if ($tours->hasMorePages())
                                    <li><a href="{{ $tours->nextPageUrl() }}"><i class="icon-Right-Arrow"></i></a></li>
                                @endif
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
