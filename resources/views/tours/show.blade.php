@php
$pageTitle = $tour->title ?? 'Tour details';
@endphp

@extends('layouts.main')

@section('content')

    <section class="page-title style-three" style="background-image: url({{asset('/images/background/page-title-3.jpg')}});">
        <div class="auto-container">
            <div class="inner-box">
                <h2>{{ $tour->title }}</h2>
                <h3>{{ $tour->price }}<span> / {{__('main.per_person')}}</span></h3>
            </div>
        </div>
    </section>

    <section class="tour-details">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                    <div class="tour-details-content">
                        <div class="inner-box">
                            <div class="text">
                                <h2>{{ __('main.overview') }}</h2>
                                {!! $tour->description ?? $tour->products->first()->supplier->description !!}
                            </div>
                        </div>
                        <div class="overview-inner">
                            <h4>{{ __('main.destination') }}</h4>
                            <div>{{ $tour->country->name }}</div>

                            @foreach($tour->products as $product)
                                <h2>{{ $product->supplier->name }}</h2>

                                @if($product->supplier->activities()->count())
                                    <h4>{{ __('booking.activities') }}</h4>
                                    <div>{{ $product->supplier->showActivities }}</div>
                                @endif

                                @if($product->supplier->amenities()->count())
                                    <h4>{{ __('booking.amenities') }}</h4>
                                    <div>{{ $product->supplier->showAmenities }}</div>
                                @endif

                                @foreach($product->characteristics as $characteristic)
                                    <h4>{{ Str::title(__('booking.' . $characteristic->name)) }}</h4>
                                    <div>{{ $characteristic->pivot->value }}</div>
                                @endforeach


                            @endforeach
                        </div>

                        <div class="location-map">
                            <div class="text">
                                <h2>{{ __('main.view_map') }}</h2>
                            </div>
                            <div class="map-inner">
                                <div
                                    class="google-map"
                                    id="contact-google-map"
                                    data-map-lat="{{$tour->products->first()->supplier->latitude}}"
                                    data-map-lng="{{$tour->products->first()->supplier->longitude}}"
                                    data-icon-path="{{asset('/images/icons/map-marker.png')}}"
                                    data-map-title="{{ $tour->title }}"
                                    data-map-zoom="12"
                                    data-markers='{
                                            "marker-1": [{{$tour->products->first()->supplier->latitude}}, {{$tour->products->first()->supplier->longitude}} , "{{ $tour->country->name }} : {{ $tour->title }}","{{asset('/images/icons/map-marker.png')}}"]
                                        }'>
                                </div>
                            </div>
                        </div>

                        @if($tour->products->first()->supplier->images()->count())
                        <div class="photo-gallery">
                            <div class="text">
                                <h2>{{ __('main.photo_gallery') }}</h2>
                            </div>
                            <div class="image-box clearfix">
                                @foreach($tour->products->first()->supplier->images as $k => $photo)
                                    @if(0 === $k%3 && 0 !== $k)
                                        <figure class="image">
                                            <img src="{{ $photo->path }}" alt="">
                                            <a href="{{ $photo->path }}" class="view-btn lightbox-image" data-fancybox="gallery"><i class="icon-Plus"></i></a>
                                        </figure>
                                    @else
                                    <figure class="image">
                                        <img src="{{ $photo->path }}" alt="">
                                        <a href="{{ $photo->path }}" class="view-btn lightbox-image" data-fancybox="gallery"><i class="icon-Plus"></i></a>
                                    </figure>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                    <div class="default-sidebar tour-sidebar ml-20">
                        <div class="advice-widget">
                            <div class="inner-box" style="background-image: url({{asset('/images/resource/advice-1.jpg')}});">
                                <div class="text">
                                    <h2>Get <br />25% Off <br />On {{ __('country.' . $tour->country->name) }}, {{ $tour->city->name }}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
