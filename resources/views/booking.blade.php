@php
    $tour = $booking->tour;
@endphp

<link href="{{ asset('/css/booking-pdf.css')}}" rel="stylesheet">

<div class="container">
    <div class="logo-box">
        <div><img src="{{ asset('/images/ewa-logo.png')}}" alt=""></div>
    </div>
    <h1>{{ $booking->fullName }}: {{ $tour->title }}</h1>

    <h2 class="price">{{ $tour->price }}</h2>
    <h3>Overview</h3>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
        dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum.</p>

    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
        veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

    <div class="wrapper">
        <div class="header">
            <table>
                <tr>
                    <td border="0"><div>{{ $tour->duration }} day(s)</div></td>
                    <td border="0"><div>{{ $tour->start_trip_date->format('Y-m-d') }} - {{ $tour->end_trip_date->format('Y-m-d') }}</div></td>
                    <td border="0"><div>{{ $tour->season_of_year }}</div></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="wrapper">
        <div class="tour-info">
            <table>
                <tr>
                    <td><div>{{ __('booking.destination') }}:</div></td>
                    <td><div>{{ __('country.'. $tour->country->name) }}</div></td>
                </tr>
                <tr>
                    <td><div>{{ __('booking.departure') }}:</div></td>
                    <td><div> Yes Requard </div></td>
                </tr>
                <tr>
                    <td><div>{{ __('booking.departure_time') }}:</div></td>
                    <td><div> New York City </div></td>
                </tr>
                <tr>
                    <td><div>{{ __('booking.return_time') }}:</div></td>
                    <td><div> English & Spanish </div></td>
                </tr>
                <tr>
                    <td><div>{{ __('booking.included') }}:</div></td>
                    <td><div> Air fares </div></td>
                </tr>
                <tr>
                    <td><div>&nbsp;</div></td>
                    <td><div> 4 Nights Hotel Accomodation </div></td>
                </tr>
                <tr>
                    <td><div>&nbsp;</div></td>
                    <td><div> Entrance Fees </div></td>
                </tr>
                <tr>
                    <td><div>&nbsp;</div></td>
                    <td><div> Tour Guide </div></td>
                </tr>
                <tr>
                    <td><div>{{ __('booking.excluded') }}:</div></td>
                    <td><div> Air fares </div></td>
                </tr>
                <tr>
                    <td><div>&nbsp;</div></td>
                    <td><div> Air fares </div></td>
                </tr>
                <tr>
                    <td><div>&nbsp;</div></td>
                    <td><div> Air fares </div></td>
                </tr>
                <tr>
                    <td><div>&nbsp;</div></td>
                    <td><div> Air fares </div></td>
                </tr>
            </table>
        </div>
    </div>
    <h3>Get 25% Off On New York Tours</h3>
</div>
