jQuery(document).ready(function ($) {

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;

    setProgressBar(current);

    $(".nextBtn").click(function () {
        setProgressBar(current++);
    });

    $(".prevBtn").click(function () {
        setProgressBar(--current);
    });

    function setProgressBar(curStep) {
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width", percent + "%")
    }

    $(".submit").click(function () {
        return false;
    })

    $('.booking-travel-form select#trip_theme').selectpicker({noneSelectedText: 'Nichts ausgewählt'});
    $('.booking-travel-form select#trip_theme_type').selectpicker({noneSelectedText: 'Nichts ausgewählt'});
    $('.booking-travel-form select#region').selectpicker({noneSelectedText: 'Nichts ausgewählt'});
    $('.booking-travel-form select#city').selectpicker({noneSelectedText: 'Nichts ausgewählt'});
    $('input[name="daterange"]').daterangepicker();
});
