<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

use App\Http\Controllers\Admin\TourSearchController;

Route::group(
    [
        'prefix'     => config('backpack.base.route_prefix', 'admin'),
        'middleware' => array_merge(
            (array)config('backpack.base.web_middleware', 'web'),
            (array)config('backpack.base.middleware_key', 'admin')
        ),
        'namespace'  => 'App\Http\Controllers\Admin',
    ],
    function () { // custom admin routes
        Route::get('create-tour', [TourSearchController::class, 'index']);
        Route::crud('tour', 'TourCrudController');
        Route::crud('continent', 'ContinentCrudController');
        Route::crud('country', 'CountryCrudController');
        Route::crud('region', 'RegionCrudController');
        Route::crud('organizer', 'OrganizerCrudController');
        Route::crud('room-category', 'RoomCategoryCrudController');
        Route::crud('room-info', 'RoomInfoCrudController');
        Route::crud('room-type', 'RoomTypeCrudController');
        Route::crud('age-category', 'AgeCategoryCrudController');
        Route::crud('trip-info', 'TripInfoCrudController');
        Route::crud('trip-info-type', 'TripInfoTypeCrudController');
        Route::crud('trip-theme', 'TripThemeCrudController');
        Route::crud('trip-theme-type', 'TripThemeTypeCrudController');
        Route::crud('booking', 'BookingCrudController');
        Route::crud('product-type', 'ProductTypeCrudController');
        Route::crud('product-type-group', 'ProductTypeGroupCrudController');
        Route::crud('city', 'CityCrudController');
        Route::crud('hotel', 'HotelCrudController');
        Route::crud('supplier', 'SupplierCrudController');
    }
); // this should be the absolute last line of this file
