<?php

namespace App\Http\Requests;

use App\DTO\Requests\FilterDTO;
use App\Enums\SortTypesEnum;
use App\Models\Country;
use App\Models\TripTheme;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class FilterRequest.
 * @property string $country
 * @property string $theme
 * @property string $sort
 */
class FilterRequest extends FormRequest
{
    protected $redirect = '/';

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'country' => ['sometimes', 'required', 'exists:countries,code'],
            'theme'   => ['sometimes', 'required', 'exists:trip_themes,id'],
            'sort'    => ['sometimes', 'required', Rule::in(SortTypesEnum::toValues())],
        ];
    }

    public function getDTO(): FilterDTO
    {
        $country = Country::where(Country::FIELD_CODE, $this->country)->first();
        $theme = TripTheme::find($this->theme);

        return new FilterDTO(
            [
                'country' => $country,
                'theme'   => $theme,
                'sort'    => $this->sort,
            ]
        );
    }
}
