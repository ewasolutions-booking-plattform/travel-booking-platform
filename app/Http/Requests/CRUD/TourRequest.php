<?php

namespace App\Http\Requests\CRUD;

use App\Models\Continent;
use App\Models\Country;
use App\Models\Organizer;
use App\Models\Region;
use App\Models\RoomInfo;
use App\Models\Tour;
use App\Models\TripInfo;
use App\Models\TripTheme;
use App\Models\TripThemeType;
use App\Rules\TourDescription;
use Illuminate\Foundation\Http\FormRequest;

class TourRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            Tour::FIELD_TITLE           => ['required', 'min:5', 'max:255'],
            Tour::FIELD_DESCRIPTION     => ['nullable', 'string', new TourDescription()],
            Tour::FIELD_TRIP_START      => ['required'],
            Tour::FIELD_TRIP_END        => ['required'],
            Tour::FIELD_TRIP_THEME      => ['required', 'exists:'.TripTheme::tableName().',id'],
            Tour::FIELD_TRIP_THEME_TYPE => ['required', 'exists:'.TripThemeType::tableName().',id'],
        ];
    }
}
