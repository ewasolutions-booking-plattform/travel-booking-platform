<?php

namespace App\Http\Requests\CRUD;

use App\Models\ProductType;
use Illuminate\Foundation\Http\FormRequest;

class ProductTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ProductType::FIELD_CODE        => ['required', 'string', 'min:2'],
            ProductType::FIELD_DESCRIPTION => ['required', 'string'],
        ];
    }
}
