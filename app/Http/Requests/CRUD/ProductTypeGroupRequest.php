<?php

namespace App\Http\Requests\CRUD;

use App\Enums\RoleEnum;
use Illuminate\Foundation\Http\FormRequest;

class ProductTypeGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check() && backpack_user()->hasRole(RoleEnum::administrator()->value);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'min:5', 'max:255'],
        ];
    }
}
