<?php

namespace App\Http\Requests\CRUD;

use App\Models\Continent;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContinentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $continentID = $this->get('id') ?? request()->route('id');

        return [
            Continent::FIELD_NAME => [
                'required',
                'min:5',
                'max:100',
                Rule::unique(Continent::tableName(), Continent::FIELD_NAME)->ignore($continentID),
            ],
        ];
    }
}
