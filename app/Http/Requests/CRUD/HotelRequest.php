<?php

namespace App\Http\Requests\CRUD;

use App\Models\Hotel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            Hotel::FIELD_SUPPLIER_CODE => ['required', 'string'],
            Hotel::FIELD_NAME          => ['required', 'string'],
            Hotel::FIELD_DESCRIPTION   => ['required', 'string'],
            Hotel::FIELD_RATING        => ['required', 'string'],
            Hotel::FIELD_ADDRESS1      => ['required', 'string'],
            Hotel::FIELD_ADDRESS2      => ['nullable', 'required', 'string'],
            Hotel::FIELD_ADDRESS3      => ['nullable', 'required', 'string'],
            Hotel::FIELD_PHONE         => ['required', 'string'],
            Hotel::FIELD_FAX           => ['nullable', 'required', 'string'],
            Hotel::FIELD_CITY          => ['required', Rule::exists(Hotel::tableName(), 'id')],

        ];
    }
}
