<?php

namespace App\Http\Requests\CRUD;

use App\Models\TripInfo;
use App\Models\TripInfoType;
use Illuminate\Foundation\Http\FormRequest;

class TripInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            TripInfo::FIELD_NUMBER_IN_GROUP        => ['required', 'numeric', 'min:1'],
            TripInfo::FIELD_NUMBER_OF_GROUPS       => ['required', 'numeric', 'min:1'],
            TripInfo::FIELD_INFO_TYPE              => ['required', 'exists:'.TripInfoType::tableName().',id'],
            TripInfo::FIELD_DESCRIPTION            => ['required', 'string', 'min:50'],
            TripInfo::FIELD_INCLUDED_SERVICES      => ['required', 'string', 'min:50'],
            TripInfo::FIELD_ARRIVAL_DEPARTURE_INFO => ['required', 'string', 'min:50'],
            TripInfo::FIELD_SPECIAL_INFO           => ['required', 'string', 'min:50'],
            TripInfo::FIELD_LATITUDE               => ['required', 'numeric', 'between:-90,90'],
            TripInfo::FIELD_LONGITUDE              => ['required', 'numeric', 'between:-180,180'],
            TripInfo::FIELD_PHOTOS                 => ['sometimes', 'array'],
            TripInfo::FIELD_PHOTOS.'.*'            => ['nullable', 'file', 'mimes:jpg,jpeg,png', 'max:2048'],
        ];
    }
}
