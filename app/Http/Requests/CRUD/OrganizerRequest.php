<?php

namespace App\Http\Requests\CRUD;

use App\Models\Organizer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrganizerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $organizerID = $this->get('id') ?? request()->route('id');

        return [
            Organizer::FIELD_NAME => [
                'required',
                'min:3',
                'max:255',
                Rule::unique(Organizer::tableName(), Organizer::FIELD_NAME)->ignore($organizerID),
            ],
        ];
    }
}
