<?php

namespace App\Http\Requests\CRUD;

use App\Models\RoomType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoomTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roomTypeID = $this->get('id') ?? request()->route('id');

        return [
            RoomType::FIELD_TITLE => [
                'required',
                'min:5',
                Rule::unique(RoomType::tableName(), RoomType::FIELD_TITLE)->ignore($roomTypeID),
            ],
        ];
    }
}
