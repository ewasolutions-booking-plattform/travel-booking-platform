<?php

namespace App\Http\Requests\CRUD;

use App\Enums\CurrencyEnum;
use App\Models\Organizer;
use App\Models\RoomCategory;
use App\Models\RoomInfo;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoomInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            RoomInfo::FIELD_ORGANIZER   => ['required', 'exists:'.Organizer::tableName().',id'],
            RoomInfo::FIELD_CATEGORY    => ['required', 'exists:'.RoomCategory::tableName().',id'],
            RoomInfo::FIELD_BASE_PRISE  => ['required', 'numeric', 'min:1'],
            RoomInfo::FIELD_USER_PRISE  => ['required', 'numeric', 'min:1'],
            RoomInfo::FIELD_CURRENCY    => ['required', 'string', Rule::in(CurrencyEnum::toValues())],
            RoomInfo::FIELD_CONTENT_URL => ['required', 'string', 'url'],
            RoomInfo::FIELD_DESCRIPTION => ['required', 'string', 'min:50'],
            RoomInfo::FIELD_PRICE_INFO  => ['required', 'string', 'min:50'],
        ];
    }
}
