<?php

namespace App\Http\Requests\CRUD;

use App\Models\Continent;
use App\Models\Country;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $countryID = $this->get('id') ?? request()->route('id');
        return [
            Country::FIELD_NAME      => [
                'required',
                'min:5',
                'max:100',
                Rule::unique(Country::tableName(), Country::FIELD_NAME)->ignore($countryID),
            ],
            Country::FIELD_CONTINENT => ['required', 'exists:'.Continent::tableName().',id'],
        ];
    }
}
