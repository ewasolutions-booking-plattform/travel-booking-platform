<?php

namespace App\Http\Requests\CRUD;

use App\Models\RoomCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoomCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $roomCategoryID = $this->get('id') ?? request()->route('id');

        return [
            RoomCategory::FIELD_NAME => [
                'required',
                'min:5',
                'max:255',
                Rule::unique(RoomCategory::tableName(), RoomCategory::FIELD_NAME)->ignore($roomCategoryID),
            ],
        ];
    }
}
