<?php

namespace App\Http\Requests\CRUD;

use App\Models\TripInfoType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TripInfoTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $tripTypeID = $this->get('id') ?? request()->route('id');

        return [
            TripInfoType::FIELD_TITLE => [
                'required',
                'min:3',
                'max:255',
                Rule::unique(TripInfoType::tableName(), TripInfoType::FIELD_TITLE)->ignore($tripTypeID),
            ],
        ];
    }
}
