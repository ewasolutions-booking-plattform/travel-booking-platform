<?php

namespace App\Http\Requests\CRUD;

use App\Models\City;
use App\Models\Region;
use Illuminate\Foundation\Http\FormRequest;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            City::FIELD_CODE        => ['required', 'string', 'min:3'],
            City::FIELD_DESCRIPTION => ['required', 'string'],
            City::FIELD_REGION      => ['required', 'exists:'.Region::tableName().',id'],
        ];
    }
}
