<?php

namespace App\Http\Requests\CRUD;

use App\Enums\RoleEnum;
use App\Models\Profile;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    public function authorize(): bool
    {
        return backpack_auth()->check() && backpack_user()->hasRole(RoleEnum::operator()->value);
    }

    public function rules(): array
    {
        return [
            'title'  => ['required', 'string', 'min:5'],
            'domain' => ['required', 'string', Rule::unique(Profile::tableName(), Profile::FIELD_DOMAIN)],
        ];
    }
}
