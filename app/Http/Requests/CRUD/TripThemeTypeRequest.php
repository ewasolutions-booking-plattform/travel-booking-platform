<?php

namespace App\Http\Requests\CRUD;

use App\Models\TripTheme;
use App\Models\TripThemeType;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TripThemeTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $themeTypeID = $this->get('id') ?? request()->route('id');
        $themeID = $this->request->get(TripThemeType::FIELD_TRIP_THEME);
        $typeTitle = $this->request->get(TripThemeType::FIELD_TITLE);

        return [
            TripThemeType::FIELD_TITLE      => [
                'required',
                'min:5',
                'max:255',
                Rule::unique(TripThemeType::tableName())
                    ->where(function (Builder $query) use ($themeTypeID, $themeID, $typeTitle) {
                        return $query->where(TripThemeType::FIELD_TRIP_THEME, $themeID)
                            ->where(TripThemeType::FIELD_TITLE, $typeTitle)
                            ->where('id', '!=', $themeTypeID);
                    }),
            ],
            TripThemeType::FIELD_TRIP_THEME => ['required', 'exists:'.TripTheme::tableName().',id'],
        ];
    }
}
