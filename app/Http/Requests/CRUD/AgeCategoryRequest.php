<?php

namespace App\Http\Requests\CRUD;

use App\Models\AgeCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AgeCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ageCategoryID = $this->get('id') ?? request()->route('id');

        return [
            AgeCategory::FIELD_TITLE => [
                'required',
                'min:3',
                'max:32',
                Rule::unique(AgeCategory::tableName(), AgeCategory::FIELD_TITLE)->ignore($ageCategoryID),
            ],
        ];
    }
}
