<?php

namespace App\Http\Requests\CRUD;

use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $regionID = $this->get('id') ?? request()->route('id');
        $countryID = $this->request->get(Region::FIELD_COUNTRY);
        $regionName = $this->request->get(Region::FIELD_NAME);

        return [
            Region::FIELD_NAME    => [
                'required',
                'min:5',
                'max:100',
                Rule::unique(Region::tableName())
                    ->where(function (Builder $query) use ($regionID, $countryID, $regionName) {
                        return $query->where(Region::FIELD_COUNTRY, $countryID)
                            ->where(Region::FIELD_NAME, $regionName)
                            ->where('id', '!=', $regionID);
                    }),
            ],
            Region::FIELD_COUNTRY => ['required', 'exists:'.Country::tableName().',id'],
        ];
    }
}
