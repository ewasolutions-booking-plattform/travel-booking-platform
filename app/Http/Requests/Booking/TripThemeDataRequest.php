<?php

namespace App\Http\Requests\Booking;

use App\Models\TripTheme;
use App\Models\TripThemeType;

class TripThemeDataRequest
{
    public static function rules(): array
    {
        return [
            'tripTheme'     => ['required', 'exists:'.TripTheme::tableName().',id'],
            'tripThemeType' => ['required', 'exists:'.TripThemeType::tableName().',id'],
        ];
    }
}
