<?php

namespace App\Http\Requests\Booking;

use App\Models\Country;
use App\Models\Region;

/**
 * Class CountryRequest.
 *
 * @param int $country
 */
class LocationRequest
{
    public static function rules(): array
    {
        return [
            'country' => ['required', 'exists:'.Country::tableName().',id'],
            'region'  => ['required', 'exists:'.Region::tableName().',id'],
        ];
    }
}
