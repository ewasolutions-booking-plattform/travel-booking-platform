<?php

namespace App\Http\Requests\Booking;

class ContactData
{
    public static function rules(): array
    {
        return [
            'firstName' => ['required', 'string'],
            'lastName'  => ['required', 'string'],
            'email'     => ['required', 'email'],
            'phone'     => ['required', 'regex:/[0-9]{4} [0-9]{2} [0-9]{3} [0-9]{2} [0-9]{2}/'],
        ];
    }
}
