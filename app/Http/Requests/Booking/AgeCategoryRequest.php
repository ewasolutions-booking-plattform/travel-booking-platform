<?php

namespace App\Http\Requests\Booking;

use App\Models\AgeCategory;

class AgeCategoryRequest
{
    public static function rules(): array
    {
        return [
            'selectedAgeCategories' => ['required', 'array'],
            'selectedAgeCategories.1' => ['required', 'numeric', 'min:1', 'max:10'],
            'selectedAgeCategories.*' => ['required', 'numeric', 'min:0', 'max:10'],
        ];
    }

    public static function messages(): array
    {
        return [
            'selectedAgeCategories.1.required' => 'The number of persons must be greater than zero',
            'selectedAgeCategories.1.min' => 'The number of persons must be greater than zero'
        ];
    }

    public static function attributes(): array
    {
        $attributes = [];
        foreach (AgeCategory::all() as $ageCategory) {
            $attributes['selectedAgeCategories.'.$ageCategory->id] = $ageCategory->title;
        }

        return $attributes;
    }
}
