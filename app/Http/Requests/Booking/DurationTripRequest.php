<?php

namespace App\Http\Requests\Booking;

use App\Rules\MaxTripDateRange;

class DurationTripRequest
{
    public static function rules(): array
    {
        return [
            'tripRangeDate' => [
                'required',
                'regex:/[0-9]{2}.[0-9]{2}.[0-9]{4} - [0-9]{2}.[0-9]{2}.[0-9]{4}/',
                new MaxTripDateRange(),
            ],
        ];
    }
}
