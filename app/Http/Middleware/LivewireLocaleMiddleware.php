<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LivewireLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        app()->setLocale('de_CH');

        return $next($request);
    }
}
