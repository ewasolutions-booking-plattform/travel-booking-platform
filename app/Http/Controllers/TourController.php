<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterRequest;
use App\Models\Tour;
use App\Services\SupplierService;
use App\Services\TourService;

class TourController extends Controller
{
    public function index(FilterRequest $request, TourService $tourService, SupplierService $supplierService)
    {
        $filterDTO = $request->getDTO();

        return view('tours.list', [
            'tours'           => $tourService->searchTours($filterDTO),
            'supplierService' => $supplierService,
            'selectedCountry' => $filterDTO->country->code ?? '',
            'selectedTheme'   => $filterDTO->theme->id ?? 0,
            'selectedSort'    => $filterDTO->sort ?? '',
        ]);
    }

    public function show(Tour $tour)
    {
        if (!$tour->is_publish) {
            abort(404);
        }

        return view('tours.show', compact('tour'));
    }
}
