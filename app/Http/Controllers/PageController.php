<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Backpack\PageManager\app\Models\Page;

class PageController extends Controller
{
    private array $data = [];

    public function index($slug, $subs = null)
    {
        $page = Page::findBySlug($slug);

        if (!$page) {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $this->data['title'] = $page->title;
        $this->data['countries'] = Country::all();

        return view('pages.'.$page->template, $this->data);
    }
}
