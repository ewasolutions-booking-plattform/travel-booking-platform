<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Columns\TourColumn;
use App\Helpers\Fields\TourFiled;
use App\Helpers\Filters\TourFilter;
use App\Http\Requests\CRUD\TourRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Models\TripTheme;
use App\Models\TripThemeType;
use App\Models\Tour;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Class TourCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TourCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup(): void
    {
        CRUD::setModel(Tour::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/tour');
        CRUD::setEntityNameStrings('tour', 'tours');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        TourFilter::filters();
        TourColumn::columns();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation(): void
    {
        CRUD::setValidation(TourRequest::class);
        TourFiled::fields();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation(): void
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation(): void
    {
        TourColumn::columns();
        TourColumn::previewColumns();
    }

    private function getIDFromForm(array $data, string $fieldName): ?int
    {
        $form = data_get($data, 'form');
        if (null === $form) {
            return null;
        }
        $fieldData = Arr::where($form, function ($value) use ($fieldName) {
            if ($fieldName === data_get($value, 'name')) {
                return $value;
            }

            return [];
        });

        return data_get(Arr::first($fieldData), 'value');
    }

    public function fetchTripTheme()
    {
        return $this->fetch(TripTheme::class);
    }

    public function fetchTripThemeType(Request $request)
    {
        $tripThemeID = $this->getIDFromForm($request->all(), Tour::FIELD_TRIP_THEME);
        if (null === $tripThemeID) {
            return $this->fetch(TripThemeType::class);
        }

        return $this->fetch(TripThemeType::class)->where(TripThemeType::FIELD_TRIP_THEME, $tripThemeID);
    }

    public function fetchCountry()
    {
        return $this->fetch(Country::class);
    }

    public function fetchRegion(Request $request)
    {
        $countryID = $this->getIDFromForm($request->all(), Tour::FIELD_COUNTRY);
        if (null === $countryID) {
            return $this->fetch(Region::class);
        }

        return $this->fetch(Region::class)->where(Region::FIELD_COUNTRY, $countryID);
    }

    public function fetchCity(Request $request)
    {
        $regionID = $this->getIDFromForm($request->all(), Tour::FIELD_REGION);
        if (null === $regionID) {
            return $this->fetch(City::class);
        }

        return $this->fetch(City::class)->where(City::FIELD_REGION, $regionID);
    }
}
