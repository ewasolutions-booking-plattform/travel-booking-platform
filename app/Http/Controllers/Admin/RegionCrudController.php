<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Columns\RegionColumn;
use App\Helpers\Filters\RegionFilter;
use App\Models\Country;
use App\Models\Region;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class RegionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RegionCrudController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup(): void
    {
        CRUD::setModel(Region::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/region');
        CRUD::setEntityNameStrings('region', 'regions');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        RegionFilter::filters();
        RegionColumn::columns();
    }

    protected function setupShowOperation(): void
    {
        RegionColumn::columns();
    }

    public function fetchCountry()
    {
        return $this->fetch(Country::class);
    }
}
