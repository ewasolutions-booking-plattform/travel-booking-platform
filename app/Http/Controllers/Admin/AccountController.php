<?php

namespace App\Http\Controllers\Admin;

use Alert;
use App\Enums\RoleEnum;
use App\Http\Requests\CRUD\ProfileRequest;
use Backpack\CRUD\app\Http\Requests\AccountInfoRequest;
use Backpack\CRUD\app\Http\Requests\ChangePasswordRequest;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    protected $data = [];

    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    /**
     * @return Application|Factory|View
     */
    public function getAccountInfoForm(): View
    {
        $this->data['title'] = trans('backpack::base.my_account');
        $this->data['user'] = $this->guard()->user();

        return view(backpack_view('my_account'), $this->data);
    }

    /**
     * @param AccountInfoRequest $request
     *
     * @return RedirectResponse
     */
    public function postAccountInfoForm(AccountInfoRequest $request): RedirectResponse
    {
        $result = $this->guard()->user()->update($request->except(['_token']));

        if ($result) {
            Alert::success(trans('backpack::base.account_updated'))->flash();
        } else {
            Alert::error(trans('backpack::base.error_saving'))->flash();
        }

        return redirect()->back();
    }

    /**
     * @param ChangePasswordRequest $request
     *
     * @return RedirectResponse
     */
    public function postChangePasswordForm(ChangePasswordRequest $request): RedirectResponse
    {
        $user = $this->guard()->user();
        $user->password = Hash::make($request->new_password);

        if ($user->save()) {
            Alert::success(trans('backpack::base.account_updated'))->flash();
        } else {
            Alert::error(trans('backpack::base.error_saving'))->flash();
        }

        return redirect()->back();
    }

    public function postAccountProfileForm(ProfileRequest $request): RedirectResponse
    {
        $user = $this->guard()->user();

        if (!$user->hasRole(RoleEnum::operator()->value)) {
            abort(403, 'Profile allowed Operators only');
        }

        if (null === $user->profile) {
            $user->profile()->create($request->validated());
        } else {
            $user->profile()->update($request->validated());
        }

        return redirect()->back();
    }

    /**
     * Get the guard to be used for account manipulation.
     *
     * @return StatefulGuard
     */
    protected function guard(): StatefulGuard
    {
        return backpack_auth();
    }
}
