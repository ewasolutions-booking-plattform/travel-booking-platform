<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Columns\CountryColumn;
use App\Helpers\Filters\CountryFilter;
use App\Models\Continent;
use App\Models\Country;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CountryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CountryCrudController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use FetchOperation;
    use InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup(): void
    {
        CRUD::setModel(Country::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/country');
        CRUD::setEntityNameStrings('country', 'countries');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        CountryFilter::filters();
        CountryColumn::columns();
        CRUD::setFromDb();
    }

    protected function setupShowOperation(): void
    {
        CountryColumn::columns();
    }

    public function fetchContinent()
    {
        return $this->fetch(Continent::class);
    }
}
