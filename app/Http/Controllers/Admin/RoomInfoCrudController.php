<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Columns\RoomInfoColumn;
use App\Helpers\Fields\RoomInfoField;
use App\Helpers\Filters\RoomInfoFilter;
use App\Http\Requests\CRUD\RoomInfoRequest;
use App\Models\RoomCategory;
use App\Models\Organizer;
use App\Models\RoomInfo;
use App\Models\RoomType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OrganizerRoomCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RoomInfoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use FetchOperation;
    use InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup(): void
    {
        CRUD::setModel(RoomInfo::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/room-info');
        CRUD::setEntityNameStrings('room_info', 'rooms_infos');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation(): void
    {
        CRUD::setValidation(RoomInfoRequest::class);
        RoomInfoField::fields();
        CRUD::setFromDb();
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        CRUD::setFromDb();

        RoomInfoFilter::filters();
        RoomInfoColumn::columns();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation(): void
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation(): void
    {
        RoomInfoColumn::columns();
        CRUD::setFromDb();
        RoomInfoColumn::description();
        RoomInfoColumn::priceInfo();
    }

    public function fetchOrganizer()
    {
        return $this->fetch(Organizer::class);
    }

    public function fetchRoomType()
    {
        return $this->fetch(RoomType::class);
    }

    public function fetchRoomCategory()
    {
        return $this->fetch(RoomCategory::class);
    }
}
