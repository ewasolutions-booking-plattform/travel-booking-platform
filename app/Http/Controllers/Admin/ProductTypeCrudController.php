<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Columns\ProductTypeColumn;
use App\Helpers\Fields\ProductTypeField;
use App\Helpers\Filters\ProductTypeFilters;
use App\Http\Requests\CRUD\ProductTypeRequest;
use App\Models\ProductType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductTypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductTypeCrudController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use UpdateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(ProductType::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/product-type');
        CRUD::setEntityNameStrings('product_type', 'product_types');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        ProductTypeFilters::filters($this);
        ProductTypeColumn::columns();
        CRUD::setFromDb();
    }

    protected function setupShowOperation(): void
    {
        ProductTypeColumn::columns();
        CRUD::setFromDb();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation(): void
    {
        CRUD::setValidation(ProductTypeRequest::class);
        ProductTypeField::fields();
    }
}
