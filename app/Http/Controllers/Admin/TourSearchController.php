<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Controller;

class TourSearchController extends Controller
{
    protected array $data = [];

    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    /**
     * @return Application|Factory|View
     */
    public function index(): View
    {
        $this->data['user'] = auth()->user();

        return view('backpack::tour.search', $this->data);
    }
}
