<?php

namespace App\Http\Controllers\Admin;

use App\DTO\Jonview\SearchDTO;
use App\Enums\RoleEnum;
use App\Helpers\Columns\BookingColumn;
use App\Models\Booking;
use App\Services\ApiProvider\JonviewAPI;
use App\Services\SupplierService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;

/**
 * Class BookingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BookingCrudController extends CrudController
{
    use DeleteOperation;
    use ListOperation;
    use ShowOperation {
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     * @throws \Exception
     */
    public function setup(): void
    {
        CRUD::setModel(Booking::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/booking');
        CRUD::setEntityNameStrings('booking', 'bookings');
        $this->crud->setShowView('backpack::crud.booking-offer-show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        if (auth()->user()->hasRole(RoleEnum::operator()->value)) {
            $this->crud->query = $this->crud->query->where(Booking::FK_OPERATOR, auth()->user()->id);
        }
        BookingColumn::columns();
    }

    protected function setupShowOperation(): void
    {
        $this->setupListOperation();
    }

    public function show(int $id, SupplierService $supplierService, JonviewAPI $apiService)
    {
        $booking = Booking::findOrFail($id);
        $DTO = new SearchDTO(
            [
                'cityID'          => $booking->city->id,
                'cityCode'        => $booking->city->code,
                'productTypeCode' => 'FIT',
                'fromDate'        => Carbon::parse($booking->trip_start_date),
                'toDate'          => Carbon::parse($booking->trip_end_date),
                'people'          => [],
            ]
        );

        $apiService->search($DTO);
        $this->data['booking'] = $booking->id;
        $this->data['suppliers'] = $supplierService->search($DTO);

        return $this->traitShow($id);
    }
}
