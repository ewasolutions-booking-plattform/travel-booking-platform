<?php

namespace App\Http\Livewire;

use App\Enums\SortTypesEnum;
use Livewire\Component;

class ToursSort extends Component
{
    public int $tripTheme = 0;
    public string $countryCode = '';
    public string $sort = '';

    public array $sortTypes = [];

    public function mount(string $country = '', int $theme = 0, string $sort = ''): void
    {
        $this->countryCode = $country;
        $this->tripTheme = $theme;
        $this->sort = $sort;

        $this->sortTypes = SortTypesEnum::toValues();
    }

    public function render()
    {
        return view('livewire.tours-sort');
    }

    public function updatedSort(string $value): void
    {
        $queryParams = [];

        if ($this->countryCode) {
            $queryParams['country'] = $this->countryCode;
        }

        if ($this->tripTheme) {
            $queryParams['theme'] = $this->tripTheme;
        }

        if ($this->sort) {
            $queryParams['sort'] = $this->sort;
        } else {
            $this->redirect(route('tours', $queryParams));
        }

        if (count($queryParams) > 0) {
            $this->redirect(route('tours', $queryParams));
        }
    }
}
