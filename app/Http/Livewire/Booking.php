<?php

namespace App\Http\Livewire;

use App\DTO\Jonview\SearchDTO;
use App\Events\TourBookedEvent;
use App\Http\Requests\Booking\AgeCategoryRequest;
use App\Http\Requests\Booking\ContactData;
use App\Http\Requests\Booking\LocationRequest;
use App\Http\Requests\Booking\DurationTripRequest;
use App\Http\Requests\Booking\TripThemeDataRequest;
use App\Models\AgeCategory;
use App\Models\Booking as BookingModel;
use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Models\Tour;
use App\Models\TripTheme;
use App\Models\TripThemeType;
use App\Services\ApiProvider\JonviewAPI;
use App\Services\TourService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class Booking extends Component
{
    private const MIN_AGA_COUNT = 0;
    private const MAX_AGA_COUNT = 10;

    public int $currentStep = 1;
    public int $country;
    public int $region;
    public int $city;
    public array $selectedAgeCategories;
    public string $tripRangeDate;
    public ?Carbon $tripStartDate;
    public ?Carbon $tripEndDate;
    public int $tripTheme;
    public string $tripThemeType;
    public int $budget;
    public string $firstName;
    public string $lastName;
    public string $email;
    public string $phone;
    public ?Tour $tour;

    public array $dataCountries;
    public array $dataRegions;
    public array $dataCities;
    public array $dataAgeCategories;
    public array $dataTripTheme;
    public array $dataTripThemeType;
    public string $minDate;
    public string $startDate;
    public string $endDate;

    public string $successMsg = '';

    public function render()
    {
        $this->refreshData();

        return view('livewire.booking');
    }

    public function mount()
    {
        $today = Carbon::now();
        $this->dataCountries = Country::pluck(Country::FIELD_NAME, 'id')->all();
        $this->dataRegions = [];
        $this->dataCities = [];
        $this->dataAgeCategories = AgeCategory::all()->toArray();
        $this->dataTripTheme = TripTheme::all()->toArray();
        $this->minDate = $today->format('d.m.Y');
        $this->startDate = $today->format('d.m.Y');
        $this->endDate = $today->addDays(7)->format('d.m.Y');

        $this->refreshData();

        $this->clearForm();
    }

    public function updatedCountry(int $value): void
    {
        if ($value > 0) {
            $this->dataRegions = Region::where(Region::FIELD_COUNTRY, $value)
                ->pluck(Region::FIELD_NAME, 'id')
                ->all();
            $this->dataCities = [];
            $this->emit('updatedDataRegions');
        } else {
            $this->dataRegions = [];
            $this->dataCities = [];
        }
    }

    public function updatedRegion(int $value): void
    {
        if ($value > 0) {
            $this->dataCities = City::where(City::FIELD_REGION, $value)
                ->pluck(City::FIELD_NAME, 'id')
                ->all();
            $this->emit('updatedDataCities');
        } else {
            $this->dataCities = [];
        }
    }

    public function updatedTripTheme(int $value): void
    {
        if ($value > 0) {
            $this->dataTripThemeType = TripThemeType::where(TripThemeType::FIELD_TRIP_THEME, $value)
                ->pluck(TripThemeType::FIELD_TITLE, 'id')
                ->all();

            $this->emit('updatedDataTripThemeType');
        }
    }

    public function oneStepSubmit(): void
    {
        $this->validate(LocationRequest::rules());

        $this->currentStep = 2;
    }

    public function stepDownAge(int $ageCategoryID): void
    {
        if ($this->selectedAgeCategories[$ageCategoryID] > self::MIN_AGA_COUNT) {
            $this->selectedAgeCategories[$ageCategoryID]--;
        }
    }

    public function stepUpAge(int $ageCategoryID): void
    {
        if ($this->selectedAgeCategories[$ageCategoryID] < self::MAX_AGA_COUNT) {
            $this->selectedAgeCategories[$ageCategoryID]++;
        }
    }

    public function twoStepSubmit(): void
    {
        $this->validate(AgeCategoryRequest::rules(), AgeCategoryRequest::messages());

        $this->validate(DurationTripRequest::rules());

        $rangeDates = explode('-', $this->tripRangeDate);
        $this->tripStartDate = Carbon::parse($rangeDates[0]);
        $this->tripEndDate = Carbon::parse($rangeDates[1]);

        $this->currentStep = 3;
    }

    public function fourStepSubmit(): void
    {
        $this->validate(TripThemeDataRequest::rules());

        $this->currentStep = 4;
    }

    public function fiveStepSubmit(TourService $tourService, JonviewAPI $apiService): void
    {
        $this->validate(['budget' => 'required']);

        $city = City::find($this->city);
        if (!$city) {
            Log::error('Can\'t find city');
            return;
        }
        $DTO = new SearchDTO(
            [
                'cityID'          => $city->id,
                'cityCode'        => $city->code,
                'productTypeCode' => 'FIT',
                'fromDate'        => $this->tripStartDate,
                'toDate'          => $this->tripEndDate,
                'people'          => $this->selectedAgeCategories,
            ]
        );
        $apiService->search($DTO);
        $this->tour = $tourService->searchTour(
            $this->country,
            $this->region,
            $this->city,
            $this->tripStartDate,
            $this->tripEndDate,
            $this->tripTheme,
            $this->tripThemeType,
            $this->budget
        );

        $this->currentStep = 5;
    }

    public function sendProposal(): void
    {
        $this->validate(ContactData::rules());

        $booking = BookingModel::create(
            [
                BookingModel::FIELD_CLIENT_FIRST_NAME => $this->firstName,
                BookingModel::FIELD_CLIENT_LAST_NAME  => $this->lastName,
                BookingModel::FIELD_CLIENT_EMAIL      => $this->email,
                BookingModel::FIELD_CLIENT_PHONE      => $this->phone,
                BookingModel::FIELD_COUNTRY           => $this->country,
                BookingModel::FIELD_REGION            => $this->region,
                BookingModel::FIELD_CITY              => $this->city,
                BookingModel::FIELD_TRIP_THEME        => $this->tripTheme,
                BookingModel::FIELD_TRIP_THEME_TYPE   => $this->tripThemeType,
                BookingModel::FIELD_BUDGET            => $this->budget,
                BookingModel::FIELD_TOUR              => $this->tour->id ?? null,
                BookingModel::FIELD_TRIP_START        => $this->tripStartDate,
                BookingModel::FIELD_TRIP_END          => $this->tripEndDate,
                BookingModel::FK_OPERATOR             => 2, //@TODO must get operator by host name in future
            ]
        );

        foreach ($this->selectedAgeCategories as $categoryID => $count) {
            if ($count) {
                $ageCategory = AgeCategory::find($categoryID);
                $booking->ageCategories()->attach($ageCategory, ['count' => $count]);
            }
        }

        event(new TourBookedEvent($booking));

        $this->successMsg = __('booking.msg_successful');

        $this->clearForm();

        $this->currentStep = 6;
    }

    public function back($step): void
    {
        $this->currentStep = $step;
    }

    private function refreshData(): void
    {
        if (!empty($this->tripTheme)) {
            $this->dataTripThemeType = TripThemeType::where(TripThemeType::FIELD_TRIP_THEME, $this->tripTheme)
                ->pluck(TripThemeType::FIELD_TITLE, 'id')
                ->all();
        } else {
            $this->dataTripThemeType = [];
        }

        if (!empty($this->country)) {
            $this->dataRegions = Region::where(Region::FIELD_COUNTRY, $this->country)
                ->pluck(Region::FIELD_NAME, 'id')
                ->all();
        } else {
            $this->dataRegions = [];
        }

        if (!empty($this->region)) {
            $this->dataCities = City::where(City::FIELD_REGION, $this->region)
                ->pluck(City::FIELD_NAME, 'id')
                ->all();
        } else {
            $this->dataCities = [];
        }
    }

    public function clearForm(): void
    {
        $this->country = 1;
        $this->region = 0;
        $this->city = 0;
        $this->budget = 5000;
        $this->tripTheme = 0;
        $this->tripThemeType = 0;
        $this->tripRangeDate = '';
        $this->tripStartDate = null;
        $this->tripEndDate = null;

        foreach ($this->dataAgeCategories as $ageCategory) {
            $this->selectedAgeCategories[$ageCategory['id']] = 1 === $ageCategory['id'] ? 2 : 0;
        }

        $this->tour = null;
        $this->firstName = '';
        $this->lastName = '';
        $this->email = '';
        $this->phone = '';
    }
}
