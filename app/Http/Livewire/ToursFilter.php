<?php

namespace App\Http\Livewire;

use App\Models\Country;
use App\Models\TripTheme;
use Livewire\Component;

class ToursFilter extends Component
{
    public int $tripTheme = 0;
    public string $countryCode = '';
    public string $sort = '';

    public array $countries = [];
    public array $tripThemes = [];

    public function mount(string $country = '', int $theme = 0, string $sort = ''): void
    {
        $this->countries = Country::pluck(Country::FIELD_NAME, Country::FIELD_CODE)->toArray();
        $this->tripThemes = TripTheme::pluck(TripTheme::FIELD_TITLE, 'id')->toArray();

        $this->countryCode = $country;
        $this->tripTheme = $theme;
    }

    public function render()
    {
        return view('livewire.tours-filter');
    }

    public function filter(): void
    {
        $queryParams = [];

        if ($this->countryCode) {
            $queryParams['country'] = $this->countryCode;
        }

        if ($this->tripTheme) {
            $queryParams['theme'] = $this->tripTheme;
        }

        if ($this->sort) {
            $queryParams['sort'] = $this->sort;
        }

        if (count($queryParams) > 0) {
            $this->redirect(route('tours', $queryParams));
        }
    }

    public function clear(): void
    {
        $this->redirect(route('tours'));
    }
}
