<?php

namespace App\Http\Livewire\Admin;

use App\DTO\Jonview\SearchDTO;
use App\DTO\TourDTO;
use App\Enums\AgeCategoryEnum;
use App\Services\ApiProvider\JonviewAPI;
use App\Services\SupplierService;
use App\Services\TourService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\App;
use Livewire\Component;
use Throwable;

class Tour extends Component
{
    public int $countryID = 0;
    public int $regionID = 0;
    public int $cityID = 0;
    public string $cityCode;
    public array $travelPeople;
    public string $travelStartDate;
    public string $travelEndDate;
    public string $productTypeCode;
    public array $searchResult;

    public $listeners = [
        'countryChanged',
        'regionChanged',
        'cityChanged',
        'travelPeopleChanged',
        'startDateUpdated',
        'endDateUpdated',
        'productTypeUpdated',
        'createTour',
    ];

    protected array $rules = [
        'countryID'             => ['required', 'exists:countries,id'],
        'regionID'              => ['required', 'exists:regions,id'],
        'cityID'                => ['required', 'exists:cities,id'],
        'cityCode'              => ['required', 'exists:cities,code'],
        'travelPeople'          => ['required', 'array'],
        'travelPeople.adult'    => ['required', 'integer', 'min:1', 'max:10'],
        'travelPeople.children' => ['required', 'integer', 'min:0', 'max:10'],
        'travelStartDate'       => ['required'],
        'travelEndDate'         => ['required'],
        'productTypeCode'       => ['required', 'exists:product_types,code'],
    ];

    protected array $messages = [
        'countryID.required'             => 'Please select Country',
        'regionID.required'              => 'Please select Region',
        'cityID.required'                => 'Please select City',
        'cityCode.required'              => 'Please select City',
        'travelPeople.adult.required'    => 'adult people is required',
        'travelPeople.adult.min'         => 'min :min',
        'travelPeople.adult.max'         => 'min :max',
        'travelPeople.children.required' => 'children is required',
        'travelPeople.children.min'      => 'min :min',
        'travelPeople.children.max'      => 'min :max',
        'travelStartDate.required'       => 'field is required',
        'travelEndDate.required'         => 'field is required',
        'productTypeCode'                => 'field is required',
    ];

    public function mount(): void
    {
        App::setLocale('en');
        $this->cityID = 0;
        $this->cityCode = '';
        foreach (AgeCategoryEnum::toValues() as $category) {
            $this->travelPeople[$category] = AgeCategoryEnum::adult()->value === $category ? 2 : 0;
        }

        $this->travelStartDate = '';
        $this->travelEndDate = '';
        $this->productTypeCode = '';
        $this->searchResult = [];
    }

    public function render()
    {
        return view('livewire.admin.tour');
    }

    public function countryChanged(int $countryID): void
    {
        $this->countryID = $countryID;
    }

    public function regionChanged(int $regionID): void
    {
        $this->regionID = $regionID;
    }

    public function cityChanged(array $city): void
    {
        $this->cityID = $city['id'];
        $this->cityCode = $city['code'];
    }

    public function travelPeopleChanged(array $value): void
    {
        $this->travelPeople = $value;
    }

    public function startDateUpdated(string $startDate): void
    {
        $this->travelStartDate = $startDate;
    }

    public function endDateUpdated(string $endDate): void
    {
        $this->travelEndDate = $endDate;
    }

    public function productTypeUpdated(string $productTypeCode): void
    {
        $this->productTypeCode = $productTypeCode;
    }

    /**
     * @throws Exception
     */
    public function searchProducts(): void
    {
        $this->validate();

        /** @var JonviewAPI $apiService */
        $apiService = resolve(JonviewAPI::class);

        /** @var SupplierService $supplierService */
        $supplierService = resolve(SupplierService::class);

        $DTO = new SearchDTO(
            [
                'cityID'          => $this->cityID,
                'cityCode'        => $this->cityCode,
                'productTypeCode' => $this->productTypeCode,
                'fromDate'        => Carbon::parse($this->travelStartDate),
                'toDate'          => Carbon::parse($this->travelEndDate),
                'people'          => $this->travelPeople,
            ]
        );
        $apiService->search($DTO);

        $this->emitTo('admin.product-list', 'productListUpdated', $supplierService->search($DTO));
    }

    public function createTour(
        int $productID,
        string $tourTitle,
        int $tripThemeID,
        int $tripThemeTypeID,
        string $userPrice,
        bool $isPublish,
        int $bookingID = null
    ): void {
        $this->validate();

        //@TODO need move to CreateTour Modal class, location and date need get from product data
        $tourDTO = new TourDTO(
            [
                'productID'          => $productID,
                'title'              => $tourTitle,
                'country_id'         => $this->countryID,
                'region_id'          => $this->regionID,
                'city_id'            => $this->cityID,
                'start_trip_date'    => Carbon::parse($this->travelStartDate),
                'end_trip_date'      => Carbon::parse($this->travelEndDate),
                'trip_theme_id'      => $tripThemeID,
                'trip_theme_type_id' => $tripThemeTypeID,
                'user_price'         => $userPrice,
                'is_publish'         => $isPublish,
            ]
        );

        try {
            /** @var TourService $tourService */
            $tourService = resolve(TourService::class);
            $tourService->createTour($tourDTO, $bookingID);
        } catch (Throwable $exception) {
            if ($bookingID) {
                $this->emit('appendToBookingError', $productID);
            } else {
                $this->emit('createTourError', $productID);
            }
        }

        if ($bookingID) {
            $this->emit('appendToBookingSuccess', $productID);
        } else {
            $this->emit('createTourSuccess', $productID);
        }
    }
}
