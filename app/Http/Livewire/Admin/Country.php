<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Country as CountryModel;

class Country extends Component
{
    public int $country;

    public function mount(): void
    {
        $this->country = 0;
    }

    public function getSortedCountriesProperty(): array
    {
        return CountryModel::orderBy(CountryModel::FIELD_NAME)
            ->pluck(CountryModel::FIELD_NAME, 'id')
            ->all();
    }

    public function render()
    {
        return view('livewire.admin.country');
    }

    public function updatedCountry($value): void
    {
        $this->emit('countryChanged', $value);
    }
}
