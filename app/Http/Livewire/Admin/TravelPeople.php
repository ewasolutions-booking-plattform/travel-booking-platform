<?php

namespace App\Http\Livewire\Admin;

use App\Enums\AgeCategoryEnum;
use Livewire\Component;

class TravelPeople extends Component
{
    public array $ageCategories;
    public array $travelPeople;

    protected array $rules = [
        'travelPeople.adult'    => ['required', 'numeric', 'min:1', 'max:10'],
        'travelPeople.children' => ['numeric', 'min:0', 'max:10'],
    ];

    protected array $messages = [
        'travelPeople.adult.required' => 'field is required',
        'travelPeople.adult.min'      => 'min :min',
        'travelPeople.children.min'   => 'min :min',
        'travelPeople.adult.max'      => 'max :max',
        'travelPeople.children.max'   => 'max :max',
    ];

    public function mount(): void
    {
        $this->ageCategories = AgeCategoryEnum::toValues();
        foreach ($this->ageCategories as $category) {
            $this->travelPeople[$category] = AgeCategoryEnum::adult()->value === $category ? 2 : 0;
        }
    }

    public function render()
    {
        return view('livewire.admin.travel-people');
    }

    public function updated(string $propName, $propVal): void
    {
        $this->validateOnly($propName);
        $this->$propName = $propVal;

        $this->emit('travelPeopleChanged', $this->travelPeople);
    }
}
