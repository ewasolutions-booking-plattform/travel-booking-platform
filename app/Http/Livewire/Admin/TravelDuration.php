<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use DateTimeInterface;
use Livewire\Component;

class TravelDuration extends Component
{
    public DateTimeInterface $minStartDate;

    public string $travelStartDate;
    public string $travelEndDate;

    protected array $rules = [
        'travelStartDate' => ['required', 'regex:/[0-9]{4}-[0-9]{2}-[0-9]{2}/', 'after_or_equal:today'],
        'travelEndDate'   => ['required', 'regex:/[0-9]{4}-[0-9]{2}-[0-9]{2}/', 'after:travelStartDate'],
    ];

    protected array $messages = [
        'travelStartDate.required' => 'Trip end field is required',
        'travelStartDate.regex'    => 'incorrect date format',
        'travelEndDate.required'   => 'Trip end date field is required',
        'travelEndDate.regex'      => 'incorrect date format',
    ];

    public function mount(): void
    {
        $this->minStartDate = Carbon::now();
        $this->travelStartDate = '';
        $this->travelEndDate = '';
    }

    public function render()
    {
        return view('livewire.admin.travel-duration');
    }

    public function updatedTravelStartDate($value): void
    {
        $this->travelStartDate = $value;
        $this->validateOnly('travelStartDate');
        $this->emit('startDateUpdated', $value);
    }

    public function updatedTravelEndDate($value): void
    {
        $this->travelEndDate = $value;
        $this->validateOnly('travelEndDate');
        $this->emit('endDateUpdated', $value);
    }
}
