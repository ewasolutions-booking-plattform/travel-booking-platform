<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\City as CityModel;

class City extends Component
{
    public array $cities;
    public int $region;
    public int $city;

    public $listeners = ['regionChanged'];

    public function mount(): void
    {
        $this->cities = [];
        $this->region = 0;
        $this->city = 0;
    }

    public function render()
    {
        return view('livewire.admin.city');
    }

    public function updatedCity(int $value): void
    {
        $city = array_filter(
            $this->cities,
            function ($item) use ($value) {
                return $item['id'] === $value;
            }
        );
        $this->emit('cityChanged', array_shift($city));
    }

    public function regionChanged(int $regionID): void
    {
        $this->region = $regionID;
        $this->updateCities();
    }

    public function getSortedCitiesProperty(): array
    {
        $this->updateCities();

        return $this->cities;
    }

    private function updateCities(): void
    {
        $this->cities = [];

        if ($this->region) {
            $this->cities = CityModel::where(CityModel::FIELD_REGION, $this->region)
                ->orderBy(CityModel::FIELD_NAME)
                ->get()
                ->toArray();
        }
    }
}
