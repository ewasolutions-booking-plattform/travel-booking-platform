<?php

namespace App\Http\Livewire\Admin;

use App\Models\TripTheme;
use App\Models\TripThemeType;
use Livewire\Component;

class CreateTour extends Component
{
    public array $tripThemes = [];
    public array $tripThemeTypes = [];

    public string $tourTitle = '';
    public ?int $selectedProduct = null;
    public int $tripTheme = 0;
    public int $tripThemeType = 0;
    public string $tourUserPrice = '0.0';
    public bool $isPublish = false;

    protected array $rules = [
        'selectedProduct' => ['required', 'exists:products,id'],
        'tourTitle'       => ['required', 'string', 'min:3'],
        'tripTheme'       => ['required', 'exists:trip_themes,id'],
        'tripThemeType'   => ['required', 'exists:trip_theme_types,id'],
        'tourUserPrice'   => ['required', 'min:1'],

    ];

    protected array $messages = [
        'tourTitle.required' => 'Please write tour title',
        'tourTitle.min'      => 'Tour title :min',
        'tourUserPrice.required' => 'Price for user is required',
        'tourUserPrice.min' => 'Price for user must be bigger :min',
    ];

    public $listeners = ['selectedProductForTourEvent'];

    public function mount(): void
    {
        $this->tripThemes = TripTheme::pluck(TripTheme::FIELD_TITLE, 'id')->toArray();
    }

    public function render()
    {
        return view('livewire.admin.create-tour');
    }

    public function updatedTripTheme(int $value): void
    {
        if ($value > 0) {
            $this->tripThemeTypes = TripThemeType::where(TripThemeType::FIELD_TRIP_THEME, $value)
                ->pluck(TripThemeType::FIELD_TITLE, 'id')
                ->toArray();
        } else {
            $this->tripThemeTypes = [];
        }
    }

    public function selectedProductForTourEvent(int $productID): void
    {
        $this->selectedProduct = $productID;
    }

    public function store(): void
    {
        $this->validate();

        $this->emit(
            'createTour',
            $this->selectedProduct,
            $this->tourTitle,
            $this->tripTheme,
            $this->tripThemeType,
            $this->tourUserPrice,
            $this->isPublish
        );
        $this->clearData();
    }

    public function cancel(): void
    {
        $this->clearData();
    }

    private function clearData(): void
    {
        $this->tourTitle = '';
        $this->tripTheme = 0;
        $this->tripThemeType = 0;
        $this->selectedProduct = null;
        $this->tourUserPrice = '0.0';
        $this->isPublish = false;
        $this->clearValidation();
    }
}
