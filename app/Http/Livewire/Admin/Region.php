<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Region as RegionModel;

class Region extends Component
{
    public int $region;
    public int $country;

    public $listeners = ['countryChanged'];

    public function mount(): void
    {
        $this->region = 0;
        $this->country = 0;
    }

    public function render()
    {
        return view('livewire.admin.region');
    }

    public function updatedRegion(int $value): void
    {
        $this->emit('regionChanged', $value);
    }

    public function countryChanged(int $countryID): void
    {
        $this->country = $countryID;
    }

    public function getSortedRegionsProperty(): array
    {
        if ($this->country) {
            return RegionModel::where(RegionModel::FIELD_COUNTRY, $this->country)
                ->orderBy(RegionModel::FIELD_NAME)
                ->pluck(RegionModel::FIELD_NAME, 'id')
                ->all();
        }

        return [];
    }
}
