<?php

namespace App\Http\Livewire\Admin;

use App\Models\ProductType as ProductTypeModel;
use App\Models\ProductTypeGroup;
use Livewire\Component;

class ProductType extends Component
{
    public string $productType;
    public array $productTypes;
    public int $typeGroup;
    public array $typeGroups;

    protected array $rules = [
        'productType' => ['required', 'string', 'exists:product_types,code'],
    ];

    public function mount(): void
    {
        $this->productType = 0;
        $this->typeGroup = 1;

        $this->productTypes = ProductTypeModel::orderBy(ProductTypeModel::FIELD_DESCRIPTION)
            ->whereHas('typeGroups', function ($query) {
                $query->where('product_type_group_id', 1);
            })
            ->pluck(ProductTypeModel::FIELD_DESCRIPTION, ProductTypeModel::FIELD_CODE)
            ->all();
        $this->typeGroups = ProductTypeGroup::orderBy(ProductTypeGroup::FIELD_NAME)
            ->pluck(ProductTypeGroup::FIELD_NAME, 'id')
            ->all();
    }

    public function render()
    {
        return view('livewire.admin.product-type');
    }

    public function updatedProductType(string $value): void
    {
        $this->validateOnly('productType');
        $this->emit('productTypeUpdated', $value);
    }

    public function updatedTypeGroup(int $value): void
    {
        $this->productTypes = ProductTypeModel::orderBy(ProductTypeModel::FIELD_DESCRIPTION)
            ->whereHas('typeGroups', function ($query) use ($value) {
                $query->where('product_type_group_id', $value);
            })
            ->pluck(ProductTypeModel::FIELD_DESCRIPTION, ProductTypeModel::FIELD_CODE)
            ->all();
    }
}
