<?php

namespace App\Http\Livewire\Admin;

use App\Enums\RoleEnum;
use App\Models\Booking;
use App\Models\TripTheme;
use App\Models\TripThemeType;
use Livewire\Component;

class AppendToBooking extends Component
{
    public array $bookings = [];
    public array $tripThemes = [];
    public array $tripThemeTypes = [];

    public string $booking = '';
    public string $tourTitle = '';
    public ?int $selectedProduct = null;
    public int $tripTheme = 0;
    public int $tripThemeType = 0;
    public string $tourUserPrice = '0.0';
    public bool $isPublish = false;

    protected array $rules = [
        'booking'         => ['required', 'exists:bookings,id'],
        'selectedProduct' => ['required', 'exists:products,id'],
        'tourTitle'       => ['required', 'string', 'min:3'],
        'tripTheme'       => ['required', 'exists:trip_themes,id'],
        'tripThemeType'   => ['required', 'exists:trip_theme_types,id'],
        'tourUserPrice'   => ['required', 'min:1'],

    ];

    protected array $messages = [
        'booking.required'   => 'Please select booking from list.',
        'tourTitle.required' => 'Please write tour title',
        'tourTitle.min'      => 'Tour title :min',
        'tourUserPrice.min' => 'Price for user must be bigger :min',
    ];

    public $listeners = ['selectedProductForBookingEvent'];

    public function mount(): void
    {
        $model = Booking::with(['city', 'tripTheme', 'tripThemeType']);
        if (auth()->user()->hasRole(RoleEnum::operator()->value)) {
            $model->where(Booking::FK_OPERATOR, auth()->user()->id);
        }
        $this->bookings =$model->get()->toArray();
        $this->tripThemes = TripTheme::pluck(TripTheme::FIELD_TITLE, 'id')->toArray();
    }

    public function render()
    {
        return view('livewire.admin.append-to-booking');
    }

    public function updatedTripTheme(int $value): void
    {
        if ($value > 0) {
            $this->tripThemeTypes = TripThemeType::where(TripThemeType::FIELD_TRIP_THEME, $value)
                ->pluck(TripThemeType::FIELD_TITLE, 'id')->toArray();
        } else {
            $this->tripThemeTypes = [];
        }
    }

    public function selectedProductForBookingEvent(int $productID): void
    {
        $this->selectedProduct = $productID;
    }

    public function updatedBooking(string $value): void
    {
        $this->validateOnly('booking');
        $this->booking = $value;
    }

    public function store(): void
    {
        $this->validate();

        $this->emit(
            'createTour',
            $this->selectedProduct,
            $this->tourTitle,
            $this->tripTheme,
            $this->tripThemeType,
            $this->tourUserPrice,
            $this->isPublish,
            $this->booking,
        );
        $this->clearData();
    }

    public function cancel(): void
    {
        $this->clearData();
    }

    private function clearData(): void
    {
        $this->booking = '';
        $this->tourTitle = '';
        $this->tripTheme = 0;
        $this->tripThemeType = 0;
        $this->selectedProduct = null;
        $this->tourUserPrice = '0.0';
        $this->isPublish = false;
        $this->clearValidation();
    }
}
