<?php

namespace App\Http\Livewire\Admin\Booking;

use App\DTO\TourDTO;
use App\Models\Booking;
use App\Services\TourService;
use Livewire\Component;
use Throwable;

class Suppliers extends Component
{
    public int $productID = 0;
    public int $bookingID = 0;
    public array $suppliers = [];

    public $listeners = ['createTour'];

    protected array $rules = [
        'productID' => ['required', 'exists:products,id'],
        'bookingID' => ['required', 'exists:bookings,id'],
    ];

    public function mount(int $booking, array $suppliers): void
    {
        $this->bookingID = $booking;
        $this->suppliers = $suppliers;
    }

    public function render()
    {
        return view('livewire.admin.booking.suppliers');
    }

    public function showTourModalClick(int $productID): void
    {
        $this->productID = $productID;
        $this->dispatchBrowserEvent('show-modal');
    }

    public function createTour(string $title, string $userPrice, bool $isPublish): void
    {
        $this->validate();

        $booking = Booking::find($this->bookingID);
        $tourDTO = new TourDTO(
            [
                'productID'          => $this->productID,
                'title'              => $title,
                'country_id'         => $booking->country->id,
                'region_id'          => $booking->region->id,
                'city_id'            => $booking->city->id,
                'start_trip_date'    => $booking->trip_start_date,
                'end_trip_date'      => $booking->trip_start_date,
                'trip_theme_id'      => $booking->tripTheme->id,
                'trip_theme_type_id' => $booking->tripThemeType->id,
                'user_price'         => $userPrice,
                'is_publish'         => $isPublish,
            ]
        );

        try {
            /** @var TourService $tourService */
            $tourService = resolve(TourService::class);
            $tourService->createTour($tourDTO, $booking->id);
        } catch (Throwable $exception) {
            $this->addError('bookingID', 'Can\'t create tour.');
        }
    }
}
