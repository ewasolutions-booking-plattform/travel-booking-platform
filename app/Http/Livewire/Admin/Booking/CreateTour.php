<?php

namespace App\Http\Livewire\Admin\Booking;

use Livewire\Component;

class CreateTour extends Component
{
    public string $tourTitle = '';
    public string $tourUserPrice = '';
    public bool $isPublish = false;

    protected array $rules = [
        'tourTitle'     => ['required', 'string', 'min:3'],
        'tourUserPrice' => ['required', 'min:1'],

    ];

    protected array $messages = [
        'tourTitle.required'     => 'Please write tour title',
        'tourTitle.min'          => 'Tour title :min',
        'tourUserPrice.required' => 'Price for user is required',
        'tourUserPrice.min'      => 'Price for user must be bigger :min',
    ];

    public function render()
    {
        return view('livewire.admin.booking.create-tour');
    }

    public function store(): void
    {
        $this->validate();

        $this->emit('createTour', $this->tourTitle, $this->tourUserPrice, $this->isPublish);
        $this->dispatchBrowserEvent('hide-modal');
        $this->clearData();
    }

    public function cancel(): void
    {
        $this->clearData();
    }

    private function clearData(): void
    {
        $this->tourTitle = '';
        $this->tourUserPrice = '';
        $this->isPublish = false;
        $this->clearValidation();
    }
}
