<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class ProductList extends Component
{
    public array $searchResult;

    protected $listeners = [
        'productListUpdated',
        'createTourError',
        'createTourSuccess',
        'appendToBookingError',
        'appendToBookingSuccess',
    ];

    public function mount(array $searchResult): void
    {
        $this->searchResult = $searchResult;
    }

    public function render()
    {
        return view('livewire.admin.product-list');
    }

    public function productListUpdated(array $newSearchResult): void
    {
        $this->searchResult = $newSearchResult;
    }

    public function showTourModalClick(int $productID): void
    {
        $this->emit('selectedProductForTourEvent', $productID);
        $this->dispatchBrowserEvent('show-tour-modal');
    }

    public function showBookingModalClick(int $productID): void
    {
        $this->emit('selectedProductForBookingEvent', $productID);
        $this->dispatchBrowserEvent('show-booking-modal');
    }

    public function createTourError(int $productID): void
    {
        $this->addError('product_id_'.$productID, 'Can\'t create tour.');
        $this->dispatchBrowserEvent('hide-tour-modal');
    }

    public function createTourSuccess(int $productID): void
    {
        $message = "Tour created successful ";
        session()->flash('success_'.$productID, $message);
        $this->dispatchBrowserEvent('hide-tour-modal');
    }

    public function appendToBookingError(int $productID): void
    {
        $this->addError('product_id_'.$productID, 'Can\'t append tour to Booking.');
        $this->dispatchBrowserEvent('hide-append-modal');
    }

    public function appendToBookingSuccess(int $productID): void
    {
        $message = "Tour appended successful ";
        session()->flash('success_'.$productID, $message);
        $this->dispatchBrowserEvent('hide-append-modal');
    }
}
