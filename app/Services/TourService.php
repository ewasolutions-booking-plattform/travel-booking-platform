<?php

namespace App\Services;

use App\DTO\Requests\FilterDTO;
use App\DTO\TourDTO;
use App\Enums\SortTypesEnum;
use App\Models\Booking;
use App\Models\Product;
use App\Models\Tour;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;

class TourService
{
    private const DEFAULT_PER_PAGE = 15;

    public function searchTour(
        int $country,
        int $region,
        int $city,
        Carbon $fromDate,
        Carbon $todDate,
        int $tripThemeID,
        int $tripThemeTypeID,
        int $budget
    ) {
        return Tour::where(Tour::FIELD_COUNTRY, $country)
            ->where(Tour::FIELD_REGION, $region)
            ->where(Tour::FIELD_CITY, $city)
            ->where(Tour::FIELD_TRIP_THEME, $tripThemeID)
            ->where(Tour::FIELD_TRIP_THEME_TYPE, $tripThemeTypeID)
            ->where(Tour::FIELD_USER_PRICE, '>=', $budget)
            ->where(Tour::FIELD_TRIP_START, $fromDate)
            ->where(Tour::FIELD_TRIP_END, $todDate)
            ->orderByDESC(Tour::FIELD_USER_PRICE)
            ->first();
    }

    public function searchTours(FilterDTO $filterDTO): Paginator
    {
        $builder = Tour::whereHas('products')
            ->with(
                [
                    'country',
                    'region',
                    'products',
                    'products.supplier',
                    'products.supplier.images',
                ]
            )
            ->where(Tour::FIELD_IS_PUBLISH, true);

        if ($country = $filterDTO->country) {
            $builder->where(Tour::FIELD_COUNTRY, $country->id);
        }

        if ($tripTheme = $filterDTO->theme) {
            $builder->where(Tour::FIELD_TRIP_THEME, $tripTheme->id);
        }

        if ($sort = $filterDTO->sort) {
            switch($sort) {
                case SortTypesEnum::nameASC():
                    $builder->orderBy(Tour::FIELD_TITLE);
                    break;
                case SortTypesEnum::nameDESC():
                    $builder->orderByDesc(Tour::FIELD_TITLE);
                    break;
                case SortTypesEnum::priceLow():
                    $builder->orderBy(Tour::FIELD_USER_PRICE);
                    break;
                case SortTypesEnum::priceHigh():
                    $builder->orderByDesc(Tour::FIELD_USER_PRICE);
                    break;
            }
        }

        $items = $builder->paginate(self::DEFAULT_PER_PAGE);

        if ($filterDTO->country) {
            $items->appends('country', $filterDTO->country->code);
        }

        if ($filterDTO->theme) {
            $items->appends('theme', $filterDTO->theme->id);
        }

        if ($filterDTO->sort) {
            $items->appends('sort', $filterDTO->sort);
        }

        return $items;
    }

    public function createTour(TourDTO $tourDTO, int $bookingID = null): Tour
    {
        $product = Product::findOrFail($tourDTO->productID);

        $tour = Tour::create($tourDTO->except('productID')->toArray());
        $tour->products()->attach($product->id);

        if ($bookingID) {
            $booking = Booking::findOrFail($bookingID);
            $booking->tour()->associate($tour);
            $booking->save();
        }

        return $tour;
    }
}
