<?php

namespace App\Services;

use App\Models\City;

class CityService
{
    public function getForSelect(int $region = 0): array
    {
        $qb = City::pluck(City::FIELD_NAME, 'id')
            ->sortBy(City::FIELD_NAME);

        if ($region > 0) {
            $qb->where(City::FIELD_REGION, $region);
        }

        return $qb->all();
    }
}
