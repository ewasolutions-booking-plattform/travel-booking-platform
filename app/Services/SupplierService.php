<?php

namespace App\Services;

use App\DTO\Jonview\SearchDTO;
use App\DTO\Jonview\SupplierDTO;
use App\Models\Activity;
use App\Models\Amenity;
use App\Models\ProductType;
use App\Models\Supplier;
use Exception;
use RuntimeException;

class SupplierService
{
    public function createSupplier(SupplierDTO $supplierDTO): Supplier
    {
        $activities = $supplierDTO->activities;
        $amenities = $supplierDTO->amenities;
        $images = $supplierDTO->images;

        /** @var Supplier $supplier */
        $supplier = Supplier::firstOrCreate(
            $supplierDTO->only('code')->toArray(),
            $supplierDTO->except('code', 'activities', 'amenities', 'images')->toArray()
        );
        if ($activities) {
            $ids = [];
            foreach ($activities as $record) {
                $ids[] = Activity::firstOrCreate($record)->id;
            }
            $supplier->activities()->sync($ids);
        }

        if ($amenities) {
            $ids = [];
            foreach ($amenities as $record) {
                $ids[] = Amenity::firstOrCreate($record)->id;
            }
            $supplier->amenities()->sync($ids);
        }

        if ($images) {
            $supplier->images()->delete();
            $supplier->images()->createMany($images);
        }

        return $supplier;
    }

    /**
     * @throws Exception
     */
    public function search(SearchDTO $DTO): array
    {
        $fromDate = $DTO->fromDate->format('Y-m-d');
        $toDate = $DTO->toDate->format('Y-m-d');
        $productType = ProductType::where('code', $DTO->productTypeCode)->first();
        if (!$productType) {
            throw new RuntimeException('product type not found');
        }

        return Supplier::where(Supplier::FIELD_CITY, $DTO->cityID)
            ->withCount(
                [
                    'products' => function ($query) use ($productType) {
                        $query->where('product_type_id', $productType->id);
                    },
                ]
            )
            ->with(
                [
                    'activities',
                    'amenities',
                    'products'            => function ($query) {
                        $query->whereHas('prices')->whereHas('availables');
                    },
                    'products.prices'     => function ($query) use ($fromDate, $toDate) {
                        $query->where(['from_date' => $fromDate, 'to_date' => $toDate]);
                    },
                    'products.availables' => function ($query) use ($fromDate, $toDate) {
                        $query->where(['from_date' => $fromDate, 'to_date' => $toDate]);
                    },
                    'products.characteristics',
                    'products.prices.currency',
                ]
            )
            ->get()
            ->where('products_count', '>', 0)
            ->toArray();
    }

    public function getFirstImage(Supplier $supplier): string
    {
        return $supplier->images()->first()->path ?? '/images/tour/tour-1.jpg';
    }
}
