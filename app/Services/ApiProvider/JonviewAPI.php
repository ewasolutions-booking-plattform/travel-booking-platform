<?php

namespace App\Services\ApiProvider;

use App\DTO\Jonview\AvailableDTO;
use App\DTO\Jonview\CityDTO;
use App\DTO\Jonview\SupplierDTO;
use App\DTO\Jonview\PriceDTO;
use App\DTO\Jonview\ProductDTO;
use App\DTO\Jonview\ProductTypeDTO;
use App\DTO\Jonview\SearchDTO;
use App\Enums\Jonview\ActionEnum;
use App\Enums\Jonview\SearchTypeEnum;
use App\Models\Activity;
use App\Models\Amenity;
use App\Models\Booking;
use App\Models\Characteristic;
use App\Models\City;
use App\Models\Currency;
use App\Models\Image;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\RequestLog;
use App\Models\Supplier;
use App\Services\SupplierService;
use DateTimeInterface;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use RuntimeException;
use SimpleXMLElement;
use Spatie\ArrayToXml\ArrayToXml;
use Throwable;

class JonviewAPI
{
    private const REQUEST_STATUS_OK = 'C';
    private const DISPLAY_PROP_YES = 'Y';

    private Client $client;
    private SupplierService $supplierService;
    private string $endpoint;
    private string $userID;
    private string $password;
    private string $locationSequence;
    private array $productTypes = [];
    private array $suppliers = [];

    public function __construct(SupplierService $supplierService)
    {
        $this->endpoint = config('apis.jonview.endpoint');
        $this->userID = config('apis.jonview.user_id');
        $this->password = config('apis.jonview.password');
        $this->locationSequence = config('apis.jonview.location_sequence');

        if (empty($this->endpoint)) {
            throw new RuntimeException('JONVIEW API PROVIDER, endpoint is empty');
        }

        if (empty($this->userID)) {
            throw new RuntimeException('JONVIEW API PROVIDER, user is empty');
        }

        if (empty($this->password)) {
            throw new RuntimeException('JONVIEW API PROVIDER, password is empty');
        }

        if (empty($this->locationSequence)) {
            throw new RuntimeException('JONVIEW API PROVIDER, location sequence is empty');
        }

        $this->client = new Client();
        $this->supplierService = $supplierService;
    }

    /**
     * @return ProductTypeDTO[]|array
     * @throws Exception
     */
    public function getDFProdType(): array
    {
        $data = [];
        $payload = [
            'changesince' => null,
            'type'        => SearchTypeEnum::prodtype(),
        ];
        $xml = $this->request(ActionEnum::DF(), $payload);
        if (!$xml) {
            return $data;
        }

        foreach ($xml->referencelistseg->listrecord as $record) {
            $data[] =
                new ProductTypeDTO(
                    [
                        'code'        => Str::upper((string)$record->code),
                        'description' => (string)$record->description,
                    ]
                );
        }

        return $data;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getDFCity(): array
    {
        $data = [];
        $payload = [
            'changesince'      => null,
            'type'             => SearchTypeEnum::city(),
            'detailedresponse' => self::DISPLAY_PROP_YES,
        ];
        $xml = $this->request(ActionEnum::DF(), $payload);
        if (!$xml) {
            return $data;
        }

        foreach ($xml->referencelistseg->listrecord as $record) {
            $data[] =
                new CityDTO(
                    [
                        'code'         => Str::upper($record->code),
                        'description'  => Str::upper($record->description),
                        'name'         => Str::upper($record->cityname),
                        'provinceCode' => Str::upper($record->provincecode),
                        'provinceName' => Str::upper($record->provincename),
                        'countryCode'  => Str::upper($record->countrycode),
                        'countryName'  => Str::upper($record->countryname),
                    ]
                );
        }

        return $data;
    }

    /**
     * Get suppliers by city
     *
     * @param City $city
     *
     * @return array
     * @throws Exception
     */
    public function getDSCity(City $city): array
    {
        $requestHash = $this->createRequestHash(
            ActionEnum::DS(),
            SearchTypeEnum::city(),
            $city->code
        );
        if ($this->isRequestHash($requestHash)) {
            return [];
        }

        $payload = [
            'changesince'    => null,
            'searchtype'     => SearchTypeEnum::city(),
            'citycode'       => $city->code,
            'displaygeocode' => self::DISPLAY_PROP_YES,
        ];
        $xml = $this->request(ActionEnum::DS(), $payload);
        if (!$xml) {
            return [];
        }

        $data = $this->getSuppliersData($xml->supplierlistseg, $city);

        if (count($data)) {
            $this->requestLog($requestHash, today()->addWeek());
        }

        return $data;
    }

    /**
     * @param string $supplierCode
     *
     * return NULL if was recently request with same parameters or response from API is empty
     *
     * @return SupplierDTO|null
     * @throws Exception
     */
    public function getDSSup(string $supplierCode): ?SupplierDTO
    {
        $requestHash = $this->createRequestHash(
            ActionEnum::DS(),
            SearchTypeEnum::sup()
        );
        if ($this->isRequestHash($requestHash)) {
            return null;
        }

        $payload = [
            'changesince'     => null,
            'searchtype'      => SearchTypeEnum::sup(),
            'displaygeocode'  => self::DISPLAY_PROP_YES,
            'supplierlistseg' => [
                'codeitem' => [
                    'suppliercode' => $supplierCode,
                ],
            ],
        ];
        $xml = $this->request(ActionEnum::DS(), $payload);
        if (!$xml) {
            return null;
        }

        $data = $this->getSuppliersData($xml->supplierlistseg);

        $this->requestLog($requestHash, today()->addWeek());

        return array_shift($data);
    }

    /**
     * @param SearchDTO $searchDTO
     *
     * @return array
     * @throws Exception
     */
    public function getDPCity(SearchDTO $searchDTO): array
    {
        $requestHash = $this->createRequestHash(
            ActionEnum::DP(),
            SearchTypeEnum::city(),
            $searchDTO->cityCode,
            $searchDTO->productTypeCode,
            $searchDTO->fromDate,
            $searchDTO->toDate
        );

        if ($this->isRequestHash($requestHash)) {
            return [];
        }

        $payload = [
            'changesince'         => null,
            'citycode'            => $searchDTO->cityCode,
            'prodtypecode'        => $searchDTO->productTypeCode,
            'fromdate'            => $searchDTO->fromDate->format('d-M-Y'),
            'todate'              => $searchDTO->toDate->format('d-M-Y'),
            'searchtype'          => SearchTypeEnum::city(),
            'displaynamedetails'  => self::DISPLAY_PROP_YES,
            'displayrestriction'  => self::DISPLAY_PROP_YES,
            'displaypolicy'       => self::DISPLAY_PROP_YES,
            'displayschdate'      => self::DISPLAY_PROP_YES,
            'displayusage'        => self::DISPLAY_PROP_YES,
            'displaydynamicrates' => self::DISPLAY_PROP_YES,
        ];

        $xml = $this->request(ActionEnum::DP(), $payload);

        if (!$xml) {
            return [];
        }

        $data = [];
        foreach ($xml->productlistseg->listrecord as $record) {
            $characteristics = [];
            if ($productSeg = $record->productnamedetails) {
                if ($room = (string)$productSeg->roomtype) {
                    $characteristics[] = [
                        'name'  => 'room',
                        'value' => $room,
                    ];
                }

                if ($board = (string)$productSeg->board) {
                    $characteristics[] = [
                        'name'  => 'board',
                        'value' => $board,
                    ];
                }
            }
            $supplierID = $this->getSupplierID($record->suppliercode);
            if (null === $supplierID) {
                info(
                    'failed record',
                    [
                        'name'            => (string)$record->name,
                        'code'            => (string)$record->prodcode,
                        'supplier_id'     => (string)$record->suppliercode,
                        'product_type_id' => (string)$record->prodtypecode,
                    ]
                );
                continue;
            }
            $data[] = new ProductDTO(
                [
                    'name'            => (string)$record->name,
                    'code'            => (string)$record->prodcode,
                    'supplier_id'     => $supplierID,
                    'product_type_id' => $this->getProductTypeID($record->prodtypecode),
                    'characteristics' => $characteristics,
                ]
            );
        }

        if (count($data)) {
            $this->requestLog($requestHash, today()->addWeek());
        }

        return $data;
    }

    /**
     * @param SearchDTO $searchDTO
     *
     * @return array
     * @throws Exception
     */
    public function getDICity(SearchDTO $searchDTO): array
    {
        $requestHash = $this->createRequestHash(
            ActionEnum::DI(),
            SearchTypeEnum::city(),
            $searchDTO->cityCode,
            $searchDTO->productTypeCode,
            $searchDTO->fromDate,
            $searchDTO->toDate
        );
        if ($this->isRequestHash($requestHash)) {
            return [];
        }

        $payload = [
            'changesince'  => null,
            'searchtype'   => SearchTypeEnum::city(),
            'citycode'     => $searchDTO->cityCode,
            'prodtypecode' => $searchDTO->productTypeCode,
            'fromdate'     => $searchDTO->fromDate->format('d-M-Y'),
            'todate'       => $searchDTO->toDate->format('d-M-Y'),
            'showstatus'   => self::DISPLAY_PROP_YES,
        ];
        $xml = $this->request(ActionEnum::DI(), $payload);
        if (!$xml) {
            return [];
        }

        $data = [];
        foreach ($xml->productlistseg->listrecord as $record) {
            $data[] = new AvailableDTO(
                [
                    'product_code' => (string)$record->prodcode,
                    'from_date'    => (string)$record->fromdate,
                    'to_date'      => (string)$record->todate,
                    'available'    => (string)$record->availstring,
                ]
            );
        }

        if (count($data)) {
            $this->requestLog($requestHash, today());
        }

        return $data;
    }

    /**
     * @param SearchDTO $searchDTO
     *
     * @return array
     * @throws Exception
     */
    public function getDRCity(SearchDTO $searchDTO): array
    {
        $requestHash = $this->createRequestHash(
            ActionEnum::DR(),
            SearchTypeEnum::city(),
            $searchDTO->cityCode,
            $searchDTO->productTypeCode,
            $searchDTO->fromDate,
            $searchDTO->toDate
        );
        if ($this->isRequestHash($requestHash)) {
            return [];
        }

        $payload = [
            'changesince'  => null,
            'searchtype'   => SearchTypeEnum::city(),
            'citycode'     => $searchDTO->cityCode,
            'prodtypecode' => $searchDTO->productTypeCode,
            'fromdate'     => $searchDTO->fromDate->format('d-M-Y'),
            'todate'       => $searchDTO->toDate->format('d-M-Y'),
        ];
        $xml = $this->request(ActionEnum::DR(), $payload);
        if (!$xml) {
            return [];
        }

        $data = [];
        foreach ($xml->productlistseg->listrecord as $record) {
            $data[] = new PriceDTO(
                [
                    'product_code' => (string)$record->prodcode,
                    'from_date'    => (string)$record->effectivedate,
                    'to_date'      => (string)$record->expirydate,
                    'currency'     => (string)$record->currencycode,
                    'single'       => (float)$record->single,
                    'twin'         => $record->twin ? (float)$record->twin : null,
                    'triple'       => $record->triple ? (float)$record->triple : null,
                    'quad'         => $record->quad ? (float)$record->quad : null,
                    'child1'       => $record->child1 ? (float)$record->child1 : null,
                    'child2'       => $record->child2 ? (float)$record->child2 : null,
                    'child3'       => $record->child3 ? (float)$record->child3 : null,
                    'child_age1'   => $record->childAge1 ? (float)$record->childAge1 : null,
                    'child_age2'   => $record->childAge2 ? (float)$record->childAge2 : null,
                    'child_age3'   => $record->childAge3 ? (float)$record->childAge3 : null,
                ]
            );
        }

        if (count($data)) {
            $this->requestLog($requestHash, today()->addWeek());
        }

        return $data;
    }

    /**
     * @param Booking $booking
     *
     * @throws Exception
     * @deprecated
     */
    public function takeProductPriceAndAvailable(Booking $booking): void
    {
        $people = [];
        foreach ($booking->ageCategories as $category) {
            $people[$category->title] = $category->count;
        }

        $searchDTO = new SearchDTO(
            [
                'cityID'          => $booking->city->id,
                'cityCode'        => $booking->city->code,
                'productTypeCode' => 'FIT',
                'fromDate'        => $booking->trip_start_date,
                'toDate'          => $booking->trip_end_date,
                'people'          => $people,
            ]
        );

        $this->search($searchDTO);
    }

    /**
     * @param SearchDTO $searchDTO
     *
     * @throws Exception
     */
    public function search(SearchDTO $searchDTO): void
    {
        $productDTOs = $this->getDPCity($searchDTO);
        $availableDTOs = $this->getDICity($searchDTO);
        $priceDTOs = $this->getDRCity($searchDTO);

        /** @var ProductDTO $DTO */
        foreach ($productDTOs as $DTO) {
            try {
                $product = Product::firstOrCreate($DTO->except('characteristics')->toArray());
            } catch (Throwable $exception) {
                info($exception->getMessage());
                continue;
            }
            $characteristics = [];
            foreach ($DTO->characteristics as $item) {
                $characteristic = Characteristic::firstOrCreate([Characteristic::FIELD_NAME => $item['name']]);
                $characteristics[$characteristic->id] = ['value' => $item['value']];
            }
            if (count($characteristics)) {
                $product->characteristics()->sync($characteristics);
            }
        }

        /** @var AvailableDTO $DTO */
        foreach ($availableDTOs as $DTO) {
            $product = Product::where([Product::FIELD_CODE => $DTO->product_code])->first();
            if (!$product) {
                continue;
            }
            $product->availables()->delete();
            $product->availables()->create($DTO->except('product_code')->toArray());
        }

        /** @var PriceDTO $DTO */
        foreach ($priceDTOs as $DTO) {
            $product = Product::where([Product::FIELD_CODE => $DTO->product_code])->first();
            if (!$product) {
                continue;
            }
            $currency = Currency::firstOrCreate([Currency::FIELD_CODE => $DTO->currency]);
            $product->prices()->delete();
            $product->prices()->create(
                array_merge(
                    $DTO->except('product_code', 'currency')->toArray(),
                    ['currency_id' => $currency->id],
                )
            );
        }
    }

    /**
     * @param string $action
     * @param array $data
     *
     * @return SimpleXMLElement|null
     * @throws Exception
     */
    private function request(string $action, array $data): ?SimpleXMLElement
    {
        $message = ArrayToXml::convert(
            [
                'actionseg' => $action,
                'searchseg' => $data,
            ],
            'message'
        );

        try {
            $response = $this->client->request(
                'GET',
                $this->endpoint,
                [
                    'query' => [
                        'actioncode'   => 'HOSTXML',
                        'clientlocseq' => $this->locationSequence,
                        'userid'       => $this->userID,
                        'password'     => $this->password,
                        'message'      => $message,
                    ],
                ]
            );
        } catch (Throwable $exception) {
            Log::error('JONVIEW API PROVIDER, request is failed', [$exception->getMessage()]);
            throw new RuntimeException('JONVIEW API PROVIDER, request is failed');
        }

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new RuntimeException('JONVIEW API PROVIDER, request is failed');
        }

        $xml = new SimpleXMLElement($response->getBody()->getContents() ?: '<root></root>');

        if (self::REQUEST_STATUS_OK === (string)$xml->actionseg->status) {
            return $xml;
        }

        return null;
    }

    /**
     * @param string $requestHash
     *
     * @return bool
     */
    private function isRequestHash(string $requestHash): bool
    {
        RequestLog::where(RequestLog::FIELD_REQUEST, $requestHash)
            ->where(RequestLog::FIELD_ACTUAL_DATE, '<', today())
            ->delete();

        return RequestLog::where(RequestLog::FIELD_REQUEST, $requestHash)
            ->where(RequestLog::FIELD_ACTUAL_DATE, '>=', today())
            ->exists();
    }

    /**
     * @param string $requestHash
     * @param DateTimeInterface $actualDate
     */
    private function requestLog(string $requestHash, DateTimeInterface $actualDate): void
    {
        RequestLog::firstOrCreate(
            [
                RequestLog::FIELD_REQUEST     => $requestHash,
                RequestLog::FIELD_ACTUAL_DATE => $actualDate,
            ]
        );
    }

    /**
     * @param string $actionseg
     * @param string $searchType
     * @param string|null $cityCode
     * @param string|null $productTypeCode
     * @param DateTimeInterface|null $fromDate
     * @param DateTimeInterface|null $toDate
     *
     * @return string
     */
    private function createRequestHash(
        string $actionseg,
        string $searchType,
        string $cityCode = null,
        string $productTypeCode = null,
        DateTimeInterface $fromDate = null,
        DateTimeInterface $toDate = null
    ): string {
        return implode(
            '.',
            [
                $actionseg,
                $searchType,
                $cityCode,
                $productTypeCode,
                $fromDate ? $fromDate->format('Y-m-d') : '',
                $toDate ? $toDate->format('Y-m-d') : '',
            ]
        );
    }

    /**
     * @param string $productTypeCode
     *
     * @return int
     */
    private function getProductTypeID(string $productTypeCode): int
    {
        if (!isset($this->productTypes[$productTypeCode])) {
            $this->productTypes[$productTypeCode] =
                ProductType::where(ProductType::FIELD_CODE, $productTypeCode)->first()->id;
        }

        return $this->productTypes[$productTypeCode];
    }

    /**
     * @param string $supplierCode
     *
     * @return int
     * @throws Exception
     */
    private function getSupplierID(string $supplierCode): ?int
    {
        info('get supplier', [$supplierCode]);
        if (!isset($this->suppliers[$supplierCode])) {
            $supplier = Supplier::where(Supplier::FIELD_CODE, $supplierCode)->first();
            info('find supplier', [$supplier]);
            if (!$supplier) {
                $supplierDTO = $this->getDSSup($supplierCode);
                info('supplier DTO', [$supplierDTO]);
                if (null !== $supplierDTO) {
                    $supplier = $this->supplierService->createSupplier($supplierDTO);
                }
            }

            if (null === $supplier) {
                return null;
            }
            $this->suppliers[$supplierCode] = $supplier->id;
        }

        return $this->suppliers[$supplierCode];
    }

    /**
     * @param SimpleXMLElement $suppliersList
     * @param City|null $city
     *
     * @return array
     */
    private function getSuppliersData(SimpleXMLElement $suppliersList, City $city = null): array
    {
        $data = [];
        foreach ($suppliersList->listrecord as $record) {
            $activities = [];
            $amenities = [];
            $images = [];

            if ($record->activitieslistseg->actiitem) {
                foreach ($record->activitieslistseg->actiitem as $actiitem) {
                    $activities[] = [Activity::FIELD_NAME => (string)$actiitem->actiname];
                }
            }

            if ($record->amenitieslistseg->amenitem) {
                foreach ($record->amenitieslistseg->amenitem as $amenitem) {
                    $amenities[] = [Amenity::FIELD_NAME => (string)$amenitem->amenname];
                }
            }

            if ($record->imageslistseg->picitem) {
                foreach ($record->imageslistseg->picitem as $picitem) {
                    $images[] = [Image::FIELD_PATH => (string)$picitem->picpath];
                }
            }

            if (null === $city) {
                $city = City::where('name', $record->city)->firstOrFail();
            }

            $data[] = new SupplierDTO(
                [
                    'code'        => (string)$record->suppliercode,
                    'name'        => Str::title(Str::lower((string)$record->suppliername)),
                    'description' => (string)$record->description,
                    'rating'      => (string)$record->rating,
                    'address1'    => (string)$record->address1,
                    'address2'    => (string)$record->address2,
                    'address3'    => (string)$record->address3,
                    'address4'    => (string)$record->address3,
                    'phone'       => (string)$record->telephone,
                    'fax'         => (string)$record->fax,
                    'latitude'    => (float)$record->geocode->latitude,
                    'longitude'   => (float)$record->geocode->longitude,
                    'city_id'     => $city->id,
                    'activities'  => $activities,
                    'amenities'   => $amenities,
                    'images'      => $images,
                ]
            );
        }

        return $data;
    }
}
