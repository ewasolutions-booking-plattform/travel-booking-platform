<?php

namespace App\Traits;

trait CanTableName
{
    public static function tableName()
    {
        return (new self())->getTable();
    }
}
