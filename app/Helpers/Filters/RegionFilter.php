<?php

namespace App\Helpers\Filters;

use App\Models\Country;
use App\Models\Region;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class RegionFilter
{
    public static function filters(): void
    {
        CRUD::addFilter(
            [
                'name'  => 'country',
                'type'  => 'select2',
                'label' => 'Country',
            ],
            Country::get()->pluck(Country::FIELD_NAME, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', Region::FIELD_COUNTRY, $value);
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'code',
                'type'  => 'text',
                'label' => 'Code',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', Region::FIELD_CODE, 'LIKE', "$value%");
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'name',
                'type'  => 'text',
                'label' => 'Region',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', Region::FIELD_NAME, 'LIKE', "$value%");
            }
        );
    }
}
