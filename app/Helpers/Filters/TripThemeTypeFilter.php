<?php

namespace App\Helpers\Filters;

use App\Models\TripTheme;
use App\Models\TripThemeType;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TripThemeTypeFilter
{
    public static function tripTheme(): void
    {
        CRUD::addFilter(
            [
                'label' => 'Trip theme',
                'type'  => 'select2',
                'name'  => TripThemeType::FIELD_TRIP_THEME,
            ],
            TripTheme::get()->pluck(TripTheme::FIELD_TITLE, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', TripThemeType::FIELD_TRIP_THEME, $value);
            }
        );
    }
}
