<?php

namespace App\Helpers\Filters;

use App\Models\Organizer;
use App\Models\RoomCategory;
use App\Models\RoomInfo;
use App\Models\RoomType;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class RoomInfoFilter
{
    public static function filters(): void
    {
        self::organizer();
        self::roomCategory();
        self::roomType();
    }

    private static function organizer(): void
    {
        CRUD::addFilter(
            [
                'name'  => 'organizer',
                'type'  => 'select2',
                'label' => 'Organizer',
            ],
            Organizer::get()->pluck(Organizer::FIELD_NAME, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', RoomInfo::FIELD_ORGANIZER, $value);
            }
        );
    }

    private static function roomCategory(): void
    {
        CRUD::addFilter(
            [
                'name'  => 'category',
                'type'  => 'select2',
                'label' => 'Room category',
            ],
            RoomCategory::get()->pluck(RoomCategory::FIELD_NAME, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', RoomInfo::FIELD_CATEGORY, $value);
            }
        );
    }

    private static function roomType(): void
    {
        CRUD::addFilter(
            [
                'name'  => 'type',
                'type'  => 'select2',
                'label' => 'Room type',
            ],
            RoomType::get()->pluck(RoomType::FIELD_TITLE, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', RoomInfo::FIELD_TYPE, $value);
            }
        );
    }
}
