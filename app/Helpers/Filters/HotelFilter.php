<?php

namespace App\Helpers\Filters;

use App\Models\City;
use App\Models\Hotel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class HotelFilter
{
    public static function filters(): void
    {
        CRUD::addFilter(
            [
                'label' => 'City',
                'name'  => 'City',
                'type'  => 'select2',
            ],
            City::get()->pluck(City::FIELD_NAME, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', Hotel::FIELD_CITY, $value);
            }
        );

        CRUD::addFilter(
            [
                'label' => 'Name',
                'name'  => 'Name',
                'type'  => 'text',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', Hotel::FIELD_NAME, 'LIKE', "%$value%");
            }
        );
    }
}
