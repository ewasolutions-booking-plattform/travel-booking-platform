<?php

namespace App\Helpers\Filters;

use App\Models\Continent;
use App\Models\Country;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class CountryFilter
{
    public static function filters(): void
    {
        CRUD::addFilter(
            [
                'name'  => 'continent',
                'type'  => 'select2',
                'label' => 'Continent',
            ],
            Continent::get()->pluck(Continent::FIELD_NAME, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', Country::FIELD_CONTINENT, $value);
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'code',
                'type'  => 'text',
                'label' => 'Code',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', Country::FIELD_CODE, 'LIKE', "$value%");
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'name',
                'type'  => 'text',
                'label' => 'Country',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', Country::FIELD_NAME, 'LIKE', "$value%");
            }
        );
    }
}
