<?php

namespace App\Helpers\Filters;

use App\Models\ProductType;
use App\Models\ProductTypeGroup;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;

class ProductTypeFilters
{
    public static function filters(CrudController $context): void
    {
        CRUD::addFilter(
            [
                'name'  => 'code',
                'type'  => 'text',
                'label' => 'Code',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', ProductType::FIELD_CODE, 'LIKE', "$value%");
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'description',
                'type'  => 'text',
                'label' => 'Description',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', ProductType::FIELD_DESCRIPTION, 'LIKE', "%$value%");
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'group',
                'type'  => 'select2_multiple',
                'label' => 'Group',
            ],
            ProductTypeGroup::get()->pluck(ProductTypeGroup::FIELD_NAME, 'id')->toArray() + [-1 => 'Not set'],
            function ($value) use ($context) {
                $values = json_decode($value);
                if ('-1' === $values[0]) {
                    $context->crud->query = $context->crud->query->whereDoesntHave('typeGroups');
                } else {
                    $context->crud->query = $context->crud->query->whereHas(
                        'typeGroups',
                        function (Builder $query) use ($value) {
                            $values = json_decode($value);
                            $query->whereIn('product_type_group_id', $values);
                        }
                    );
                }
            }
        );
    }
}
