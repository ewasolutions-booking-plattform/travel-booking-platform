<?php

namespace App\Helpers\Filters;

use App\Models\City;
use App\Models\Continent;
use App\Models\Country;
use App\Models\Region;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class CityFilter
{
    public static function filters(): void
    {
        CRUD::addFilter(
            [
                'name'  => 'Region',
                'type'  => 'select2',
                'label' => 'Region',
            ],
            Region::get()->pluck(Region::FIELD_NAME, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', City::FIELD_REGION, $value);
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'code',
                'type'  => 'text',
                'label' => 'Code',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', City::FIELD_CODE, 'LIKE', "$value%");
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'name',
                'type'  => 'text',
                'label' => 'City',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', City::FIELD_NAME, 'LIKE', "$value%");
            }
        );
    }
}
