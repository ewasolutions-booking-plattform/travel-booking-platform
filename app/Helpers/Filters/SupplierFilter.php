<?php

namespace App\Helpers\Filters;

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Models\Supplier;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class SupplierFilter
{
    public static function filters(): void
    {
        CRUD::addFilter(
            [
                'name'  => 'code',
                'type'  => 'text',
                'label' => 'Code',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', Supplier::FIELD_CODE, 'LIKE', "$value%");
            }
        );

        CRUD::addFilter(
            [
                'name'  => 'name',
                'type'  => 'text',
                'label' => 'Supplier',
            ],
            false,
            function ($value) {
                CRUD::addClause('where', Supplier::FIELD_NAME, 'LIKE', "$value%");
            }
        );

        CRUD::addFilter(
            [
                'name'  => 'Country',
                'type'  => 'select2',
                'label' => 'Country',
            ],
            Country::get()->pluck(Country::FIELD_NAME, 'id')->toArray(),
            function ($value) {
                $cities = City::whereHas(
                    'country',
                    function ($query) use ($value) {
                        $query->where('countries.id', $value);
                    }
                )
                    ->pluck('id', 'id')
                    ->toArray();
                CRUD::addClause('whereIn', Supplier::FIELD_CITY, $cities);
            }
        );

        CRUD::addFilter(
            [
                'name'  => 'Region',
                'type'  => 'select2',
                'label' => 'Region',
            ],
            function () {
                $country = CRUD::getFilter('Country')->currentValue;
                return Region::where(Region::FIELD_COUNTRY, $country)
                    ->pluck(Region::FIELD_NAME, 'id')
                    ->toArray();
            },
            function ($value) {
                $cities = City::where(City::FIELD_REGION, $value)
                    ->pluck('id', 'id')
                    ->toArray();
                CRUD::addClause('whereIn', Supplier::FIELD_CITY, $cities);
            }
        );

        CRUD::addFilter(
            [
                'name'  => 'City',
                'type'  => 'select2',
                'label' => 'City',
            ],
            function () {
                $region = CRUD::getFilter('Region')->currentValue;
                info($region);
                return City::where(City::FIELD_REGION, $region)
                    ->pluck(City::FIELD_NAME, 'id')
                    ->toArray();
            },
            function ($value) {
                CRUD::addClause('where', Supplier::FIELD_CITY, $value);
            }
        );
    }
}
