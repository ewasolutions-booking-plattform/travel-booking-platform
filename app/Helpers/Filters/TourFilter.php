<?php

namespace App\Helpers\Filters;

use App\Models\Country;
use App\Models\Region;
use App\Models\Tour;
use App\Models\TripTheme;
use App\Models\TripThemeType;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TourFilter
{
    public static function filters()
    {
        CRUD::addFilter(
            [
                'name'  => 'country',
                'type'  => 'select2',
                'label' => 'Country',
            ],
            Country::get()->pluck(Country::FIELD_NAME, 'id')->toArray(),
            function ($value) {
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'region',
                'type'  => 'select2',
                'label' => 'Region',
            ],
            Region::get()->pluck(Region::FIELD_NAME, 'id')->toArray(),
            function ($value) {
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'from_to',
                'type'  => 'date_range',
                'label' => 'Start date',
            ],
            false,
            function ($value) {
                $date = json_decode($value);
                CRUD::addClause('where', Tour::FIELD_TRIP_START, '>=', $date->from);
                CRUD::addClause('where', Tour::FIELD_TRIP_START, '<=', $date->to);
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'trip_theme',
                'type'  => 'select2',
                'label' => 'Trip theme',
            ],
            TripTheme::get()->pluck(TripTheme::FIELD_TITLE, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', Tour::FIELD_TRIP_THEME, $value);
            }
        );
        CRUD::addFilter(
            [
                'name'  => 'trip_type',
                'type'  => 'select2',
                'label' => 'Trip type',
            ],
            TripThemeType::get()->pluck(TripThemeType::FIELD_TITLE, 'id')->toArray(),
            function ($value) {
                CRUD::addClause('where', Tour::FIELD_TRIP_THEME_TYPE, $value);
            }
        );
        CRUD::addFilter(
            [
                'name'  => Tour::FIELD_IS_PUBLISH,
                'type'  => 'select2',
                'label' => 'Publish status',
            ],
            [0 => 'Not published', 1 => 'Published'],
            function ($value) {
                if ($value !== -1) {
                    CRUD::addClause('where', Tour::FIELD_IS_PUBLISH, $value);
                }
            }
        );
    }
}
