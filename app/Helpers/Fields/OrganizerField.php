<?php

namespace App\Helpers\Fields;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class OrganizerField
{
    public static function field(string $fieldName, string $tabName = null): void
    {
        CRUD::addField([
            'label'                => 'Organizer',
            'type'                 => 'relationship',
            'name'                 => $fieldName,
            'minimum_input_length' => 0,
            'ajax'                 => true,
            'inline_create'        => true,
            'tab'                  => $tabName,
        ]);
    }
}
