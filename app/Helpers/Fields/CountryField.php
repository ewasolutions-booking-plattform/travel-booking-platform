<?php

namespace App\Helpers\Fields;

use App\Models\Country;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class CountryField
{
    public static function fields(): void
    {
        self::continent();
        self::country();
    }

    public static function continent(): void
    {
        ContinentField::field(Country::FIELD_CONTINENT);
    }

    public static function country(): void
    {
        CRUD::addField([
            'label' => 'Country',
            'name'  => Country::FIELD_NAME,
        ]);
    }
}
