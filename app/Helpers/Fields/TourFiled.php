<?php

namespace App\Helpers\Fields;

use App\Models\AgeCategory;
use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Models\Tour;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TourFiled
{
    private const TAB_TOUR = 'Tour';
    private const TAB_INFO = 'Tour info';

    public static function fields(): void
    {
        self::showTabTour();
        self::showTabInfo();
    }

    private static function showTabTour(): void
    {
        CRUD::addField(
            [
                'label' => 'Title',
                'type'  => 'text',
                'name'  => Tour::FIELD_TITLE,
                'tab'   => self::TAB_TOUR,
            ]
        );
        CRUD::addField(
            [
                'label' => 'Description',
                'type'  => 'tinymce',
                'name'  => Tour::FIELD_DESCRIPTION,
                'tab'   => self::TAB_TOUR,
            ]
        );
        CRUD::addField(
            [
                'label' => 'Slug',
                'type'  => 'text',
                'name'  => Tour::FIELD_SLUG,
                'tab'   => self::TAB_TOUR,
            ]
        );
        CRUD::addField(
            [
                'label'         => 'Age category',
                'type'          => 'select2_multiple',
                'name'          => 'ageCategories',
                'entity'        => 'ageCategories',
                'model'         => AgeCategory::class,
                'inline_create' => [
                    'entity' => 'age_category',
                ],
                'tab'           => self::TAB_TOUR,
            ]
        );
        CRUD::addField(
            [
                'name'                => Tour::FIELD_TRIP_START,
                'type'                => 'date_picker',
                'label'               => 'Start Trip Date',
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'yyyy-mm-dd',
                    'language' => 'en',
                ],
                'tab'                 => self::TAB_TOUR,
            ]
        );
        CRUD::addField(
            [
                'name'                => Tour::FIELD_TRIP_END,
                'type'                => 'date_picker',
                'label'               => 'End Trip Date',
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'yyyy-mm-dd',
                    'language' => 'en',
                ],
                'tab'                 => self::TAB_TOUR,
            ]
        );

        CRUD::addField(
            [
                'name'  => Tour::FIELD_USER_PRICE,
                'type'  => 'text',
                'label' => 'User price',
                'tab'   => self::TAB_TOUR,
            ]
        );

        CRUD::addField(
            [
                'name'  => Tour::FIELD_IS_PUBLISH,
                'type'  => 'checkbox',
                'label' => 'Publish on the front-end',
                'tab'   => self::TAB_TOUR,
            ]
        );
    }

    private static function showTabInfo(): void
    {
        CRUD::addField(
            [
                'label'                => 'Country',
                'type'                 => 'relationship',
                'name'                 => 'country',
                'entity'               => 'country',
                'model'                => Country::class,
                'minimum_input_length' => 0,
                'ajax'                 => true,
                'tab'                  => self::TAB_INFO,
            ]
        );
        CRUD::addField(
            [
                'label'                => 'Region',
                'type'                 => 'relationship',
                'name'                 => 'region',
                'entity'               => 'region',
                'model'                => Region::class,
                'dependencies'         => [Tour::FIELD_COUNTRY],
                'minimum_input_length' => 0,
                'ajax'                 => true,
                'tab'                  => self::TAB_INFO,
            ]
        );
        CRUD::addField(
            [
                'label'                => 'City',
                'type'                 => 'relationship',
                'name'                 => 'city',
                'entity'               => 'city',
                'model'                => City::class,
                'dependencies'         => [Tour::FIELD_REGION],
                'minimum_input_length' => 0,
                'ajax'                 => true,
                'tab'                  => self::TAB_INFO,
            ]
        );

        CRUD::addField(
            [
                'label'                => 'Trip theme',
                'type'                 => 'relationship',
                'name'                 => Tour::FIELD_TRIP_THEME,
                'entity'               => 'tripTheme',
                'minimum_input_length' => 0,
                'ajax'                 => true,
                'inline_create'        => true,
                'tab'                  => self::TAB_INFO,
            ]
        );
        CRUD::addField(
            [
                'label'                => 'Trip theme type',
                'type'                 => 'relationship',
                'name'                 => Tour::FIELD_TRIP_THEME_TYPE,
                'entity'               => 'tripThemeType',
                'minimum_input_length' => 0,
                'ajax'                 => true,
                'inline_create'        => true,
                'tab'                  => self::TAB_INFO,
            ]
        );
    }
}
