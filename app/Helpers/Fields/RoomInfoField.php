<?php

namespace App\Helpers\Fields;

use App\Enums\CurrencyEnum;
use App\Models\RoomInfo;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class RoomInfoField
{
    public static function fields(): void
    {
        OrganizerField::field(RoomInfo::FIELD_ORGANIZER);
        self::roomCategory();
        self::roomType();
        self::currency();
        CRUD::addField([
            'label' => 'Base price',
            'name'  => RoomInfo::FIELD_BASE_PRISE,
        ]);
        CRUD::addField([
            'label' => 'User price',
            'name'  => RoomInfo::FIELD_USER_PRISE,
        ]);
        CRUD::addField([
            'label' => 'Description',
            'type'  => 'tinymce',
            'name'  => RoomInfo::FIELD_DESCRIPTION,
        ]);
        CRUD::addField([
            'label' => 'Price info',
            'type'  => 'tinymce',
            'name'  => RoomInfo::FIELD_PRICE_INFO,
        ]);
    }

    private static function roomCategory(): void
    {
        CRUD::addField([
            'label'                => 'Room category',
            'type'                 => 'relationship',
            'name'                 => RoomInfo::FIELD_CATEGORY,
            'entity'               => 'roomCategory',
            'ajax'                 => true,
            'minimum_input_length' => 0,
            'inline_create'        => true,
        ]);
    }

    private static function roomType(): void
    {
        CRUD::addField([
            'label'                => 'Room type',
            'type'                 => 'relationship',
            'name'                 => RoomInfo::FIELD_TYPE,
            'entity'               => 'roomType',
            'ajax'                 => true,
            'minimum_input_length' => 0,
            'inline_create'        => true,
        ]);
    }

    private static function currency(): void
    {
        CRUD::addField([
            'label'   => 'Currency',
            'type'    => 'select_from_array',
            'name'    => RoomInfo::FIELD_CURRENCY,
            'options' => CurrencyEnum::toArray(),
        ]);
    }
}
