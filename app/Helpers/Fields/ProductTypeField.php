<?php

namespace App\Helpers\Fields;

use App\Models\ProductType;
use App\Models\ProductTypeGroup;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class ProductTypeField
{
    public static function fields(): void
    {
        CRUD::addField(
            [
                'label'  => 'Group',
                'type'   => 'select2_multiple',
                'name'   => 'typeGroups',
                'entity' => 'typeGroups',
                'model'  => ProductTypeGroup::class,
                'pivot'  => true,
            ]
        );

        CRUD::addField(
            [
                'label'      => 'Code',
                'type'       => 'text',
                'name'       => ProductType::FIELD_CODE,
                'attributes' => [
                    'readonly' => 'readonly',
                ],
            ]
        );

        CRUD::addField(
            [
                'label'      => 'Description',
                'type'       => 'text',
                'name'       => ProductType::FIELD_DESCRIPTION,
                'attributes' => [
                    'readonly' => 'readonly',
                ],
            ]
        );
    }
}
