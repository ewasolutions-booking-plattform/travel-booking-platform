<?php

namespace App\Helpers\Fields;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class ContinentField
{
    public static function field(string $fieldName, string $tabName = null): void
    {
        CRUD::addField([
            'label'                => 'Continent',
            'type'                 => 'relationship',
            'name'                 => $fieldName,
            'ajax'                 => true,
            'minimum_input_length' => 0,
            'inline_create'        => true,
            'tab'                  => $tabName,
        ]);
    }
}
