<?php

namespace App\Helpers\Fields;

use App\Models\TripThemeType;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TripThemeTypeField
{
    public static function fields(): void
    {
        self::tripTheme();
        self::field();
    }

    public static function field(): void
    {
        CRUD::addField([
            'label' => 'Trip theme type',
            'name'  => TripThemeType::FIELD_TITLE,
        ]);
    }

    public static function tripTheme(): void
    {
        CRUD::addField([
            'label'                => 'Trip theme',
            'type'                 => 'relationship',
            'name'                 => TripThemeType::FIELD_TRIP_THEME,
            'entity'               => 'tripTheme',
            'minimum_input_length' => 0,
            'ajax'                 => true,
            'inline_create'        => true,
        ]);
    }
}
