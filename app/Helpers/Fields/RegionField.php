<?php

namespace App\Helpers\Fields;

use App\Models\Region;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class RegionField
{
    public static function fields(): void
    {
        self::country();
        self::region();
    }

    public static function country(): void
    {
        CRUD::addField([
            'label'                => 'Country',
            'type'                 => 'relationship',
            'name'                 => Region::FIELD_COUNTRY,
            'ajax'                 => true,
            'minimum_input_length' => 0,
            'inline_create'        => true,
        ]);
    }

    public static function region(): void
    {
        CRUD::addField([
            'label' => 'Region',
            'name'  => Region::FIELD_NAME,
        ]);
    }
}
