<?php

namespace App\Helpers\Fields;

use App\Models\TripInfo;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TripInfoField
{
    public static function fields(): void
    {
        CRUD::addField([
            'label'                => 'Trip info type',
            'type'                 => 'relationship',
            'name'                 => TripInfo::FIELD_INFO_TYPE,
            'entity'               => 'tripInfoType',
            'ajax'                 => true,
            'minimum_input_length' => 0,
            'inline_create'        => true,
        ]);

        CRUD::addField([
            'label' => 'Guests in group',
            'type'  => 'text',
            'name'  => TripInfo::FIELD_NUMBER_IN_GROUP,
        ]);

        CRUD::addField([
            'label' => 'Groups of tour',
            'type'  => 'text',
            'name'  => TripInfo::FIELD_NUMBER_OF_GROUPS,
        ]);

        CRUD::addField([
            'label' => 'Description',
            'type'  => 'tinymce',
            'name'  => TripInfo::FIELD_DESCRIPTION,
        ]);

        CRUD::addField([
            'label' => 'Included services',
            'type'  => 'tinymce',
            'name'  => TripInfo::FIELD_INCLUDED_SERVICES,
        ]);

        CRUD::addField([
            'label' => 'Arrival and Departure info',
            'type'  => 'tinymce',
            'name'  => TripInfo::FIELD_ARRIVAL_DEPARTURE_INFO,
        ]);

        CRUD::addField([
            'label' => 'Special info',
            'type'  => 'tinymce',
            'name'  => TripInfo::FIELD_SPECIAL_INFO,
        ]);

        CRUD::addField([
            'label' => 'Latitude',
            'type'  => 'text',
            'name'  => TripInfo::FIELD_LATITUDE,
        ]);

        CRUD::addField([
            'label' => 'Longitude',
            'type'  => 'text',
            'name'  => TripInfo::FIELD_LONGITUDE,
        ]);

        CRUD::addField([
            'name'   => 'photos',
            'label'  => 'Photos',
            'type'   => 'upload_multiple',
            'upload' => true,
            'disk'   => 'public',
        ]);
    }
}
