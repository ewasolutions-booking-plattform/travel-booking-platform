<?php

namespace App\Helpers\Columns;

use App\Models\Booking;
use App\Models\City;
use App\Models\Region;
use App\Models\Tour;
use App\Models\User;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class BookingColumn
{
    public static function columns(): void
    {
        CountryColumn::column(Booking::FIELD_COUNTRY);
        CRUD::addColumn(
            [
                'label'     => 'Region',
                'name'      => Booking::FIELD_REGION,
                'entity'    => 'region',
                'attribute' => Region::FIELD_NAME,
                'model'     => Region::class,
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'City',
                'name'      => Booking::FIELD_CITY,
                'entity'    => 'city',
                'attribute' => City::FIELD_NAME,
                'model'     => City::class,
            ]
        );
        TripThemeColumn::column(Booking::FIELD_TRIP_THEME);
        CRUD::addColumn(
            [
                'label'     => 'Email',
                'name'      => Booking::FIELD_CLIENT_EMAIL,
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'Phone',
                'name'      => Booking::FIELD_CLIENT_PHONE,
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'Full Name',
                'name'      => 'fullName',
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'Duration',
                'name'      => 'showDuration',
            ]
        );

        CRUD::addColumn(
            [
                'label'     => 'Budget',
                'name'      => 'showBudget',
            ]
        );

        CRUD::addColumn(
            [
                'label'     => 'Tour',
                'name'      => Booking::FIELD_TOUR,
                'entity'    => 'tour',
                'attribute' => Tour::FIELD_TITLE,
                'model'     => Tour::class,
            ]
        );

        CRUD::addColumn(
            [
                'label'     => 'Operator',
                'name'      => Booking::FK_OPERATOR,
                'entity'    => 'operator',
                'attribute' => 'email',
                'model'     => User::class,
            ]
        );
    }

    public static function previewColumns(): void
    {
    }
}
