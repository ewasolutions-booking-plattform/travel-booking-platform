<?php

namespace App\Helpers\Columns;

use App\Models\City;
use App\Models\Hotel;
use App\Models\Property;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class HotelColumn
{
    public static function columns(): void
    {
        CRUD::addColumn([
            'label'     => 'City',
            'name'      => Hotel::FIELD_CITY,
            'entity'    => 'city',
            'attribute' => City::FIELD_NAME,
            'model'     => City::class,
        ]);

        CRUD::addColumn([
            'label' => 'Hotel',
            'name'  => Hotel::FIELD_NAME,
        ]);

        CRUD::addColumn([
            'label'     => 'Properties',
            'name'      => 'Properties',
            'type'      => 'relationship',
            'entity'    => 'properties',
            'attribute' => Property::FIELD_NAME,
            'model'     => Property::class,
        ]);

        CRUD::addColumn([
            'label' => 'Address',
            'name'  => Hotel::FIELD_ADDRESS1,
        ]);

        CRUD::addColumn([
            'label' => 'Phone',
            'name'  => Hotel::FIELD_PHONE,
        ]);

        CRUD::addColumn([
            'label' => 'Fax',
            'name'  => Hotel::FIELD_FAX,
        ]);
    }
}
