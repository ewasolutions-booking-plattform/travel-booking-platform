<?php

namespace App\Helpers\Columns;

use App\Models\Country;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class CountryColumn
{
    public static function columns(): void
    {
        ContinentColumn::column(Country::FIELD_CONTINENT);
        CRUD::addColumn([
            'label'     => 'Code',
            'name'      => Country::FIELD_CODE,
        ]);
    }

    public static function column(string $fieldName): void
    {
        CRUD::addColumn([
            'label'     => 'Country',
            'name'      => $fieldName,
            'entity'    => 'country',
            'attribute' => Country::FIELD_NAME,
            'model'     => Country::class,
        ]);
    }
}
