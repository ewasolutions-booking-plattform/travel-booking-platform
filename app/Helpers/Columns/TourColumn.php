<?php

namespace App\Helpers\Columns;

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Models\Tour;
use App\Models\TripThemeType;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TourColumn
{
    public static function columns(): void
    {
        self::column(Tour::FIELD_TITLE);
        CRUD::addColumn(
            [
                'label' => 'Start date',
                'type'  => 'date',
                'name'  => Tour::FIELD_TRIP_START,
            ]
        );
        CRUD::addColumn(
            [
                'label' => 'End date',
                'type'  => 'date',
                'name'  => Tour::FIELD_TRIP_END,
            ]
        );

        TripThemeColumn::column(Tour::FIELD_TRIP_THEME);
        CRUD::addColumn(
            [
                'label'     => 'Trip theme type',
                'name'      => Tour::FIELD_TRIP_THEME_TYPE,
                'entity'    => 'tripThemeType',
                'attribute' => TripThemeType::FIELD_TITLE,
                'model'     => TripThemeType::class,
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'Country',
                'name'      => Tour::FIELD_COUNTRY,
                'entity'    => 'country',
                'attribute' => Country::FIELD_NAME,
                'model'     => Country::class,
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'Region',
                'name'      => Tour::FIELD_REGION,
                'entity'    => 'region',
                'attribute' => Region::FIELD_NAME,
                'model'     => Region::class,
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'City',
                'name'      => Tour::FIELD_CITY,
                'entity'    => 'city',
                'attribute' => City::FIELD_NAME,
                'model'     => City::class,
            ]
        );
        CRUD::addColumn(
            [
                'label'     => 'Published',
                'name'      => Tour::FIELD_IS_PUBLISH,
                'type' => 'boolean',
                'options' => [0 => 'Not published', 1 => 'Published']
            ]
        );
    }

    public static function column(string $fieldName): void
    {
        CRUD::addColumn(
            [
                'label' => 'Title',
                'type'  => 'text',
                'name'  => $fieldName,
            ]
        );
    }

    public static function previewColumns(): void
    {
        CRUD::addColumn(
            [
                'label' => 'Created date',
                'type'  => 'datetime',
                'name'  => Tour::CREATED_AT,
            ]
        );
    }
}
