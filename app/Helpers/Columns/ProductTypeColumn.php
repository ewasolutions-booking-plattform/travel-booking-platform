<?php

namespace App\Helpers\Columns;

use App\Models\ProductTypeGroup;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class ProductTypeColumn
{
    public static function columns(): void
    {
        CRUD::addColumn(
            [
                'label'     => 'Groups',
                'name'      => 'typeGroups',
                'entity'    => 'typeGroups',
                'attribute' => ProductTypeGroup::FIELD_NAME,
                'model'     => ProductTypeGroup::class,
            ]
        );
    }
}
