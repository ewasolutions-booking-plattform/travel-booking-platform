<?php

namespace App\Helpers\Columns;

use App\Models\TripThemeType;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TripThemeTypeColumn
{
    public static function columns(): void
    {
        CRUD::removeColumn(TripThemeType::FIELD_TRIP_THEME);
        self::column();
        TripThemeColumn::column(TripThemeType::FIELD_TRIP_THEME);
    }

    public static function column(): void
    {
        CRUD::addColumn([
            'label'     => 'Trip theme type',
            'name'      => TripThemeType::FIELD_TITLE,
        ]);
    }
}
