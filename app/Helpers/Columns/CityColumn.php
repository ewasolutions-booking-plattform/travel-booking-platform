<?php

namespace App\Helpers\Columns;

use App\Models\City;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class CityColumn
{
    public static function columns(): void
    {
        RegionColumn::column(City::FIELD_REGION);
        CRUD::addColumn([
            'label' => 'Code',
            'name'  => City::FIELD_CODE,
        ]);
        CRUD::addColumn([
            'label' => 'City',
            'name'  => City::FIELD_NAME,
        ]);
        CRUD::addColumn([
            'label' => 'Description',
            'name'  => City::FIELD_DESCRIPTION,
        ]);
    }
}
