<?php

namespace App\Helpers\Columns;

use App\Models\Continent;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class ContinentColumn
{
    public static function column(string $fieldName): void
    {
        CRUD::addColumn([
            'label'     => 'Continent',
            'type'      => 'select',
            'name'      => $fieldName,
            'entity'    => 'continent',
            'attribute' => Continent::FIELD_NAME,
            'model'     => Continent::class,
        ]);
    }
}
