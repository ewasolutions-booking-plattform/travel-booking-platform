<?php

namespace App\Helpers\Columns;

use App\Models\TripInfo;
use App\Models\TripInfoType;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TripInfoColumn
{
    public static function columns(): void
    {
        CRUD::addColumn([
            'label'     => 'Trip info type',
            'name'      => TripInfo::FIELD_INFO_TYPE,
            'entity'    => 'tripInfoType',
            'attribute' => TripInfoType::FIELD_TITLE,
            'model'     => TripInfoType::class,
        ]);

        CRUD::addColumn([
            'label' => 'Guest in group',
            'name'  => TripInfo::FIELD_NUMBER_IN_GROUP,
        ]);

        CRUD::addColumn([
            'label' => 'Groups of tour',
            'name'  => TripInfo::FIELD_NUMBER_OF_GROUPS,
        ]);

        CRUD::addColumn([
            'label' => 'Latitude',
            'name'  => TripInfo::FIELD_LATITUDE,
        ]);

        CRUD::addColumn([
            'label' => 'Longitude',
            'name'  => TripInfo::FIELD_LONGITUDE,
        ]);
    }

    public static function previewColumns(): void
    {
        self::columns();
        CRUD::addColumn([
            'label' => 'Description',
            'name'  => TripInfo::FIELD_DESCRIPTION,
        ]);

        CRUD::addColumn([
            'label' => 'Included services',
            'name'  => TripInfo::FIELD_INCLUDED_SERVICES,
        ]);

        CRUD::addColumn([
            'label' => 'Arrival and Departure',
            'name'  => TripInfo::FIELD_ARRIVAL_DEPARTURE_INFO,
        ]);

        CRUD::addColumn([
            'label' => 'Special info',
            'name'  => TripInfo::FIELD_SPECIAL_INFO,
        ]);

        CRUD::addColumn([
            'label' => 'Latitude',
            'name'  => TripInfo::FIELD_LATITUDE,
        ]);

        CRUD::addColumn([
            'label' => 'Longitude',
            'name'  => TripInfo::FIELD_LONGITUDE,
        ]);
    }
}
