<?php

namespace App\Helpers\Columns;

use App\Models\Organizer;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class OrganizerColumn
{
    public static function column(string $fieldName): void
    {
        CRUD::addColumn([
            'label'     => 'Organizer',
            'name'      => $fieldName,
            'entity'    => 'organizer',
            'attribute' => Organizer::FIELD_NAME,
            'model'     => Organizer::class,
        ]);
    }
}
