<?php

namespace App\Helpers\Columns;

use App\Models\TripTheme;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class TripThemeColumn
{
    public static function column(string $fieldName): void
    {
        CRUD::addColumn([
            'label'     => 'Trip theme',
            'name'      => $fieldName,
            'entity'    => 'tripTheme',
            'attribute' => TripTheme::FIELD_TITLE,
            'model'     => TripTheme::class,
        ]);
    }
}
