<?php

namespace App\Helpers\Columns;

use App\Models\Region;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class RegionColumn
{
    public static function columns(): void
    {
        self::country();
        CRUD::addColumn([
            'label' => 'Code',
            'name'  => Region::FIELD_CODE,
        ]);
        self::column(Region::FIELD_NAME);
    }

    public static function country(): void
    {
        CRUD::addColumn([
            'label' => 'Country',
            'name'  => 'country',
        ]);
    }

    public static function column(string $columnName): void
    {
        CRUD::addColumn([
            'label' => 'Region',
            'name'  => $columnName,
        ]);
    }
}
