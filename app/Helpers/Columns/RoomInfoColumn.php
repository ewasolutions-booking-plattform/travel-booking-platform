<?php

namespace App\Helpers\Columns;

use App\Models\Organizer;
use App\Models\RoomInfo;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class RoomInfoColumn
{
    public static function columns(): void
    {
        CRUD::removeColumn(RoomInfo::FIELD_ORGANIZER);
        CRUD::removeColumn(RoomInfo::FIELD_CATEGORY);
        CRUD::removeColumn(RoomInfo::FIELD_TYPE);
        CRUD::removeColumn(RoomInfo::FIELD_DESCRIPTION);
        CRUD::removeColumn(RoomInfo::FIELD_PRICE_INFO);

        self::organizer();
        self::roomCategory();
        self::roomType();
    }

    private static function organizer(): void
    {
        CRUD::addColumn([
            'label'     => 'Organizer',
            'type'      => 'relationship',
            'name'      => RoomInfo::FIELD_ORGANIZER,
            'entity'    => 'organizer',
            'attribute' => Organizer::FIELD_NAME,
            'model'     => Organizer::class,
        ])->makeFirstColumn();
    }

    private static function roomCategory(): void
    {
        CRUD::addColumn([
            'label'  => 'Room category',
            'type'   => 'relationship',
            'name'   => RoomInfo::FIELD_CATEGORY,
            'entity' => 'roomCategory',
        ])->afterColumn(RoomInfo::FIELD_ORGANIZER);
    }

    private static function roomType(): void
    {
        CRUD::addColumn([
            'label'  => 'Room type',
            'type'   => 'relationship',
            'name'   => RoomInfo::FIELD_TYPE,
            'entity' => 'roomType',
        ])->afterColumn(RoomInfo::FIELD_ORGANIZER);
    }

    public static function description(): void
    {
        CRUD::addColumn([
            'label' => 'Description',
            'name'  => RoomInfo::FIELD_DESCRIPTION,
        ]);
    }

    public static function priceInfo(): void
    {
        CRUD::addColumn([
            'label' => 'Price info',
            'name'  => RoomInfo::FIELD_PRICE_INFO,
        ]);
    }
}
