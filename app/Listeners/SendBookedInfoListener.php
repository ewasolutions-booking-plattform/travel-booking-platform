<?php

namespace App\Listeners;

use App\Events\TourBookedEvent;
use App\Mail\TourBooked;
use Illuminate\Support\Facades\Mail;
use PDF;

class SendBookedInfoListener
{
    public function handle(TourBookedEvent $event)
    {
        $booking = $event->getBooking();

        if ($booking->tour) {
            $filePath = storage_path('./app/'.$booking->id.'-'.$booking->tour->id.'.pdf');
            $pdf = PDF::loadView('booking', ['booking' => $booking]);
            $pdf->save($filePath);
        }

        Mail::to($booking->operator->email)
            ->send(new TourBooked($booking));
    }
}
