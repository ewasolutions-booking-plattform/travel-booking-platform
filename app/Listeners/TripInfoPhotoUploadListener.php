<?php

namespace App\Listeners;

use App\Events\TripInfoPhotoUploadedEvent;
use Illuminate\Support\Facades\File;
use Image;

class TripInfoPhotoUploadListener
{
    public function handle(TripInfoPhotoUploadedEvent $event): void
    {
        $tripInfo = $event->getTripInfo();

        foreach ($tripInfo->photos as $photo) {
            if (!$photo) {
                break;
            }

            $destinationPath = storage_path('app/public').'/trip_info/'.$tripInfo->id;
            $thumbnailsPath = $destinationPath.'/small';
            $middlePath = $destinationPath.'/middle';

            File::makeDirectory($destinationPath, 0777, true, true);
            File::makeDirectory($thumbnailsPath, 0777, true, true);
            File::makeDirectory($middlePath, 0777, true, true);

            $pathData = explode('/', $photo);
            $fileName = array_pop($pathData);

            $img = Image::make(storage_path('app/public').'/'.$photo);

            if (!File::exists($middlePath.'/'.$fileName)) {
                $img->resize(496, 300, function ($const) {
                    $const->aspectRatio();
                })->save($middlePath.'/'.$fileName);
            }

            if (!File::exists($thumbnailsPath.'/'.$fileName)) {
                $img->resize(243, 300, function ($const) {
                    $const->aspectRatio();
                })->save($thumbnailsPath.'/'.$fileName);
            }
        }
    }
}
