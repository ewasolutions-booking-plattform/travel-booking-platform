<?php

namespace App\Console\Commands;

use App\DTO\Jonview\SupplierDTO;
use App\Models\City;
use App\Services\ApiProvider\JonviewAPI;
use App\Services\SupplierService;
use Illuminate\Console\Command;

class JonviewUpdateSuppliers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jonview:update-supplier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update hotels from Jonview API';

    private JonviewAPI $jonviewAPIService;
    private SupplierService $supplierService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(JonviewAPI $jonviewAPIService, SupplierService $supplierService)
    {
        parent::__construct();
        $this->jonviewAPIService = $jonviewAPIService;
        $this->supplierService = $supplierService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle(): int
    {
        $barFormat = "%message%\n %current%/%max% [%bar%] %percent:3s%%";

        $cities = City::cursor();

        $bar = $this->output->createProgressBar($cities->count());
        $bar->setFormat($barFormat);
        $bar->setMessage('Getting a suppliers from jonview API ...');
        $bar->start();

        /** @var City $city */
        foreach ($cities as $city) {
            $supplierDTOs = $this->jonviewAPIService->getDSCity($city);
            if (null === $supplierDTOs) {
                continue;
            }
            /** @var SupplierDTO $supplierDTO */
            foreach ($supplierDTOs as $supplierDTO) {
                $this->supplierService->createSupplier($supplierDTO);
            }
            $bar->advance();
        }
        $bar->finish();
        $this->output->newLine();

        return 0;
    }
}
