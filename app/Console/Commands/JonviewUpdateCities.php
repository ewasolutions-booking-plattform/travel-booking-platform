<?php

namespace App\Console\Commands;

use App\DTO\Jonview\CityDTO;
use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Services\ApiProvider\JonviewAPI;
use Exception;
use Illuminate\Console\Command;

class JonviewUpdateCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jonview:update-cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cities from Jonview API';

    private JonviewAPI $jonviewAPIService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(JonviewAPI $jonviewAPIService)
    {
        parent::__construct();
        $this->jonviewAPIService = $jonviewAPIService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Exception
     */
    public function handle(): int
    {
        $dataCities = $this->jonviewAPIService->getDFCity();

        $barFormat = "%message%\n %current%/%max% [%bar%] %percent:3s%%";
        $bar = $this->output->createProgressBar(count($dataCities));
        $bar->setFormat($barFormat);
        $bar->setMessage('Getting a cities from jonview API ...');
        $bar->start();

        /** @var CityDTO $cityDTO */
        foreach ($dataCities as $cityDTO) {
            $country = Country::firstOrCreate([
                Country::FIELD_CODE => $cityDTO->countryCode,
                Country::FIELD_NAME => $cityDTO->countryName,
            ]);

            $region = Region::firstOrCreate([
                Region::FIELD_CODE    => $cityDTO->provinceCode,
                Region::FIELD_NAME    => $cityDTO->provinceName,
                Region::FIELD_COUNTRY => $country->id,
            ]);

            City::firstOrCreate([
                City::FIELD_CODE        => $cityDTO->code,
                City::FIELD_NAME        => $cityDTO->name,
                City::FIELD_DESCRIPTION => $cityDTO->description,
                City::FIELD_REGION      => $region->id,
            ]);
            $bar->advance();
        }
        $bar->finish();
        $this->output->newLine();

        return 0;
    }
}
