<?php

namespace App\Console\Commands;

use App\Models\ProductType;
use App\Services\ApiProvider\JonviewAPI;
use Illuminate\Console\Command;

class JonviewUpdateProductTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jonview:update-product-types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update product types from Jonview API';

    private JonviewAPI $jonviewAPIService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(JonviewAPI $jonviewAPIService)
    {
        parent::__construct();
        $this->jonviewAPIService = $jonviewAPIService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle(): int
    {
        $dataProductTypes = $this->jonviewAPIService->getDFProdType();

        $barFormat = "%message%\n %current%/%max% [%bar%] %percent:3s%%";
        $bar = $this->output->createProgressBar(count($dataProductTypes));
        $bar->setFormat($barFormat);
        $bar->setMessage('Getting a product types from jonview API ...');
        $bar->start();

        foreach ($dataProductTypes as $productTypeDTO) {
            ProductType::firstOrCreate($productTypeDTO->toArray());
            $bar->advance();
        }
        $bar->finish();
        $this->output->newLine();

        return 0;
    }
}
