<?php

namespace App\Enums\Jonview;

use Spatie\Enum\Enum;

/**
 * Class Action.
 *
 * @method static self DS();
 * @method static self DF();
 * @method static self DP();
 * @method static self DI();
 * @method static self DR();
 */
class ActionEnum extends Enum
{
}
