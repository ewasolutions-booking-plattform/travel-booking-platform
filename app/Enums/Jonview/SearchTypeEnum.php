<?php

namespace App\Enums\Jonview;

use Closure;
use Spatie\Enum\Enum;

/**
 * Class Type.
 *
 * @method static self city()
 * @method static self prodtype()
 * @method static self sup()
 */
class SearchTypeEnum extends Enum
{
    public static function values(): Closure
    {
        return static fn ($name) => mb_strtoupper($name);
    }
}
