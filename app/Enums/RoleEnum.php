<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * Class RoleEnum.
 *
 * @method static self operator()
 * @method static self administrator()
 */
class RoleEnum extends Enum
{
}
