<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * Class CurrencyEnum.
 *
 * @method static self CHF()
 */
class CurrencyEnum extends Enum
{
}
