<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * Class SortTypesEnum.
 *
 * @method static self nameASC()
 * @method static self nameDESC()
 * @method static self priceLow()
 * @method static self priceHigh()
 */
class SortTypesEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'nameASC'   => 'name_asc',
            'nameDESC'  => 'name_desc',
            'priceLow'  => 'price_low',
            'priceHigh' => 'price_high',
        ];
    }
}
