<?php

namespace App\Enums;

use RuntimeException;

class SeasonEnum
{
    private const SEASONS = [
        'winter' => [12, 1, 2],
        'spring' => [3, 4, 5],
        'summer' => [6, 7, 8],
        'autumn' => [9, 10, 11],
    ];

    /**
     * @param int $month
     *
     * @return string
     */
    public static function getSeason(int $month): string
    {
        foreach (self::SEASONS as $season => $months) {
            if (in_array($month, $months)) {
                return $season;
            }
        }
        throw new RuntimeException('Incorrect month number');
    }
}
