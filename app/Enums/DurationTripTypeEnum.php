<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * Class DurationEnum.
 *
 * @method static self days()
 * @method static self weeks()
 */
class DurationTripTypeEnum extends Enum
{
}
