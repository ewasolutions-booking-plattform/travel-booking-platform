<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * Class AgeCategoryEnum.
 *
 * @method static self adult()
 * @method static self children()
 */
class AgeCategoryEnum extends Enum
{
}
