<?php

namespace App\Events;

use App\Models\TripInfo;

class TripInfoPhotoUploadedEvent
{
    private TripInfo $model;

    public function __construct(TripInfo $model)
    {
        $this->model = $model;
    }

    public function getTripInfo(): TripInfo
    {
        return $this->model;
    }
}
