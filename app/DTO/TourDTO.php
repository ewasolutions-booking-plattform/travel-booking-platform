<?php

namespace App\DTO;

use Carbon\Carbon;
use Spatie\DataTransferObject\DataTransferObject;

class TourDTO extends DataTransferObject
{
    public int $productID;
    public string $title;
    public int $country_id;
    public int $region_id;
    public int $city_id;
    public Carbon $start_trip_date;
    public Carbon $end_trip_date;
    public int $trip_theme_id;
    public int $trip_theme_type_id;
    public string $user_price;
    public bool $is_publish;
}
