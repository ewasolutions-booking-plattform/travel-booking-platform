<?php

namespace App\DTO\Requests;

use App\Models\Country;
use App\Models\TripTheme;
use Spatie\DataTransferObject\DataTransferObject;

class FilterDTO extends DataTransferObject
{
    public ?Country $country;
    public ?TripTheme $theme;
    public ?string $sort;
}
