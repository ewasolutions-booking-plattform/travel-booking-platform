<?php

namespace App\DTO\Jonview;

use Spatie\DataTransferObject\DataTransferObject;

class AvailableDTO extends DataTransferObject
{
    public string $product_code;
    public string $from_date;
    public string $to_date;
    public string $available;
}
