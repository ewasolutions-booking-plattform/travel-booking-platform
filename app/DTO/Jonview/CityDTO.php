<?php

namespace App\DTO\Jonview;

use Spatie\DataTransferObject\DataTransferObject;

class CityDTO extends DataTransferObject
{
    public string $code;
    public string $name;
    public string $description;
    public string $provinceCode;
    public string $provinceName;
    public string $countryCode;
    public string $countryName;
}
