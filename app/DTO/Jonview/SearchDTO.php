<?php

namespace App\DTO\Jonview;

use DateTimeInterface;
use Spatie\DataTransferObject\DataTransferObject;

class SearchDTO extends DataTransferObject
{
    public int $cityID;
    public string $cityCode;
    public string $productTypeCode;
    public DateTimeInterface $fromDate;
    public DateTimeInterface $toDate;
    public array $people;
}
