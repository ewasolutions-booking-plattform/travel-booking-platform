<?php

namespace App\DTO\Jonview;

use Spatie\DataTransferObject\DataTransferObject;

class ProductDTO extends DataTransferObject
{
    public string $code;
    public string $name;
    public int $supplier_id;
    public int $product_type_id;
    public array $characteristics;
}
