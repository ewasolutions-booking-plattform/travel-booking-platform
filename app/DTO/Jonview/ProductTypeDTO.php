<?php

namespace App\DTO\Jonview;

use Spatie\DataTransferObject\DataTransferObject;

class ProductTypeDTO extends DataTransferObject
{
    public string $code;
    public string $description;
}
