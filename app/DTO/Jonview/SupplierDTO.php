<?php

namespace App\DTO\Jonview;

use Spatie\DataTransferObject\DataTransferObject;

class SupplierDTO extends DataTransferObject
{
    public string $code;
    public string $name;
    public string $description;
    public string $rating;
    public string $address1;
    public string $address2;
    public string $address3;
    public string $address4;
    public string $phone;
    public string $fax;
    public float $latitude;
    public float $longitude;
    public int $city_id;
    public array $activities;
    public array $amenities;
    public array $images;
}
