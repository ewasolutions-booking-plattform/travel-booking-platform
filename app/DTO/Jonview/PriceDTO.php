<?php

namespace App\DTO\Jonview;

use Spatie\DataTransferObject\DataTransferObject;

class PriceDTO extends DataTransferObject
{
    public string $product_code;
    public string $currency;
    public string $from_date;
    public string $to_date;
    public float $single;
    public ?float $twin;
    public ?float $triple;
    public ?float $quad;
    public ?float $child1;
    public ?float $child2;
    public ?float $child3;
    public ?string $child_age1;
    public ?string $child_age2;
    public ?string $child_age3;
}
