<?php

namespace App\Mail;

use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TourBooked extends Mailable
{
    use Queueable;
    use SerializesModels;

    protected Booking $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $mailable = $this->from(env('MAIL_FROM_ADDRESS', 'no-replay@example.com'))
            ->view('emails.tour.booked')
            ->with(['booking' => $this->booking]);

        if ($this->booking->tour) {
            $path = storage_path('./app/'.$this->booking->id.'-'.$this->booking->tour->id.'.pdf');
            $mailable->attach(
                $path,
                [
                    'as'   => 'tour-info.pdf',
                    'mime' => 'application/pdf',
                ]
            );
        }

        return $mailable;
    }
}
