<?php

namespace App\Providers;

use App\Events\TourBookedEvent;
use App\Events\TripInfoPhotoUploadedEvent;
use App\Listeners\SendBookedInfoListener;
use App\Listeners\TripInfoPhotoUploadListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class                 => [
            SendEmailVerificationNotification::class,
        ],
        TourBookedEvent::class            => [
            SendBookedInfoListener::class,
        ],
        TripInfoPhotoUploadedEvent::class => [
            TripInfoPhotoUploadListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
