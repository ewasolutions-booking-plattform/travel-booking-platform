<?php

namespace App\Models;

use App\Enums\CurrencyEnum;
use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Booking extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_CLIENT_EMAIL = 'client_email';
    public const FIELD_CLIENT_PHONE = 'client_phone';
    public const FIELD_CLIENT_FIRST_NAME = 'client_first_name';
    public const FIELD_CLIENT_LAST_NAME = 'client_last_name';
    public const FIELD_COUNTRY = 'country_id';
    public const FIELD_REGION = 'region_id';
    public const FIELD_CITY = 'city_id';
    public const FIELD_TRIP_THEME = 'trip_theme_id';
    public const FIELD_TRIP_THEME_TYPE = 'trip_theme_type_id';
    public const FIELD_TRIP_START = 'trip_start_date';
    public const FIELD_TRIP_END = 'trip_end_date';

    public const FIELD_BUDGET = 'budget';
    public const FIELD_TOUR = 'tour_id';

    public const FK_OPERATOR = 'operator_id';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_TRIP_START => 'datetime',
        self::FIELD_TRIP_END => 'datetime',
    ];

    protected $table = 'bookings';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_CLIENT_EMAIL,
        self::FIELD_CLIENT_PHONE,
        self::FIELD_CLIENT_LAST_NAME,
        self::FIELD_CLIENT_FIRST_NAME,
        self::FIELD_COUNTRY,
        self::FIELD_REGION,
        self::FIELD_CITY,
        self::FIELD_TRIP_THEME,
        self::FIELD_TRIP_THEME_TYPE,
        self::FIELD_BUDGET,
        self::FIELD_TOUR,
        self::FIELD_TRIP_START,
        self::FIELD_TRIP_END,
        self::FK_OPERATOR,
    ];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function ageCategories(): BelongsToMany
    {
        return $this->belongsToMany(AgeCategory::class)->withPivot('count');
    }

    public function tour(): BelongsTo
    {
        return $this->belongsTo(Tour::class);
    }

    public function tripTheme(): BelongsTo
    {
        return $this->belongsTo(TripTheme::class);
    }

    public function tripThemeType(): BelongsTo
    {
        return $this->belongsTo(TripThemeType::class);
    }

    public function getFullNameAttribute(): string
    {
        return Str::ucfirst($this->client_first_name) . ' ' . Str::ucfirst($this->client_last_name);
    }

    public function getShowBudgetAttribute(): string
    {
        return CurrencyEnum::CHF()->value . ' '.  $this->budget;
    }

    public function getShowDurationAttribute(): string
    {
        return $this->tour->duration ?? 0 . ' ('. __('booking.days') .')';
    }

    public function operator(): BelongsTo
    {
        return $this->belongsTo(User::class, self::FK_OPERATOR);
    }
}
