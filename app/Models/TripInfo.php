<?php

namespace App\Models;

use App\Events\TripInfoPhotoUploadedEvent;
use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;
use Image;

class TripInfo extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_NUMBER_IN_GROUP = 'number_in_group';
    public const FIELD_NUMBER_OF_GROUPS = 'number_of_groups';
    public const FIELD_INFO_TYPE = 'trip_info_type_id';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_INCLUDED_SERVICES = 'included_services';
    public const FIELD_ARRIVAL_DEPARTURE_INFO = 'arrival_departure_info';
    public const FIELD_SPECIAL_INFO = 'special_info';
    public const FIELD_LATITUDE = 'latitude';
    public const FIELD_LONGITUDE = 'longitude';
    public const FIELD_PHOTOS = 'photos';

    protected $table = 'trip_infos';
    public $timestamps = false;
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_NUMBER_IN_GROUP,
        self::FIELD_NUMBER_OF_GROUPS,
        self::FIELD_INFO_TYPE,
        self::FIELD_DESCRIPTION,
        self::FIELD_INCLUDED_SERVICES,
        self::FIELD_ARRIVAL_DEPARTURE_INFO,
        self::FIELD_SPECIAL_INFO,
        self::FIELD_LATITUDE,
        self::FIELD_LONGITUDE,
        self::FIELD_PHOTOS,
    ];

    protected $casts = [
        'photos' => 'array',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            if (count((array) $obj->photos)) {
                $destinationPath = public_path('storage').'/trip_info/'.$this->id;
                $thumbnailsPath = $destinationPath.'/small';
                $middlePath = $destinationPath.'/middle';

                foreach ($obj->photos as $fileName) {
                    Storage::disk('public')->delete($destinationPath.'/'.$fileName);
                    Storage::disk('public')->delete($middlePath.'/'.$fileName);
                    Storage::disk('public')->delete($thumbnailsPath.'/'.$fileName);
                }
            }
        });
    }

    public function tripInfoType(): BelongsTo
    {
        return $this->belongsTo(TripInfoType::class);
    }

    public function tours(): HasMany
    {
        return $this->hasMany(Tour::class);
    }

    public function getInfoForTourAttribute(): string
    {
        return $this->tripInfoType->title.' [ '.$this->number_in_group.' in group, '.$this->number_of_groups.' of groups ]';
    }

    public function getTripTypeNameAttribute(): string
    {
        return $this->tripInfoType->title;
    }

    public function setPhotosAttribute($value): void
    {
        $attribute_name = "photos";
        $disk = "public";
        $destination_path = "trip_info/".$this->id;

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);

        if (count($this->photos)) {
            event(new TripInfoPhotoUploadedEvent($this));
        }
    }

    public function getSmallPhoto(string $photoPath): string
    {
        return Storage::url('/trip_info/'.$this->id.'/small/'.$this->getPhotoName($photoPath));
    }

    public function getMiddlePhoto(string $photoPath): string
    {
        return Storage::url('/trip_info/'.$this->id.'/middle/'.$this->getPhotoName($photoPath));
    }

    public function getPhoto(string $photoPath): string
    {
        return Storage::url($photoPath);
    }

    private function getPhotoName(string $path): string
    {
        if (empty($path)) {
            return '';
        }
        $pathData = explode('/', $path);

        return array_pop($pathData);
    }
}
