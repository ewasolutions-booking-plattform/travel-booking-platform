<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    use HasFactory;

    protected $table = 'request_logs';
    protected $guarded = ['id'];

    public const FIELD_REQUEST = 'request_name';
    public const FIELD_ACTUAL_DATE = 'actual_date';

    public $timestamps = false;

    protected $dates = [
        self::FIELD_ACTUAL_DATE,
    ];
    protected $fillable = [
        self::FIELD_REQUEST,
        self::FIELD_ACTUAL_DATE,
    ];
}
