<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TripInfoType extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_TITLE = 'title';

    protected $table = 'trip_info_types';
    public $timestamps = false;
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_TITLE,
    ];

    public function tripInfos(): HasMany
    {
        return $this->hasMany(TripInfo::class);
    }
}
