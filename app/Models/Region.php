<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Region extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_CODE = 'code';
    public const FIELD_NAME = 'name';
    public const FIELD_COUNTRY = 'country_id';

    protected $table = 'regions';
    public $timestamps = false;
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_CODE,
        self::FIELD_NAME,
        self::FIELD_COUNTRY,
    ];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }

    public function getNameAttribute(): string
    {
        return Str::title($this->attributes['name']);
    }
}
