<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ProductTypeGroup extends Model
{
    use CrudTrait;

    public const FIELD_NAME = 'name';
    protected $table = 'product_type_groups';
    protected $guarded = ['id'];
    public $timestamps = false;

    protected $fillable = [
        self::FIELD_NAME,
    ];
}
