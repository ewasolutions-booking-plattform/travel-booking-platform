<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Property extends Model
{
    use HasFactory;

    protected $table = 'properties';

    public const FIELD_PROPERTIABLE_ID = 'propertiable_id';
    public const FIELD_PROPERTIABLE_TYPE = 'propertiable_type';
    public const FIELD_NAME = 'name';

    protected $fillable = [
        self::FIELD_PROPERTIABLE_ID,
        self::FIELD_PROPERTIABLE_TYPE,
        self::FIELD_NAME,
    ];

    public function propertiable(): MorphTo
    {
        return $this->morphTo();
    }
}
