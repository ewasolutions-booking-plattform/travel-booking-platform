<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RoomType extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_TITLE = 'title';

    public $timestamps = false;
    protected $table = 'rooms_types';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_TITLE,
    ];

    public function roomInfos(): HasMany
    {
        return $this->hasMany(RoomInfo::class);
    }
}
