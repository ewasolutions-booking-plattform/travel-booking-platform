<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $table = 'currencies';
    protected $guarded = ['id'];
    public $timestamps = false;

    private const FIELD_NAME = 'name';
    public const FIELD_CODE = 'code';

    protected $fillable = [
        self::FIELD_CODE,
        self::FIELD_NAME,
    ];
}
