<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Supplier extends Model
{
    use CrudTrait;

    public const FIELD_CODE = 'code';
    public const FIELD_NAME = 'name';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_RATING = 'rating';
    public const FIELD_ADDRESS1 = 'address1';
    public const FIELD_ADDRESS2 = 'address2';
    public const FIELD_ADDRESS3 = 'address3';
    public const FIELD_ADDRESS4 = 'address4';
    public const FIELD_PHONE = 'phone';
    public const FIELD_FAX = 'fax';
    public const FIELD_CITY = 'city_id';
    public const FIELD_LATITUDE = 'latitude';
    public const FIELD_LONGITUDE = 'longitude';

    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_CODE,
        self::FIELD_NAME,
        self::FIELD_DESCRIPTION,
        self::FIELD_RATING,
        self::FIELD_ADDRESS1,
        self::FIELD_ADDRESS2,
        self::FIELD_ADDRESS3,
        self::FIELD_ADDRESS4,
        self::FIELD_PHONE,
        self::FIELD_FAX,
        self::FIELD_CITY,
        self::FIELD_LATITUDE,
        self::FIELD_LONGITUDE,
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function activities(): BelongsToMany
    {
        return $this->belongsToMany(Activity::class);
    }

    public function amenities(): BelongsToMany
    {
        return $this->belongsToMany(Amenity::class);
    }

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function getShowActivitiesAttribute(): string
    {
        return implode(',', $this->activities()->pluck(Activity::FIELD_NAME)->toArray());
    }

    public function getShowAmenitiesAttribute(): string
    {
        return implode(', ', $this->amenities()->pluck(Amenity::FIELD_NAME)->toArray());
    }
}
