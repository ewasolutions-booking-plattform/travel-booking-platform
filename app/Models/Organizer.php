<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Organizer extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_NAME = 'name';

    public $timestamps = false;
    protected $table = 'organizers';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_NAME,
    ];

    public function roomInfos(): HasMany
    {
        return $this->hasMany(RoomInfo::class);
    }
}
