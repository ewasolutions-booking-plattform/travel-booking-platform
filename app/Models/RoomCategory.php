<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class RoomCategory extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_NAME = 'name';

    public $timestamps = false;
    protected $table = 'rooms_categories';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_NAME,
    ];

    public function tour(): HasOne
    {
        return $this->hasOne(Tour::class);
    }
}
