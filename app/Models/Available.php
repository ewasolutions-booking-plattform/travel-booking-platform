<?php

namespace App\Models;

use App\Traits\CanTableName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Available extends Model
{
    use HasFactory;
    use CanTableName;

    protected $table = 'availables';
    protected $guarded = ['id'];

    public const FIELD_PRODUCT = 'product_id';
    public const FIELD_FROM_DATE = 'from_date';
    public const FIELD_TO_DATE = 'to_date';
    public const FIELD_AVAILABLE = 'available';

    protected $fillable = [
        self::FIELD_PRODUCT,
        self::FIELD_FROM_DATE,
        self::FIELD_TO_DATE,
        self::FIELD_AVAILABLE,
    ];

    protected $casts = [
        self::FIELD_FROM_DATE => 'date',
        self::FIELD_TO_DATE   => 'date',
    ];
}
