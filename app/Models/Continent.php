<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Continent extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_NAME = 'name';

    protected $table = 'continents';
    public $timestamps = false;
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_NAME,
    ];

    public function countries(): HasMany
    {
        return $this->hasMany(Country::class);
    }
}
