<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Hotel extends Model
{
    use CanTableName;
    use CrudTrait;

    public const FIELD_SUPPLIER_CODE = 'supplier_code';
    public const FIELD_NAME = 'name';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_RATING = 'rating';
    public const FIELD_ADDRESS1 = 'address1';
    public const FIELD_ADDRESS2 = 'address2';
    public const FIELD_ADDRESS3 = 'address3';
    public const FIELD_PHONE = 'phone';
    public const FIELD_FAX = 'fax';
    public const FIELD_LATITUDE = 'latitude';
    public const FIELD_LONGITUDE = 'longitude';
    public const FIELD_CITY = 'city_id';

    protected $table = 'hotels';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_SUPPLIER_CODE,
        self::FIELD_NAME,
        self::FIELD_DESCRIPTION,
        self::FIELD_RATING,
        self::FIELD_ADDRESS1,
        self::FIELD_ADDRESS2,
        self::FIELD_ADDRESS3,
        self::FIELD_PHONE,
        self::FIELD_FAX,
        self::FIELD_LATITUDE,
        self::FIELD_LONGITUDE,
        self::FIELD_CITY,
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function properties(): MorphMany
    {
        return $this->morphMany(Property::class, 'propertiable');
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
