<?php

namespace App\Models;

use App\Traits\CanTableName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    use CanTableName;
    use HasFactory;

    public const FIELD_CODE = 'code';
    public const FIELD_NAME = 'name';
    public const FIELD_SUPPLIER = 'supplier_id';
    public const FIELD_TYPE = 'product_type_id';

    public $timestamps = false;

    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_CODE,
        self::FIELD_NAME,
        self::FIELD_SUPPLIER,
        self::FIELD_TYPE,
    ];

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function type(): HasOne
    {
        return $this->hasOne(ProductType::class);
    }

    public function prices(): HasMany
    {
        return $this->hasMany(Price::class);
    }

    public function availables(): HasMany
    {
        return $this->hasMany(Available::class);
    }

    public function characteristics(): BelongsToMany
    {
        return $this->belongsToMany(Characteristic::class)->withPivot('value');
    }
}
