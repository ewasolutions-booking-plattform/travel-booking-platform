<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class RoomInfo extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_ORGANIZER = 'organizer_id';
    public const FIELD_TYPE = 'room_type_id';
    public const FIELD_CATEGORY = 'room_category_id';
    public const FIELD_BASE_PRISE = 'base_price';
    public const FIELD_USER_PRISE = 'user_price';
    public const FIELD_CURRENCY = 'currency';
    public const FIELD_CONTENT_URL = 'content_url';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_PRICE_INFO = 'price_info';

    public $timestamps = false;
    protected $table = 'rooms_infos';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_ORGANIZER,
        self::FIELD_TYPE,
        self::FIELD_CATEGORY,
        self::FIELD_BASE_PRISE,
        self::FIELD_USER_PRISE,
        self::FIELD_CURRENCY,
        self::FIELD_CONTENT_URL,
        self::FIELD_DESCRIPTION,
        self::FIELD_PRICE_INFO,
    ];

    protected $casts = [
        self::FIELD_BASE_PRISE => 'float',
        self::FIELD_USER_PRISE => 'float',
    ];

    public function organizer(): BelongsTo
    {
        return $this->belongsTo(Organizer::class);
    }

    public function roomCategory(): BelongsTo
    {
        return $this->belongsTo(RoomCategory::class);
    }

    public function roomType(): BelongsTo
    {
        return $this->belongsTo(RoomType::class);
    }

    public function tour(): HasOne
    {
        return $this->hasOne(Tour::class);
    }

    public function getNameForTourAttribute(): string
    {
        return $this->roomType->title.':'.$this->roomCategory->name.' ('.$this->base_price.' - '.$this->user_price.')';
    }

    public function getShowBasePriceAttribute(): string
    {
        return $this->currency.' '.$this->attributes['base_price'];
    }

    public function getShowUserPriceAttribute(): string
    {
        return $this->currency.' '.$this->attributes['user_price'];
    }

    public function getCategoryNameAttribute(): string
    {
        return $this->roomCategory->name;
    }

    public function getTypeNameAttribute(): string
    {
        return $this->roomType->title;
    }

    public function getUrlTitleAttribute(): string
    {
        return $this->organizer->name.' '.$this->roomCategory->name.' '.$this->roomType->title;
    }
}
