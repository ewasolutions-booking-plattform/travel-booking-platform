<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;
use Znck\Eloquent\Relations\BelongsToThrough;

class City extends Model
{
    use CrudTrait;
    use \Znck\Eloquent\Traits\BelongsToThrough;

    public const FIELD_CODE = 'code';
    public const FIELD_NAME = 'name';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_REGION = 'region_id';

    protected $table = 'cities';
    public $timestamps = false;
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_CODE,
        self::FIELD_NAME,
        self::FIELD_DESCRIPTION,
        self::FIELD_REGION,
    ];

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }

    public function getNameAttribute(): string
    {
        return Str::title($this->attributes['name']);
    }

    public function country(): BelongsToThrough
    {
        return $this->belongsToThrough(Country::class, [Region::class]);
    }
}
