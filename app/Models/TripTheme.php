<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TripTheme extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_TITLE = 'title';

    public $timestamps = false;
    protected $table = 'trip_themes';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_TITLE,
    ];

    public function tripThemeTypes(): HasMany
    {
        return $this->hasMany(TripThemeType::class);
    }

    public function tour(): HasOne
    {
        return $this->hasOne(Tour::class);
    }
}
