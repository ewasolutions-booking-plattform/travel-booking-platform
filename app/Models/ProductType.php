<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductType extends Model
{
    use CrudTrait;

    public const FIELD_CODE = 'code';
    public const FIELD_DESCRIPTION = 'description';

    public $timestamps = false;
    protected $table = 'product_types';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_CODE,
        self::FIELD_DESCRIPTION,
    ];

    public function typeGroups(): BelongsToMany
    {
        return $this->belongsToMany(ProductTypeGroup::class, 'group_product');
    }
}
