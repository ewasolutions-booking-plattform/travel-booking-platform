<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Country extends Model
{
    use CanTableName;
    use CrudTrait;

    public const FIELD_NAME = 'name';
    public const FIELD_CODE = 'code';
    public const FIELD_CONTINENT = 'continent_id';

    protected $table = 'countries';
    public $timestamps = false;
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_CODE,
        self::FIELD_CONTINENT,
    ];

    public function continent(): BelongsTo
    {
        return $this->belongsTo(Continent::class);
    }

    public function regions(): HasMany
    {
        return $this->hasMany(Region::class);
    }

    public function getNameAttribute(): string
    {
        return Str::title($this->attributes['name']);
    }
}
