<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Characteristic extends Model
{
    use HasFactory;

    public const FIELD_NAME = 'name';

    public $timestamps = false;

    protected $fillable = [
        self::FIELD_NAME,
    ];
}
