<?php

namespace App\Models;

use App\Traits\CanTableName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Price extends Model
{
    use HasFactory;
    use CanTableName;

    protected $table = 'prices';
    protected $guarded = ['id'];

    public const FIELD_PRODUCT = 'product_id';
    public const FIELD_FROM_DATE = 'from_date';
    public const FIELD_TO_DATE = 'to_date';
    public const FIELD_CURRENCY = 'currency_id';
    public const FIELD_SINGLE = 'single';
    public const FIELD_TWIN = 'twin';
    public const FIELD_TRIPLE = 'triple';
    public const FIELD_QUAD = 'quad';
    public const FIELD_CHILD1 = 'child1';
    public const FIELD_CHILD2 = 'child2';
    public const FIELD_CHILD3 = 'child3';
    public const FIELD_CHILD_AGE1 = 'child_age1';
    public const FIELD_CHILD_AGE2 = 'child_age2';
    public const FIELD_CHILD_AGE3 = 'child_age3';

    protected $fillable = [
        self::FIELD_PRODUCT,
        self::FIELD_FROM_DATE,
        self::FIELD_TO_DATE,
        self::FIELD_CURRENCY,
        self::FIELD_SINGLE,
        self::FIELD_TWIN,
        self::FIELD_TRIPLE,
        self::FIELD_QUAD,
        self::FIELD_CHILD1,
        self::FIELD_CHILD2,
        self::FIELD_CHILD3,
        self::FIELD_CHILD_AGE1,
        self::FIELD_CHILD_AGE2,
        self::FIELD_CHILD_AGE3,
    ];

    protected $casts = [
        self::FIELD_FROM_DATE => 'date',
        self::FIELD_TO_DATE => 'date',
    ];

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }
}
