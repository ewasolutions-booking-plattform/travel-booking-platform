<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Profile extends Model
{
    use CanTableName;
    use CrudTrait;

    public const FIELD_USER = 'user_id';
    public const FIELD_TITLE = 'title';
    public const FIELD_DOMAIN = 'domain';

    protected $table = 'profiles';
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_USER,
        self::FIELD_TITLE,
        self::FIELD_DOMAIN,
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
