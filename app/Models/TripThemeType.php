<?php

namespace App\Models;

use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TripThemeType extends Model
{
    use CrudTrait;
    use CanTableName;

    public const FIELD_TITLE = 'title';
    public const FIELD_TRIP_THEME = 'trip_theme_id';

    protected $table = 'trip_theme_types';
    public $timestamps = false;
    protected $guarded = ['id'];

    protected $fillable = [
        self::FIELD_TITLE,
        self::FIELD_TRIP_THEME,
    ];

    public function tripTheme(): BelongsTo
    {
        return $this->belongsTo(TripTheme::class);
    }

    public function tour(): HasOne
    {
        return $this->hasOne(Tour::class);
    }
}
