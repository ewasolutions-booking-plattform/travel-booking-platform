<?php

namespace App\Models;

use App\Enums\SeasonEnum;
use App\Traits\CanTableName;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tour extends Model
{
    use CrudTrait;
    use CanTableName;
    use Sluggable;

    private const DAYS_IN_WEEK = 7;

    public const FIELD_TITLE = 'title';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_SLUG = 'slug';
    public const FIELD_COUNTRY = 'country_id';
    public const FIELD_REGION = 'region_id';
    public const FIELD_CITY = 'city_id';
    public const FIELD_TRIP_THEME = 'trip_theme_id';
    public const FIELD_TRIP_THEME_TYPE = 'trip_theme_type_id';
    public const FIELD_TRIP_START = 'start_trip_date';
    public const FIELD_TRIP_END = 'end_trip_date';
    public const FIELD_USER_PRICE = 'user_price';
    public const FIELD_IS_PUBLISH = 'is_publish';


    protected $casts = [
        self::FIELD_TRIP_START => 'datetime',
        self::FIELD_TRIP_END   => 'datetime',
    ];

    protected $fillable = [
        self::FIELD_TITLE,
        self::FIELD_DESCRIPTION,
        self::FIELD_SLUG,
        self::FIELD_COUNTRY,
        self::FIELD_REGION,
        self::FIELD_CITY,
        self::FIELD_TRIP_THEME,
        self::FIELD_TRIP_THEME_TYPE,
        self::FIELD_TRIP_START,
        self::FIELD_TRIP_END,
        self::FIELD_USER_PRICE,
        self::FIELD_IS_PUBLISH,
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            if ($model->isDirty(self::FIELD_TRIP_START) || $model->isDirty(self::FIELD_TRIP_END)) {
                $model->week_of_year = $model->start_trip_date->weekOfYear;
                $model->month_of_year = $model->start_trip_date->month;
                $model->season_of_year = SeasonEnum::getSeason($model->start_trip_date->month);
                $model->duration = $model->end_trip_date->diffInDays($model->start_trip_date);
                $model->duration_in_week = ceil($model->duration / self::DAYS_IN_WEEK);
            }
        });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function tripTheme(): BelongsTo
    {
        return $this->belongsTo(TripTheme::class);
    }

    public function tripThemeType(): BelongsTo
    {
        return $this->belongsTo(TripThemeType::class);
    }

    public function ageCategories(): BelongsToMany
    {
        return $this->belongsToMany(AgeCategory::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }

    public function getPriceAttribute(): string
    {
        return 'CHF ' . $this->attributes['user_price'];
    }
}
