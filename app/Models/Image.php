<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Image extends Model
{
    use HasFactory;

    protected $table = 'images';

    public const FIELD_IMAGEABLE_ID = 'imageable_id';
    public const FIELD_IMAGEABLE_TYPE = 'imageable_type';
    public const FIELD_PATH = 'path';

    protected $fillable = [
        self::FIELD_IMAGEABLE_ID,
        self::FIELD_IMAGEABLE_TYPE,
        self::FIELD_PATH,
    ];

    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }
}
