<?php

namespace App\Rules;

use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Validation\Rule;

class MaxTripDateRange implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @throws Exception
     */
    public function passes($attribute, $value): bool
    {
        $dates = explode('-', $value);
        array_walk($dates, [$this, 'trimValue']);
        $startDate = Carbon::parse($dates[0]);
        $endDate = Carbon::parse($dates[1]);

        return $startDate->diffInDays($endDate) <= 40;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Max trip duration is 40 days.';
    }

    private function trimValue(&$value)
    {
        $value = trim($value);
    }
}
