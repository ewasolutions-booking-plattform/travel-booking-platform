<?php

namespace Database\Seeders;

use App\Models\User;
use Backpack\PermissionManager\app\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'       => 'administrator',
                'guard_name' => 'web',
            ],
            [
                'name'       => 'operator',
                'guard_name' => 'web',
            ],
        ];

        Role::insert($roles);

        $roleOperator = Role::findByName('operator');
        $roleAdministrator = Role::findByName('administrator');

        $administrator = User::find(1);
        $administrator->assignRole($roleAdministrator);

        $operator = User::find(2);
        $operator->assignRole($roleOperator);

        $operator2 = User::find(3);
        $operator2->assignRole($roleOperator);
    }
}
