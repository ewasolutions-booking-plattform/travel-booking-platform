<?php

namespace Database\Seeders;

use App\Enums\CurrencyEnum;
use App\Models\AgeCategory;
use App\Models\Continent;
use App\Models\Country;
use App\Models\Organizer;
use App\Models\Region;
use App\Models\RoomCategory;
use App\Models\RoomInfo;
use App\Models\RoomType;
use App\Models\Tour;
use App\Models\TripInfo;
use App\Models\TripInfoType;
use App\Models\TripTheme;
use App\Models\TripThemeType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use SpreadsheetReader;

class ToursFromExcel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $continent = Continent::firstOrCreate(['name' => 'Nordamerika']);
        $country = Country::firstOrCreate(['name' => 'Canada']);
        $region = Region::firstOrCreate(['name' => 'British Columbia']);
        $organizer = Organizer::firstOrCreate(['name' => 'CMH']);
        $tripTheme = TripTheme::firstOrCreate(['title' => 'Heliski']);
        $tripInfoType = TripInfoType::firstOrCreate(['title' => 'Bell 407']);
        $roomType = RoomType::firstOrCreate(['title' => 'Adamants Lodge']);

        foreach ($this->getTours() as $tour) {
            $tripThemeType = TripThemeType::firstOrCreate([
                TripThemeType::FIELD_TITLE      => $tour['trip_theme_type'],
                TripThemeType::FIELD_TRIP_THEME => $tripTheme->id,
            ]);

            $tripInfo = TripInfo::create([
                TripInfo::FIELD_INFO_TYPE        => $tripInfoType->id,
                TripInfo::FIELD_NUMBER_IN_GROUP  => $tour['number_in_group'],
                TripInfo::FIELD_NUMBER_OF_GROUPS => 4,
            ]);

            $roomCategory = RoomCategory::firstOrCreate(['name' => $tour['room_category']]);
            $roomInfo = RoomInfo::create([
                RoomInfo::FIELD_ORGANIZER   => $organizer->id,
                RoomInfo::FIELD_CATEGORY    => $roomCategory->id,
                RoomInfo::FIELD_TYPE        => $roomType->id,
                RoomInfo::FIELD_BASE_PRISE  => str_replace(',', '.', $tour['base_price']),
                RoomInfo::FIELD_USER_PRISE  => str_replace(',', '.', $tour['user_price']),
                RoomInfo::FIELD_CURRENCY    => CurrencyEnum::CHF()->value,
                RoomInfo::FIELD_CONTENT_URL => 'https://example.com',
            ]);

            $tourObj = Tour::create([
                Tour::FIELD_TITLE           => $tour['title'],
                Tour::FIELD_TRIP_START      => Carbon::parse($tour['start_date']),
                Tour::FIELD_TRIP_END        => Carbon::parse($tour['end_date']),
                Tour::FIELD_CONTINENT       => $continent->id,
                Tour::FIELD_COUNTRY         => $country->id,
                Tour::FIELD_REGION          => $region->id,
                Tour::FIELD_ORGANIZER       => $organizer->id, // can delete from tour, maybe
                Tour::FIELD_ROOM_INFO       => $roomInfo->id,
                Tour::FIELD_TRIP_THEME      => $tripTheme->id,
                Tour::FIELD_TRIP_THEME_TYPE => $tripThemeType->id,
                Tour::FIELD_TRIP_INFO       => $tripInfo->id,
            ]);

            foreach (['Erwachsene', 'Anzahl Kinder'] as $category) {
                $ageCategory = AgeCategory::firstOrCreate(['title' => $category]);
                $tourObj->ageCategories()->attach($ageCategory);
            }
        }
    }

    private function getTours(): array
    {
        $path = storage_path('./app/tours.csv');
        $file = fopen($path, "r");
        $tours = [];

        while (($data = fgetcsv($file, 1000, ",")) !== false) {
            $tours[] = [
                'title'           => $data[0],
                'start_date'      => $data[2],
                'end_date'        => $data[3],
                'trip_theme_type' => $data[1],
                'number_in_group' => (int) $data[4],
                'room_category'   => $data[6],
                'base_price'      => (float) $data[5],
                'user_price'      => $data[5] * 1.1,
            ];
        }
        fclose($file);

        return $tours;
    }
}
