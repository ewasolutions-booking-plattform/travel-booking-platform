<?php

namespace Database\Seeders;

use App\Models\TripTheme;
use App\Models\TripThemeType;
use Illuminate\Database\Seeder;

class TripThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Golf'    => [
                'Indiduelle Golfreise',
                'Gruppenreise',
                'Single-Gruppenreise',
                'Pro Gruppenreise',
                'Golf-Kreuzfahrt',
            ],
            'Heliski' => [
                'Individuelle Heliskireise',
                'Gruppenreise',
                'Privatgruppe',
                'Tourengehen',
                'Powder Intro',
            ],
            'Ski'     => [
                'Individuelle Skireise',
                'Gruppenreise',
                'Touren gehen',
                'Splitt board camps',
            ],
            'More'    => [
                'Kreuzfahrten',
                'Spezielle Hotels',
            ],
        ];

        foreach ($data as $themeTitle => $types) {
            $theme = TripTheme::firstOrCreate(['title' => $themeTitle]);
            foreach ($types as $type) {
                TripThemeType::firstOrCreate([
                    'title'         => $type,
                    'trip_theme_id' => $theme->id,
                ]);
            }
        }
    }
}
