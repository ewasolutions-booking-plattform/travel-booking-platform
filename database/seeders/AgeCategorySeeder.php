<?php

namespace Database\Seeders;

use App\Models\AgeCategory;
use Illuminate\Database\Seeder;

class AgeCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ages = [
            'adult',
            'children',
        ];

        foreach ($ages as $title) {
            AgeCategory::firstOrCreate(['title' => $title]);
        }
    }
}
