<?php

namespace Database\Seeders;

use App\Models\ProductTypeGroup;
use Illuminate\Database\Seeder;

class ProductTypeGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'name' => 'Accommodation',
            ],
            [
                'name' => 'Packages',
            ],
            [
                'name' => 'Additional Services',
            ],
            [
                'name' => 'Winter',
            ],
        ];

        foreach ($groups as $group) {
            ProductTypeGroup::firstOrCreate($group);
        }
    }
}
