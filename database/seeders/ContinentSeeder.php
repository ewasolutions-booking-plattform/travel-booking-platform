<?php

namespace Database\Seeders;

use App\Models\Continent;
use Illuminate\Database\Seeder;

class ContinentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $continents = [
            'Nordamerika',
        ];
        foreach ($continents as $name) {
            Continent::firstOrCreate(['name' => $name]);
        }
    }
}
