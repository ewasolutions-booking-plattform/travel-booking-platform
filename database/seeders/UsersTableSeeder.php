<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id'       => 1,
                'name'     => 'admin',
                'email'    => 'admin@example.com',
                'password' => bcrypt('secret'),
            ],
            [
                'id'       => 2,
                'name'     => 'operator one',
                'email'    => 'operator@example.com',
                'password' => bcrypt('secret'),
            ],
            [
                'id'       => 3,
                'name'     => 'operator two',
                'email'    => 'operator2@example.com',
                'password' => bcrypt('secret'),
            ],
            [
                'id'       => 4,
                'name'     => 'guest',
                'email'    => 'guest@example.com',
                'password' => bcrypt('secret'),
            ],
        ];

        User::insert($users);
    }
}
