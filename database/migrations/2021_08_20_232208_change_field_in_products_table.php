<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldInProductsTable extends Migration
{
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropTimestamps();
            $table->unsignedBigInteger('hotel_id')->nullable()->change();
            $table->unsignedBigInteger('product_type_id')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('hotel_id')->change();
            $table->unsignedBigInteger('product_type_id')->change();
        });
    }
}
