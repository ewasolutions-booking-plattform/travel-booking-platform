<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsFromToursTable extends Migration
{
    public function up(): void
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->dropForeign(['continent_id']);
            $table->dropForeign(['country_id']);
            $table->dropForeign(['region_id']);
            $table->dropForeign(['organizer_id']);
            $table->dropForeign(['room_info_id']);
            $table->dropForeign(['trip_info_id']);

            $table->dropColumn(
                [
                    'continent_id',
                    'country_id',
                    'region_id',
                    'organizer_id',
                    'room_info_id',
                    'trip_info_id'
                ]
            );
        });
    }

    public function down(): void
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('region_id');
            $table->unsignedInteger('continent_id');
            $table->unsignedInteger('organizer_id');
            $table->unsignedInteger('room_info_id');
            $table->unsignedInteger('trip_info_id');

            $table->foreign('continent_id')
                ->references('id')
                ->on('continents')
                ->onDelete('cascade');
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');
            $table->foreign('organizer_id')
                ->references('id')
                ->on('organizers')
                ->onDelete('cascade');
            $table->foreign('room_info_id')
                ->references('id')
                ->on('rooms_infos')
                ->onDelete('cascade');
            $table->foreign('trip_info_id')
                ->references('id')
                ->on('trip_infos')
                ->onDelete('cascade');
        });
    }
}
