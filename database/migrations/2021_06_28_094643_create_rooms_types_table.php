<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organizers_rooms', function(Blueprint $table){
            $table->dropForeign(['category_id']);
        });

        Schema::rename('organizers_rooms', 'rooms_infos');
        Schema::rename('categories_rooms', 'rooms_categories');

        Schema::create('rooms_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');

            $table->unique('title');
        });

        Schema::table('rooms_infos', function(Blueprint $table){
            $table->integer('type_id')->unsigned();

            $table->foreign('type_id')
                ->references('id')
                ->on('rooms_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('rooms_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms_types');
    }
}
