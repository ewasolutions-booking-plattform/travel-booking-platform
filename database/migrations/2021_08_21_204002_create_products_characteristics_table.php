<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsCharacteristicsTable extends Migration
{
    public function up(): void
    {
        Schema::create('characteristic_product', function (Blueprint $table) {
            $table->foreignId('product_id')->constrained()->onDelete('cascade');
            $table->foreignId('characteristic_id')->constrained()->onDelete('cascade');
            $table->string('value');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('characteristic_product');
    }
}
