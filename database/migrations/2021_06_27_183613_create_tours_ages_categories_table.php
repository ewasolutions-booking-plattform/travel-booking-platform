<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursAgesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('age_category_tour', function (Blueprint $table) {
            $table->integer('tour_id')->unsigned();
            $table->integer('age_category_id')->unsigned();

            $table->foreign('tour_id')
                ->references('id')
                ->on('tours')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('age_category_id')
                ->references('id')
                ->on('age_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unique(['tour_id', 'age_category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('age_category_tour');
    }
}
