<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRoomInfoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rooms_infos', function(Blueprint $table){
            $table->renameColumn('type_id', 'room_type_id');
            $table->renameColumn('category_id', 'room_category_id');

            $table->foreign('room_type_id')
                ->references('id')
                ->on('rooms_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('room_category_id')
                ->references('id')
                ->on('rooms_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
