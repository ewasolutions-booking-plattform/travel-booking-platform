<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTypeGroupsTable extends Migration
{
    public function up(): void
    {
        Schema::create('product_type_groups', function (Blueprint $table) {
            $table->id();
            $table->string('name', 32);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('product_type_groups');
    }
}
