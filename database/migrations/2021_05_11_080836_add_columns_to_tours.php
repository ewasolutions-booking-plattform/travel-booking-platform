<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->char('country', 25)->default('');
            $table->integer('price_min')->default('0');
            $table->integer('price_max')->default('0');
            $table->integer('number_of_travelers')->default('0');
            $table->char('trip_type', 25)->default('');
            $table->integer('duration_trip')->default('0');
            $table->char('duration_trip_type', 10)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->dropColumn('price_min');
            $table->dropColumn('price_max');
            $table->dropColumn('number_of_travelers');
            $table->dropColumn('country');
            $table->dropColumn('trip_type');
            $table->dropColumn('duration_trip');
            $table->dropColumn('duration_trip_type');
        });
    }
}
