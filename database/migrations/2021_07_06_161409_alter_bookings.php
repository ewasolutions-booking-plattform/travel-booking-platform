<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function(Blueprint $table){
            $table->integer('tour_id')->unsigned()->nullable()->change();
            $table->integer('trip_theme_id')->unsigned()->after('duration_count');
            $table->integer('trip_theme_type_id')->unsigned()->after('trip_theme_id');

            $table->foreign('trip_theme_id')
                ->references('id')
                ->on('trip_themes');

            $table->foreign('trip_theme_type_id')
                ->references('id')
                ->on('trip_theme_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
