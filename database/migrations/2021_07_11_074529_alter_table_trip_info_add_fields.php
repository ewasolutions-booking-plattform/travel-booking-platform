<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableTripInfoAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trip_infos', function (Blueprint $table) {
            $table->text('description');
            $table->text('included_services');
            $table->text('arrival_departure_info');
            $table->text('special_info');
            $table->double('latitude');
            $table->double('longitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trip_infos', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('included_services');
            $table->dropColumn('arrival_departure_info');
            $table->dropColumn('special_info');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
