<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFieldFromAvailablesTable extends Migration
{
    public function up(): void
    {
        Schema::table('availables', function (Blueprint $table) {
            $table->dropColumn('product_code');
        });
    }

    public function down(): void
    {
        Schema::table('availables', function (Blueprint $table) {
            $table->string('product_code', 32);
        });
    }
}
