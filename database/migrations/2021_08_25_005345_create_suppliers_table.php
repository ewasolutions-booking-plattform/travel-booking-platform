<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    public function up(): void
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropForeign(['region_id']);
            $table->dropForeign(['country_id']);
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->unsignedBigInteger('region_id')->change();
            $table->unsignedBigInteger('country_id')->change();
        });
        Schema::table('regions', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropForeign(['region_id']);
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->unsignedBigInteger('region_id')->change();
        });
        Schema::table('regions', function (Blueprint $table) {
            $table->id()->change();
            $table->unsignedBigInteger('country_id')->change();
        });
        Schema::table('countries', function (Blueprint $table) {
            $table->id()->change();
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
        });
        Schema::table('regions', function (Blueprint $table) {
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');
        });

        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('code', 32);
            $table->string('name', 255);
            $table->mediumText('description')->nullable();
            $table->string('rating', 10)->nullable();
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('address4')->nullable();
            $table->string('phone');
            $table->string('fax')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->foreignId('city_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('suppliers');
    }
}
