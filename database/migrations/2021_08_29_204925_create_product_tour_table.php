<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTourTable extends Migration
{
    public function up(): void
    {
        Schema::table('age_category_tour', function (Blueprint $table) {
            $table->dropForeign(['tour_id']);
        });
        Schema::table('age_category_tour', function (Blueprint $table) {
            $table->unsignedBigInteger('tour_id')->change();
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropForeign(['tour_id']);
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->unsignedBigInteger('tour_id')->change();
        });
        Schema::table('tours', function (Blueprint $table) {
            $table->id()->change();
        });

        Schema::create('product_tour', function (Blueprint $table) {
            $table->foreignId('product_id')->constrained()->onDelete('cascade');
            $table->foreignId('tour_id')->constrained()->onDelete('cascade');
        });

        Schema::table('age_category_tour', function (Blueprint $table) {
            $table->foreign('tour_id')
                ->references('id')
                ->on('tours')
                ->onDelete('cascade');
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->foreign('tour_id')
                ->references('id')
                ->on('tours')
                ->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('product_tour');
    }
}
