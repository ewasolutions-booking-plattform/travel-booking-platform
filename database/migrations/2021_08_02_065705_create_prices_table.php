<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained();
            $table->string('product_code', 32);
            $table->foreignId('product_type_id')->constrained();
            $table->foreignId('currency_id')->constrained();
            $table->date('from_date');
            $table->date('to_date');
            $table->float('single');
            $table->float('twin')->nullable();
            $table->float('triple')->nullable();
            $table->float('quad')->nullable();
            $table->float('child1')->nullable();
            $table->float('child2')->nullable();
            $table->float('child3')->nullable();
            $table->float('child_age1')->nullable();
            $table->float('child_age2')->nullable();
            $table->float('child_age3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
