<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupProductTable extends Migration
{
    public function up(): void
    {
        Schema::create('group_product', function (Blueprint $table) {
            $table->foreignId('product_type_id')->constrained()->onDelete('cascade');
            $table->foreignId('product_type_group_id')->constrained()->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('group_product');
    }
}
