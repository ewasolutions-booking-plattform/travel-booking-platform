<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tours', function (Blueprint $table){
            $table->dropColumn('country');
            $table->dropColumn('trip_type');
            $table->dropColumn('duration_trip');
            $table->dropColumn('duration_trip_type');
            $table->dropColumn('price_min');
            $table->dropColumn('price_max');

            $table->increments('id')->change();
            // There fields can't be nullable
            $table->date('start_trip_date')->change();
            $table->date('end_trip_date')->change();

            $table->integer('continent_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('region_id')->unsigned();
            $table->integer('organizer_id')->unsigned();
            $table->integer('category_room_id')->unsigned();
            $table->integer('organizer_room_id')->unsigned();
            $table->integer('trip_theme_id')->unsigned();
            $table->integer('trip_type_id')->unsigned();

            $table->integer('duration')->unsigned();
            $table->integer('week_of_year')->unsigned();
            $table->integer('month_of_year')->unsigned();
            $table->string('season_of_year', 6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
