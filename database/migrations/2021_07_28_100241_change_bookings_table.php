<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function(Blueprint $table){
            $table->foreignId('city_id')->constrained();
            $table->date('trip_start_date');
            $table->date('trip_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function(Blueprint $table){
            $table->dropForeign('city_id');
            $table->dropColumn('city_id');
            $table->dropColumn('trip_start_date');
            $table->dropColumn('trip_end_date');
        });
    }
}
