<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateToursTableAddPk extends Migration
{
    public function up(): void
    {
        Schema::table('cities', function (Blueprint $table){
            $table->dropForeign(['region_id']);
        });
        Schema::table('regions', function (Blueprint $table){
            $table->dropForeign(['country_id']);
        });
        Schema::table('bookings', function(Blueprint $table){
            $table->dropForeign(['country_id']);
            $table->dropForeign(['region_id']);
        });

        Schema::table('bookings', function(Blueprint $table) {
            $table->unsignedBigInteger('country_id')->change();
            $table->unsignedBigInteger('region_id')->change();
        });

        Schema::table('countries', function (Blueprint $table){
            $table->unsignedBigInteger('id')->autoIncrement()->change();
        });

        Schema::table('regions', function (Blueprint $table){
            $table->unsignedBigInteger('id')->autoIncrement()->change();
            $table->unsignedBigInteger('country_id')->change();
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
        });

        Schema::table('cities', function (Blueprint $table){
            $table->unsignedBigInteger('region_id')->change();
            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');
        });
        Schema::table('bookings', function(Blueprint $table){
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');

        });

        Schema::table('tours', function (Blueprint $table) {
            $table->unsignedBigInteger('country_id')->change();
            $table->unsignedBigInteger('region_id')->change();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('continent_id')->references('id')->on('continents')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->foreign('organizer_id')->references('id')->on('organizers')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropForeign(['continent_id']);
            $table->dropForeign(['region_id']);
            $table->dropForeign(['organizer_id']);
        });
    }
}
