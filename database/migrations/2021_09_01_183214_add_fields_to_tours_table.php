<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToToursTable extends Migration
{
    public function up(): void
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->foreignId('country_id')->after('slug')->constrained()->onDelete('cascade');
            $table->foreignId('region_id')->after('slug')->constrained()->onDelete('cascade');
            $table->foreignId('city_id')->after('slug')->constrained()->onDelete('cascade');
            $table->double('user_price', 10, 2);
        });
    }

    public function down(): void
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropForeign(['region_id']);
            $table->dropForeign(['city_id']);

            $table->dropColumn(['country_id', 'region_id', 'city_id', 'user_price']);
        });
    }
}
