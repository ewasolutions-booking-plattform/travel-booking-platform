<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterThemeTourTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('themes', 'trip_themes');
        Schema::table('theme_types', function (Blueprint $table) {
            $table->dropForeign(['theme_id']);
            $table->renameColumn('theme_id', 'trip_theme_id');

            $table->foreign('trip_theme_id')
                ->references('id')
                ->on('trip_themes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::rename('theme_types', 'trip_theme_types');
        Schema::table('tours', function (Blueprint $table) {
            $table->renameColumn('trip_type_id', 'trip_theme_type_id');
        });

        Schema::table('tours', function (Blueprint $table) {
            $table->foreign('trip_theme_id')
                ->references('id')
                ->on('trip_themes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('trip_theme_type_id')
                ->references('id')
                ->on('trip_theme_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
