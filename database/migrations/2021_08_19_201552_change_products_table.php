<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('products', function(Blueprint $table){
            $table->unsignedBigInteger('room_id')->nullable()->change();
            $table->unsignedBigInteger('board_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('products', function(Blueprint $table){
            $table->foreignId('room_id');
            $table->foreignId('board_id');
        });
    }
}
