<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableRoomInfoAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rooms_infos', function (Blueprint $table) {
            $table->text('description');
            $table->text('price_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooms_infos', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('price_info');
        });
    }
}
