<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_email');
            $table->string('client_phone');
            $table->string('client_first_name');
            $table->string('client_last_name');
            $table->integer('country_id')->unsigned();
            $table->integer('region_id')->unsigned();
            $table->string('duration_type');
            $table->integer('duration_count')->unsigned();
            $table->integer('budget')->unsigned();
            $table->integer('tour_id')->unsigned();
            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
            $table->foreign('region_id')
                ->references('id')
                ->on('regions');
            $table->foreign('tour_id')
                ->references('id')
                ->on('tours');
        });

        Schema::create('age_category_booking', function (Blueprint $table) {
            $table->integer('booking_id')->unsigned();
            $table->integer('age_category_id')->unsigned();

            $table->foreign('booking_id')
                ->references('id')
                ->on('bookings');
            $table->foreign('age_category_id')
                ->references('id')
                ->on('age_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
